#NoTrayIcon
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
	#AutoIt3Wrapper_Version=p
	#AutoIt3Wrapper_Outfile=SMC_Launcher.exe
	#AutoIt3Wrapper_UseX64=n
	#AutoIt3Wrapper_Res_Language=1033
	#AutoIt3Wrapper_AU3Check_Parameters=-q -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
	#AutoIt3Wrapper_Run_After=_Utilities\ResourceHacker.exe -delete %out%, %out%, IconGroup, 162,
	#AutoIt3Wrapper_Run_After=_Utilities\ResourceHacker.exe -delete %out%, %out%, IconGroup, 164,
	#AutoIt3Wrapper_Run_After=_Utilities\ResourceHacker.exe -delete %out%, %out%, IconGroup, 169,
	#AutoIt3Wrapper_Run_Tidy=y
	#Tidy_Parameters=/reel /ri /sci 0 /sf
	#AutoIt3Wrapper_Run_Au3Stripper=Y
	#Au3Stripper_Parameters=/so /mi 9
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#Region Pragma
	#pragma compile(Icon, Include\SMC_Launcher.ico)
	#pragma compile(ExecLevel, asInvoker)
	#pragma compile(UPX, False)
	#pragma compile(AutoItExecuteAllowed, False)
	#pragma compile(Compression, 0)
	#pragma compile(FileDescription, Skyrim Mod Combiner Launcher)
	#pragma compile(ProductName, SMC)
	#pragma compile(ProductVersion, 1.4)
	#pragma compile(FileVersion, 1.4.0.4)
	#pragma compile(LegalCopyright, � dr.)
#EndRegion Pragma

#Region Main
	If @Compiled Then
		If WinExists(@ScriptName) Or ProcessExists('SMC.exe') Then ProcessClose('SMC.exe')
		AutoItWinSetTitle(@ScriptName)
		DirCreate(@ScriptDir & '\Temp\')
		If FileExists(@ScriptDir & '\Locale\') Then DirMove(@ScriptDir & '\Locale\', @ScriptDir & '\Lang\')
		If FileExists(@ScriptDir & '\Graphics\') Then DirMove(@ScriptDir & '\Graphics\', @ScriptDir & '\Pics\')
		If @OSArch = 'X86' Then
			FileInstall('x86\SMC.exe', @ScriptDir & '\SMC.exe', 1)
			FileInstall('x86\7z.dll', @ScriptDir & '\Temp\7z.dll', 1)
			FileInstall('x86\7z.exe', @ScriptDir & '\Temp\7z.exe', 1)
			FileInstall('x86\DDSopt.exe', @ScriptDir & '\Temp\DDSopt.exe', 1)
			FileInstall('x86\BSAopt.exe', @ScriptDir & '\Temp\BSAopt.exe', 1)
			FileInstall('x86\d3dx9_43.dll', @ScriptDir & '\Temp\d3dx9_43.dll', 1)
			FileInstall('x86\msvcp110.dll', @ScriptDir & '\Temp\msvcp110.dll', 1)
			FileInstall('x86\msvcr110.dll', @ScriptDir & '\Temp\msvcr110.dll', 1)
			FileInstall('x86\sqlite3.dll', @ScriptDir & '\Temp\sqlite3.dll', 1)
		Else
			FileInstall('x64\SMC.exe', @ScriptDir & '\SMC.exe', 1)
			FileInstall('x64\7z.dll', @ScriptDir & '\Temp\7z.dll', 1)
			FileInstall('x64\7z.exe', @ScriptDir & '\Temp\7z.exe', 1)
			FileInstall('x64\DDSopt.exe', @ScriptDir & '\Temp\DDSopt.exe', 1)
			FileInstall('x64\BSAopt.exe', @ScriptDir & '\Temp\BSAopt.exe', 1)
			FileInstall('x64\d3dx9_43.dll', @ScriptDir & '\Temp\d3dx9_43.dll', 1)
			FileInstall('x64\msvcp110.dll', @ScriptDir & '\Temp\msvcp110.dll', 1)
			FileInstall('x64\msvcr110.dll', @ScriptDir & '\Temp\msvcr110.dll', 1)
			FileInstall('x64\sqlite3_x64.dll', @ScriptDir & '\Temp\sqlite3_x64.dll', 1)
		EndIf
		FileInstall('Include\DDSopt.ini', @ScriptDir & '\Temp\DDSopt.ini', 1)
		FileInstall('Include\SMC_Logo.jpg', @ScriptDir & '\Temp\SMC_Logo.jpg', 1)
		FileInstall('Include\SMC_Stripe.png', @ScriptDir & '\Temp\SMC_Stripe.png', 1)
		FileInstall('Include\SMC_Loading.gif', @ScriptDir & '\Temp\SMC_Loading.gif', 1)
		FileInstall('Include\Calibri.ttf', @ScriptDir & '\Temp\Calibri.ttf', 1)
		FileInstall('Include\Consolas.ttf', @ScriptDir & '\Temp\Consolas.ttf', 1)
		FileInstall('Include\SMC.esp', @ScriptDir & '\Temp\SMC.esp', 1)
		RunWait('SMC.exe')
		FileDelete(@ScriptDir & '\SMC.exe')
		DirRemove(@ScriptDir & '\Temp\', 1)
	EndIf
	Exit
#EndRegion Main
