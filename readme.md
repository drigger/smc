# What is SMC (Skyrim Mod Combiner)?

SMC is a free, fast and light-weight mod combiner for Skyrim, designed to be as universal and user-friendly as possible.
> SMC was initially made as extended GUI wrapper for a batch-file from [Texture Pack Combiner](http://skyrim.nexusmods.com/mods/20801) by **Cestral**,
but uses its own updated lists now for compatibility with newer and additional mods.  
  
> If you like what I do you can buy me a drink by clicking this pretty button here:  [![Donate with PayPal Button](http://i.imgur.com/q5Azoli.png "Donate with PayPal")](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LVHX9P9F7UHHQ)
> or send a few satoshi to my BTC wallet: 1Bn9z83YvbGfNCgVHUcLcWANutucFC9nNz

---

   [![SMC Screenshot](http://i.imgur.com/yvgGCMC.png "SMC Screenshot")](http://i.imgur.com/yvgGCMC.png)

---

## SMC features:

* Written fully in AutoIt, so it is **Windows-only**  
* Comes with a **[7-Zip](http://www.7-zip.org/)** implemented  
* Uses **SQLite** as an internal database  
* Works with _archived_ and/or _extracted_ mods  
* Automatically installs _compatibility patches_, if needed  
* Dynamically calculates _space required_  
* Dynamically indicates _mod availability_  
* Optionally packs the combined output into _NMM-compatible archive_  
* Optionally _optimizes_ combined textures with **[DDSopt](http://skyrim.nexusmods.com/mods/5755)** and/or _unpacks input/packs output into BSA-archive_ with **[BSAopt](http://skyrim.nexusmods.com/mods/247)** by **Ethatron**  
* Automatically creates readable _detailed log_  

---

## Downloading & updating:

* Latest SMC version can always be found in **[Downloads](https://bitbucket.org/drigger/smc/downloads)** section  
* There is also a new **[SMC homepage on Nexus](http://www.nexusmods.com/skyrim/mods/51467)** available  
* Or you can simply click **[here](https://bitbucket.org/drigger/smc/downloads/SMC_1.4.0.4.7z)**  
* And don't worry, SMC will update itself once installed (but you can still check this page every now and then, just in case)  

---

## Issues:

If you find an issue, please report it on the **[issue tracker](https://bitbucket.org/drigger/smc/issues)**.  
However, please use the search function to make sure you don't make a duplicate.  
  
Creating a new issue:  

* Make a coherent description that explains your issue in details  
* Use of pictures is welcomed to better describe your issue  
* Please make sure you've included your faulty compilation log file (it should be available in the corresponding _Logs_ folder)  

---

## Thanks for donations from:

* Chris Soldatos  
* Jeffrey Smith  
* John Spurgeon  
* Joshua Baker  
* Luc Bobko  
* Malcolm Allison  
* Martin Klinge  
* Matthew Dayne  
* Mohammed Muhanna  
* Pascal Pascher  
* Sergio Bonfiglio  
* Tomas Sanchez Gutierrez  
  
> Really, thank you all very much, guys!  

---

## Latest changes:

> * [1.4.0.4 @ 2018.05.02]  
(+) Tooltips for mods with additional info  
(U) 7-Zip updated to the latest 18.05  

> * [1.4.0.3 @ 2018.04.27]  
(U) 7-Zip updated to the latest 18.04  
(U) SQLite.dll updated to the latest 3.23.1.0  

> * [1.4.0.2 @ 2017.06.15]  
(U) 7-Zip updated to the latest 17.00  
(U) SQLite.dll updated to the latest 3.19.3.0  

> * [1.4.0.1 @ 2016.10.13]  
(+) Text color notifications for mods containing ESM  
(U) 7-Zip updated to the latest 16.04  
(U) SQLite.dll updated to the latest 3.14.2.0  

> * [1.4.0.0 @ 2016.07.21]  
(+) Mod logos are now JPEGs with a shared transparent PNG layer on top to significantly reduce the overall app size  
(F) Invisible resize cursors fixed  
(U) 7-Zip updated to the latest 16.02  
(U) SQLite.dll updated to the latest 3.12.3.0  

> * [1.3.8.2 @ 2016.03.17]  
(F) A couple of small bugfixes  

> * [1.3.8.1 @ 2016.03.08]  
(U) SQLite.dll updated to the latest 3.11.0.0  

> * [1.3.8.0 @ 2016.01.03]  
(F) A couple of small bugfixes  
(U) 7-Zip updated to the latest 15.14  

> * [1.3.7.8 @ 2015.11.24]  
(F) Small bugfix  
(U) 7-Zip updated to the latest 15.12  
(U) SQLite.dll updated to the latest 3.9.2.0  

> * [1.3.7.7 @ 2015.10.29]  
(U) SQLite.dll updated to the latest 3.9.1.0  

> * [1.3.7.6 @ 2015.10.16]  
(U) 7-Zip updated to the latest 15.09 alpha  
(U) SQLite.dll updated to the latest 3.9.0.0  

> * [1.3.7.5 @ 2015.09.28]  
(+) Open specified path feature  

> * [1.3.7.4 @ 2015.09.20]  
(U) 7-Zip updated to the latest 15.07 alpha  
(U) SQLite.dll updated to the latest 3.8.11.1  

> * [1.3.7.3 @ 2015.08.10]  
(U) 7-Zip updated to the latest 15.06 alpha  
(U) SQLite.dll updated to the latest 3.8.11.0  

> * [1.3.7.2 @ 2015.07.10]  
(+) Windows 10 & Windows Server 2016 detection  

> * [1.3.7.1 @ 2015.06.25]  
(+) Additional columns in the main listview with available/found mod quality labels  
(U) 7-Zip updated to the latest 15.05 alpha  

> * [1.3.6.1 @ 2015.06.05]  
(U) 7-Zip updated to the latest 15.03 alpha  

> * [1.3.6.0 @ 2015.05.25]  
(+) Optional parameters in settings.ini for changing default color-coding  
(U) 7-Zip updated to the latest 15.02 alpha  

> * [1.3.5.7 @ 2015.05.12]  
(U) DDSopt.ini taken from S.T.E.P. guide  
(U) SQLite.dll updated to the latest 3.8.10.0  

> * [1.3.5.6 @ 2015.04.10]  
(U) SQLite.dll updated to the latest 3.8.9.0  

> * [1.3.5.5 @ 2015.04.03]  
(U) 7-Zip updated to the latest 15.00 alpha  

> * [1.3.5.4 @ 2015.03.21]  
(E) Slightly improved tooltips and mouse cursor behavior  
(E) Compatibility with updated AutoIt components  

> * [1.3.5.3 @ 2015.01.24]  
(F) Multi-folder extraction bug introduced in previous version reverted  
(U) SQLite.dll updated to the latest 3.8.8.1  

> * [1.3.5.2 @ 2015.01.19]  
(F) SQLite query finalization bug fixed  
(U) SQLite.dll updated to the latest 3.8.8.0  

> * [1.3.5.1 @ 2015.01.16]  
(E) Improved logs  
(U) 7-Zip updated to the latest 9.38 beta  

> * [1.3.5.0 @ 2014.12.29]  
(+) Text color notifications for mods containing scripts and/or ESP  
(U) 7-Zip updated to the latest 9.36 beta  
(U) SQLite.dll updated to the latest 3.8.7.4  

> * [1.3.4.2 @ 2014.12.08]  
(U) 7-Zip updated to the latest 9.35 beta  
(U) SQLite.dll updated to the latest 3.8.7.3  

> * [1.3.4.1 @ 2014.11.26]  
(U) SQLite.dll updated to the latest 3.8.7.2  

> * [1.3.4.0 @ 2014.11.10]  
(F) Optional ForceEnableDLC parameter in settings.ini  
(U) SQLite.dll updated to the latest 3.8.7.1  

> * [1.3.3.3 @ 2014.10.26]  
(F) Tiny UI edit  
(U) SQLite.dll updated to the latest 3.8.7.0  

> * [1.3.3.2 @ 2014.09.10]  
(F) Incorrect PreviousWindowSettings behavior bug fixed  
(U) 7-Zip updated to the latest 9.34 alpha  

> * [1.3.3.1 @ 2014.08.28]  
(F) .esm-files related bug fixed  
(U) SQLite.dll updated to the latest 3.8.6.0  

> * [1.3.3.0 @ 2014.08.14]  
(F) Fatal crash on launch and other bugfixes  

> * [1.3.2.3 @ 2014.07.22]  
(+) Double-clicking mod logo now opens in-app setting editor  
(F) Files overwriting mechanism bugfix  

> * [1.3.2.2 @ 2014.07.11]  
(F) Files overwriting mechanism improvements  
(U) Spanish language updated (thanks to Shiruz)  

> * [1.3.2.1 @ 2014.07.09]  
(F) Small bugfix  

> * [1.3.2.0 @ 2014.06.20]  
(+) Optional BSAoptUnpack parameter in settings.ini and in-app setting editor  
(F) First launch bugfixes  

> * [1.3.1.0 @ 2014.06.18]  
(+) Optional NoParallax parameter in settings.ini and in-app setting editor  
(+) Spanish language added (thanks to Shiruz)  
(E) Improved logs  
(U) 7-Zip updated to the latest 9.33 alpha  

> * [1.3.0.1 @ 2014.06.09]  
(F) Compatibility with updated AutoIt components and bugfixes  
(U) SQLite.dll updated to the latest 3.8.5.0  

> * [1.3.0.0 @ 2014.05.30]  
(E) MPRESS is not used anymore for exe/dll packaging (it should, hopefully, help to get rid of at least a couple of false positive antivirus reports)  

> * [1.2.0.3 @ 2014.05.15]  
(+) Compatible list feature: file copy conditions related to other mods selection  
(F) Compatibility with updated AutoIt components and bugfixes  
(E) Lowered priority of external components processing  

> * [1.2.0.2 @ 2014.04.09]  
(F) Compatibility with updated AutoIt components and bugfixes  
(U) SQLite.dll updated to the latest 3.8.4.3  

> * [1.2.0.1 @ 2014.03.31]  
(F) Hotfix for a "variable used without being declared" error  
(U) SQLite.dll updated to the latest 3.8.4.2  

> * [1.2.0.0 @ 2014.03.28]  
(+) NMM-compatible archive creation option is now available for users without NMM installed  
(+) Option for users with NMM installed to create NMM-compatible archive directly in NMM Skyrim mods folder (available through dialog window)  
(+) Compatible patches tab in mod settings editor  
(+) DDSoptimize file skipping feature finalized  
(+) File exclusion feature for new lists format (5th parameter)  
(F) Bugfixes  
(E) NMMCreate/NMMUseCompression renamed to CreateArchive/NMMUseCompression  

> * [1.1.0.1 @ 2014.03.20]  
(+) DDSoptimize file skipping feature  

> * [1.1.0.0 @ 2014.03.17]  
(+) Old TPC-styled batch files changed to new .lst (packed .ini)  
(+) Change compatible list used by SMC option (hold ctrl during app start)  
(+) Files overwriting mechanism  
(F) Better DLC detection  
(F) Faster list initialization  

> * [1.0.0.2 @ 2014.02.24]  
(F) Fixed incompatibility with x86 OS introduced in previous version  

> * [1.0.0.1 @ 2014.02.21]  
(E) NMMUseCompression option can now only be selected if NMMCreate is enabled  

> * [1.0.0.0 @ 2014.02.13]  
(+) Main TPC batch-file auto-download  
(F) CPU high consumption bug  

> * [0.9.0.5 @ 2014.02.03]  
(F) A bit more bugfixes  

> * [0.9.0.4 @ 2014.02.01]  
(F) A couple of small bugfixes  

> * [0.9.0.3 @ 2014.02.01]  
(+) NMM detection logic enhanced  

> * [0.9.0.2 @ 2014.02.01]  
(+) NMM detection status in logs  

> * [0.9.0.1 @ 2014.01.31]  
(+) Loading animation  
(E) Renamed some functions and cleaned code a little bit  

> * [0.9.0.0 @ 2014.01.28]  
(E) Application name changed to SMC (Skyrim Mod Combiner)  
(E) Application rebased to https://bitbucket.org/drigger/smc/  