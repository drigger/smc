# What is SMC (Skyrim Mod Combiner)?

SMC is a free, fast and light-weight mod combiner for Skyrim, designed to be as universal and user-friendly as possible.

> SMC was initially made as extended GUI wrapper for a batch-file from [Texture Pack Combiner] (http://skyrim.nexusmods.com/mods/20801) by *Cestral*,
but uses its own updated lists now for compatibility with newer and additional mods.
> If you like what I do you can buy me a drink by donating with PayPal using the link below:
https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LVHX9P9F7UHHQ
> or send a few satoshi to my BTC wallet: 1Bn9z83YvbGfNCgVHUcLcWANutucFC9nNz

## SMC features:

* Written fully in AutoIt, so it is *Windows-only*
* Comes with a [7-Zip] (http://www.7-zip.org/) implemented
* Uses *SQLite* as an internal database
* Works with _archived_ and/or _extracted_ mods
* Automatically installs _compatibility patches_, if needed
* Dynamically calculates _space required_
* Dynamically indicates _mod availability_
* Optionally packs the combined output into _NMM-compatible archive_
* Optionally _optimizes_ combined textures with [DDSopt] (http://skyrim.nexusmods.com/mods/5755) and/or _unpacks input/packs output into BSA-archive_ with [BSAopt] (http://skyrim.nexusmods.com/mods/247) by *Ethatron*
* Automatically creates readable _detailed log_

---

## Downloading & updating:

* Latest SMC version can always be found in [Downloads] (https://bitbucket.org/drigger/smc/downloads) section
* There is also a new [SMC homepage on Nexus] (http://www.nexusmods.com/skyrim/mods/51467) available
* And don't worry, SMC will update itself once installed (but you can still check this page every now and then, just in case)

---

## Issues:

If you find an issue, please report it on the [issue tracker] (https://bitbucket.org/drigger/smc/issues).
However, please use the search function to make sure you don't make a duplicate.

Creating a new issue:

* Make a coherent description that explains your issue in details
* Use of pictures is welcomed to better describe your issue
* Please make sure you've included your faulty compilation log file (it should be available in the corresponding _Logs_ folder)

---

## Thanks for donations from:

* Chris Soldatos
* Jeffrey Smith
* John Spurgeon
* Joshua Baker
* Luc Bobko
* Malcolm Allison
* Martin Klinge
* Matthew Dayne
* Mohammed Muhanna
* Pascal Pascher
* Sergio Bonfiglio
* Tomas Sanchez Gutierrez

> Really, thank you all very much, guys!