#NoTrayIcon
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
	#AutoIt3Wrapper_Version=p
	#AutoIt3Wrapper_Outfile=x86\SMC.exe
	#AutoIt3Wrapper_Outfile_x64=x64\SMC.exe
	#AutoIt3Wrapper_Compile_Both=y
	#AutoIt3Wrapper_Res_Language=1033
	#AutoIt3Wrapper_AU3Check_Parameters=-q -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
	#AutoIt3Wrapper_Run_After=_Utilities\ResourceHacker.exe -delete %out%, %out%, IconGroup, 162,
	#AutoIt3Wrapper_Run_After=_Utilities\ResourceHacker.exe -delete %out%, %out%, IconGroup, 164,
	#AutoIt3Wrapper_Run_After=_Utilities\ResourceHacker.exe -delete %out%, %out%, IconGroup, 169,
	#AutoIt3Wrapper_Run_After=_Utilities\ResourceHacker.exe -delete %outx64%, %outx64%, IconGroup, 162,
	#AutoIt3Wrapper_Run_After=_Utilities\ResourceHacker.exe -delete %outx64%, %outx64%, IconGroup, 164,
	#AutoIt3Wrapper_Run_After=_Utilities\ResourceHacker.exe -delete %outx64%, %outx64%, IconGroup, 169,
	#AutoIt3Wrapper_Run_Tidy=y
	#Tidy_Parameters=/reel /ri /sci 0 /sf
	#AutoIt3Wrapper_Run_Au3Stripper=y
	#Au3Stripper_Parameters=/so /mi 9
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#Region Pragma
	#pragma compile(Icon, Include\SMC.ico)
	#pragma compile(ExecLevel, asInvoker)
	#pragma compile(UPX, False)
	#pragma compile(AutoItExecuteAllowed, False)
	#pragma compile(Compression, 0)
	#pragma compile(FileDescription, Skyrim Mod Combiner)
	#pragma compile(ProductName, SMC)
	#pragma compile(ProductVersion, 1.4)
	#pragma compile(FileVersion, 1.4.0.4)
	#pragma compile(LegalCopyright, © dr.)
#EndRegion Pragma

Opt('GUIOnEventMode', 1)
Opt('GUIResizeMode', 802)

#Region Includes
	#include-once
	#include <Color.au3>
	#include <Constants.au3>
	#include <EditConstants.au3>
	#include <File.au3>
	#include <GUIButton.au3>
	#include <GUIConstantsEx.au3>
	#include <GUIListView.au3>
	#include <GUITreeView.au3>
	#include <Misc.au3>
	#include <StaticConstants.au3>
	#include <String.au3>
	#include <StructureConstants.au3>
	#include <WinAPIEx.au3>
	#include <WindowsConstants.au3>
	#include <Include\ExtMsgBox.au3>
	#include <Include\FileOperations.au3>
	#include <Include\GIFAnimation.au3>
	#include <Include\HotKey.au3>
	#include <Include\ITaskBarList.au3>
	#include <Include\StringSize.au3>
#EndRegion Includes

#Region Variables
	Global $7zExe, $DDSoptExe, $BSAoptExe, $SMCLogo, $SMCStripe, $SMCLoading, $AppESP
	If @Compiled Then
		$7zExe = @ScriptDir & '\Temp\7z.exe'
		$DDSoptExe = @ScriptDir & '\Temp\DDSopt.exe'
		$BSAoptExe = @ScriptDir & '\Temp\BSAopt.exe'
		$SMCLogo = @ScriptDir & '\Temp\SMC_Logo.jpg'
		$SMCStripe = @ScriptDir & '\Temp\SMC_Stripe.png'
		$SMCLoading = @ScriptDir & '\Temp\SMC_Loading.gif'
		$AppESP = @ScriptDir & '\Temp\SMC.esp'
	Else
		If @OSArch = 'X86' Then
			$7zExe = @ScriptDir & '\x86\7z.exe'
			$DDSoptExe = @ScriptDir & '\x86\DDSopt.exe'
			$BSAoptExe = @ScriptDir & '\x86\BSAopt.exe'
		Else
			$7zExe = @ScriptDir & '\x64\7z.exe'
			$DDSoptExe = @ScriptDir & '\x64\DDSopt.exe'
			$BSAoptExe = @ScriptDir & '\x64\BSAopt.exe'
		EndIf
		$SMCLogo = @ScriptDir & '\Include\SMC_Logo.jpg'
		$SMCStripe = @ScriptDir & '\Include\SMC_Stripe.png'
		$SMCLoading = @ScriptDir & '\Include\SMC_Loading.gif'
		$AppESP = @ScriptDir & '\Include\SMC.esp'
	EndIf
	Global $GUILoading = GUICreate('', 200, 200, -1, -1, $WS_POPUP, $WS_EX_TOPMOST)
	$SMCLoading = _GUICtrlCreateGIF($SMCLoading, '', 0, 0, 200, 200)
	GUIRegisterMsg(15, 'GUIRefresh')
	WinSetTrans($GUILoading, '', 0)
	GUISetState(@SW_SHOW, $GUILoading)
	For $i = 0 To 255 Step 2.5
		WinSetTrans($GUILoading, '', $i)
		Sleep(1)
	Next
	GUISetCursor(1, 1, $GUILoading)
	_ExtMsgBoxSet(14, 5, Default, Default, 10, 'Calibri') ;, 600, 700)
	_WinAPI_AddFontResourceEx(@ScriptDir & '\Temp\Calibri.ttf')
	_WinAPI_AddFontResourceEx(@ScriptDir & '\Temp\Consolas.ttf')
	Global $AppConfig = @ScriptDir & '\settings.ini'
	Global $TempDir = IniRead($AppConfig, 'System', 'TempDir', '')
	Global $NoAutoUpdate = IniRead($AppConfig, 'System', 'NoAutoUpdate', '')
	While StringRight($TempDir, 1) = '\' Or StringRight($TempDir, 1) = '/'
		$TempDir = StringTrimRight($TempDir, 1)
	WEnd
	If FileExists($TempDir) = 0 Then DirCreate($TempDir)
	If StringInStr(FileGetAttrib($TempDir), 'D') = 0 Then $TempDir = @TempDir
	Global $AppLang = @ScriptDir & '\Lang\' & CheckLanguageSystem() & '.lng'
	Global $LinkComponents = IniRead($AppConfig, 'System', 'LinkComponents', 'https://bitbucket.org/drigger/smc/raw/master/')
	CheckFileUpdate($AppLang)
	Global $NMMPath = @LocalAppDataDir & '\Black_Tree_Gaming\'
	Global $TaskBarCompat = (StringInStr('WIN_7 WIN_8 WIN_81 WIN_10 WIN_2008R2 WIN_2012 WIN_2012R2 WIN_2016', @OSVersion) ? 1 : 0)
	Global $AppVer = StringSplit(FileGetVersion(@ScriptFullPath), '.')
	Global $AppBuildTime = FileGetTime(@ScriptFullPath)
	Global $TempFile = _TempFile($TempDir, '~', '.smc')
	Global $_Proceed = IniRead($AppLang, 'Dialogs', 'Proceed', 'Proceed')
	Global $_Retry = IniRead($AppLang, 'Dialogs', 'Retry', 'Retry')
	Global $_Warning = IniRead($AppLang, 'Dialogs', 'Warning', 'Warning')
	Global $_iniNotFound = IniRead($AppLang, 'Errors', 'iniNotFound', 'SMC settings file not found')
	Global $_SelectManually = IniRead($AppLang, 'Errors', 'SelectManually', 'Please, select this file manually')
	Global $_ChooseSMCini = IniRead($AppLang, 'Dialogs', 'ChooseSMCini', 'Choose SMC settings file')
	Global $_Config = IniRead($AppLang, 'Dialogs', 'Config', 'Configuration File')
	CheckFileUpdate($AppConfig, 1)
	If FileExists($AppConfig) <> 1 Then
		_ExtMsgBox(48, '&' & $_Proceed, $_Warning, $_iniNotFound & '!' & @CRLF & @CRLF & $_SelectManually & '...', 3)
		$AppConfig = FileOpenDialog($_ChooseSMCini, @ScriptDir & '\', $_Config & ' (*.ini)', 11)
	EndIf
	Global $_PreferredLanguage = StringLower(IniRead($AppConfig, 'System', 'PreferredLanguage', ''))
	If $_PreferredLanguage Then
		$AppLang = @ScriptDir & '\Lang\' & $_PreferredLanguage & '.lng'
		CheckFileUpdate($AppLang)
		$_Proceed = IniRead($AppLang, 'Dialogs', 'Proceed', 'Proceed')
		$_Retry = IniRead($AppLang, 'Dialogs', 'Retry', 'Retry')
		$_Warning = IniRead($AppLang, 'Dialogs', 'Warning', 'Warning')
		$_iniNotFound = IniRead($AppLang, 'Errors', 'iniNotFound', 'SMC settings file not found')
		$_SelectManually = IniRead($AppLang, 'Errors', 'SelectManually', 'Please, select this file manually')
		$_ChooseSMCini = IniRead($AppLang, 'Dialogs', 'ChooseSMCini', 'Choose SMC settings file')
		$_Config = IniRead($AppLang, 'Dialogs', 'Config', 'Configuration File')
	EndIf
	If FileExists($AppLang) = 0 Then
		$AppLang = @ScriptDir & '\Lang\en.lng'
		CheckFileUpdate($AppLang)
	EndIf
	Global $ListPath = IniRead($AppConfig, 'System', 'ListPath', 'SMC')
	Global $ListLink = IniRead($AppConfig, 'System', 'ListLink', 'http://www.nexusmods.com/skyrim/mods/51467')
	$LinkComponents = IniRead($AppConfig, 'System', 'LinkComponents', 'https://bitbucket.org/drigger/smc/raw/master/')
	Global $LinkHome = IniRead($AppConfig, 'System', 'LinkHome', 'https://bitbucket.org/drigger/smc/')
	Global $LinkPics = IniRead($AppConfig, 'System', 'LinkPics', 'https://bitbucket.org/drigger/smc/raw/master/Pics/')
	Global $LinkUpdate = IniRead($AppConfig, 'System', 'LinkUpdate', 'https://bitbucket.org/drigger/smc/downloads/')
	Global $PreferredInputPath = IniRead($AppConfig, 'System', 'PreferredInputPath', '')
	Global $NoLogging = IniRead($AppConfig, 'System', 'NoLogging', '')
	Global $DefinitionsVersion = IniRead($AppConfig, 'System', 'DefinitionsVersion', 0)
	Global $VersionLatest = IniRead($AppConfig, 'System', 'VersionLatest', 0)
	Global $RequiredModsPath = IniRead($AppConfig, 'System', 'RequiredModsPath', 'Required Mods')
	Global $OptionalModsPath = IniRead($AppConfig, 'System', 'OptionalModsPath', 'Optional Mods')
	Global $PreviousOutputPath = IniRead($AppConfig, 'System', 'PreviousOutputPath', @ScriptDir)
	Global $PreviousWindowSettings = IniRead($AppConfig, 'System', 'PreviousWindowSettings', '')
	Global $EditorMode = IniRead($AppConfig, 'System', 'EditorMode', 0)
	Global $ForceEnableDLC = IniRead($AppConfig, 'System', 'ForceEnableDLC', 0)
	Global $_Language = IniRead($AppLang, 'Interface', 'Language', 'US')
	Global $_HelpTitle = IniRead($AppLang, 'Interface', 'HelpTitle', 'Help')
	Global $_SettingsTitle = IniRead($AppLang, 'Interface', 'SettingsTitle', 'Settings')
	Global $_ChangelogTitle = IniRead($AppLang, 'Interface', 'ChangelogTitle', 'Changelog')
	Global $_ChangelogDesc = IniRead($AppLang, 'Interface', 'ChangelogDesc', 'This item can be expanded to reveal list of application changes')
	Global $_FAQTitle = IniRead($AppLang, 'Interface', 'FAQTitle', 'FAQ')
	Global $_FAQDesc = IniRead($AppLang, 'Interface', 'FAQDesc', 'This item can be expanded to reveal list of frequently asked questions')
	Global $_CompatibilityTitle = IniRead($AppLang, 'Interface', 'CompatibilityTitle', 'Compatible Mods')
	Global $_CompatibilityPatches = IniRead($AppLang, 'Interface', 'CompatibilityPatches', 'Compatibility Patches')
	Global $_ModName = IniRead($AppLang, 'Interface', 'ModName', 'Mod name')
	Global $_SpecifiedPath = IniRead($AppLang, 'Interface', 'SpecifiedPath', 'Specified Path')
	Global $_BeginButton = IniRead($AppLang, 'Interface', 'BeginButton', '&Begin Combining')
	Global $_CancelButton = IniRead($AppLang, 'Interface', 'CancelButton', '&Cancel Update')
	Global $_Interrupt = IniRead($AppLang, 'Interface', 'Interrupt', 'Press Ctrl+Alt+&X to interrupt')
	Global $_Name = IniRead($AppLang, 'Interface', 'Name', 'Name')
	Global $_Directory = IniRead($AppLang, 'Interface', 'Directory', 'Directory')
	Global $_Author = IniRead($AppLang, 'Interface', 'Author', 'Author')
	Global $_Link = IniRead($AppLang, 'Interface', 'Link', 'Link')
	Global $_Logo = IniRead($AppLang, 'Interface', 'Logo', 'Logo')
	Global $_Archives = IniRead($AppLang, 'Interface', 'Archives', 'Archives')
	Global $_Size = IniRead($AppLang, 'Interface', 'Size', 'Size')
	Global $_Properties = IniRead($AppLang, 'Interface', 'Properties', 'Properties')
	Global $_PropertiesDesc = IniRead($AppLang, 'Interface', 'PropertiesDesc', 'This item can be expanded to reveal list of possible mod properties')
	Global $_Unsupported = IniRead($AppLang, 'Interface', 'Unsupported', 'Unsupported')
	Global $_Editing = IniRead($AppLang, 'Interface', 'Editing', 'Editing')
	Global $_ApplyChanges = IniRead($AppLang, 'Interface', 'ApplyChanges', 'Apply Changes')
	Global $_LastEdit = IniRead($AppLang, 'Interface', 'LastEdit', 'Last edit')
	Global $_AdditionalInfo = IniRead($AppLang, 'Interface', 'AdditionalInfo', 'Additional info')
	Global $_Updated = IniRead($AppLang, 'Interface', 'Updated', 'Updated')
	Global $_Added = IniRead($AppLang, 'Interface', 'Added', 'Added')
	Global $_Removed = IniRead($AppLang, 'Interface', 'Removed', 'Removed')
	Global $_ListSelection = IniRead($AppLang, 'Interface', 'ListSelection', 'list selection')
	Global $_ListSelect = IniRead($AppLang, 'Interface', 'ListSelect', 'Select compatible list')
	Global $_SpecifyPath = IniRead($AppLang, 'Menu', 'SpecifyPath', 'Specify path')
	Global $_SpecifyModPath = IniRead($AppLang, 'Menu', 'SpecifyModPath', 'Specify archived or extracted &mod path')
	Global $_EditModSettings = IniRead($AppLang, 'Menu', 'EditModSettings', 'Edit m&od settings')
	Global $_DownloadMod = IniRead($AppLang, 'Menu', 'DownloadMod', 'Proceed to mod &download page')
	Global $_OpenPath = IniRead($AppLang, 'Menu', 'OpenPath', 'Open specified pa&th')
	Global $_PathAll = IniRead($AppLang, 'Menu', 'PathAll', 'for the &rest')
	Global $_PathAllUnspec = IniRead($AppLang, 'Menu', 'PathAllUnspec', 'for the &unspecified')
	Global $_CheckAll = IniRead($AppLang, 'Menu', 'CheckAll', 'Check all &specified paths')
	Global $_UncheckAll = IniRead($AppLang, 'Menu', 'UncheckAll', 'Uncheck all specified &paths')
	Global $_ClearCurrent = IniRead($AppLang, 'Menu', 'ClearCurrent', 'Clear &current specified path')
	Global $_ClearAllEx = IniRead($AppLang, 'Menu', 'ClearAllEx', 'Clear all specified paths, &except current')
	Global $_ClearAll = IniRead($AppLang, 'Menu', 'ClearAll', 'Clear &all specified paths')
	Global $_Quit = IniRead($AppLang, 'Dialogs', 'Quit', 'Quit')
	Global $_Skip = IniRead($AppLang, 'Dialogs', 'Skip', 'Skip')
	Global $_OpenAppPage = IniRead($AppLang, 'Dialogs', 'OpenAppPage', 'Open SMC project home page')
	Global $_OpenModPage = IniRead($AppLang, 'Dialogs', 'OpenModPage', 'Open page of mod shown in the logo')
	Global $_OpenHelp = IniRead($AppLang, 'Dialogs', 'OpenHelp', 'Open application help')
	Global $_OpenSettings = IniRead($AppLang, 'Dialogs', 'OpenSettings', 'Open application settings')
	Global $_Apply = IniRead($AppLang, 'Dialogs', 'Apply', 'Apply')
	Global $_ApplyModChanges = IniRead($AppLang, 'Dialogs', 'ApplyModChanges', 'Apply changes to mod settings')
	Global $_ApplySettings = IniRead($AppLang, 'Dialogs', 'ApplySettings', 'Apply changes to application settings')
	Global $_ChooseModFolder = IniRead($AppLang, 'Dialogs', 'ChooseModFolder', 'Choose a folder with the downloaded archives or extracted version of mod')
	Global $_ChooseOutputFolder = IniRead($AppLang, 'Dialogs', 'ChooseOutputFolder', 'Choose a valid output path location (SMC folder will be created inside)')
	Global $_ChooseList = IniRead($AppLang, 'Dialogs', 'ChooseList', 'Choose compatible list file')
	Global $_ChooseDDSoptExe = IniRead($AppLang, 'Dialogs', 'ChooseDDSoptExe', 'Choose DDSopt executable file (usually DDSopt *.exe)')
	Global $_ChooseBSAoptExe = IniRead($AppLang, 'Dialogs', 'ChooseBSAoptExe', 'Choose BSAopt executable file (usually BSAopt *.exe)')
	Global $_Choose7zExe = IniRead($AppLang, 'Dialogs', 'Choose7zExe', 'Choose 7-Zip executable file (usually 7z.exe, 7z.dll MUST exists in the same folder)')
	Global $_List = IniRead($AppLang, 'Dialogs', 'List', 'List File')
	Global $_Executable = IniRead($AppLang, 'Dialogs', 'Executable', 'Executable File')
	Global $_UpdateAvailable = IniRead($AppLang, 'Dialogs', 'UpdateAvailable', 'update available')
	Global $_UpdateProceed = IniRead($AppLang, 'Dialogs', 'UpdateProceed', 'Proceed with this update')
	Global $_VersionCurrent = IniRead($AppLang, 'Dialogs', 'VersionCurrent', 'Your current version')
	Global $_VersionLatest = IniRead($AppLang, 'Dialogs', 'VersionLatest', 'Latest version')
	Global $_VersionLatestChanges = IniRead($AppLang, 'Dialogs', 'VersionLatestChanges', 'Changes in the latest version')
	Global $_ErrorsFound = IniRead($AppLang, 'Dialogs', 'ErrorsFound', 'There were errors during combining')
	Global $_ErrorsCheckLog = IniRead($AppLang, 'Dialogs', 'ErrorsCheckLog', 'Check the created log file for details')
	Global $_DLCFound = IniRead($AppLang, 'Logs', 'DLCFound', 'DLC found')
	Global $_CombiningStarted = IniRead($AppLang, 'Logs', 'CombiningStarted', 'combining  started')
	Global $_CombiningFinished = IniRead($AppLang, 'Logs', 'CombiningFinished', 'combining finished')
	Global $_ArchiveFound = IniRead($AppLang, 'Logs', 'ArchiveFound', 'found mod archive for extraction')
	Global $_CPSectionFound = IniRead($AppLang, 'Logs', 'CPSectionFound', 'found not empty [Compatibility Patches] section')
	Global $_CPArchiveFound = IniRead($AppLang, 'Logs', 'CPArchiveFound', 'found compatibility patch archive for extraction')
	Global $_FolderSearch = IniRead($AppLang, 'Logs', 'FolderSearch', 'looking for an extracted mod in')
	Global $_ModChecked = IniRead($AppLang, 'Logs', 'ModChecked', 'checked mod')
	Global $_ListSelected = IniRead($AppLang, 'Logs', 'ListSelected', 'compatible list file selected')
	Global $_InputPathSelected = IniRead($AppLang, 'Logs', 'InputPathSelected', 'input path search location selected')
	Global $_OutputPathSelected = IniRead($AppLang, 'Logs', 'OutputPathSelected', 'output path location selected')
	Global $_DDSoptimizeSelected = IniRead($AppLang, 'Logs', 'DDSoptimizeSelected', 'output optimization with DDSopt selected')
	Global $_BSAoptUnpackSelected = IniRead($AppLang, 'Logs', 'BSAoptUnpackSelected', 'unpack input BSA files with BSAopt selected')
	Global $_BSAoptPackSelected = IniRead($AppLang, 'Logs', 'BSAoptPackSelected', 'pack output with BSAopt selected')
	Global $_NoParallaxSelected = IniRead($AppLang, 'Logs', 'NoParallaxSelected', 'exclude parallax maps from compilation selected')
	Global $_ArchivingSelected = IniRead($AppLang, 'Logs', 'ArchivingSelected', 'NMM-compatible archive creation selected')
	Global $_NMMFound = IniRead($AppLang, 'Logs', 'NMMFound', 'Nexus Mod Manager found, Skyrim mods folder is located at')
	Global $_Finalizing = IniRead($AppLang, 'Logs', 'Finalizing', 'finalizing combining into output folder')
	Global $_TempCreated = IniRead($AppLang, 'Logs', 'TempCreated', 'temporary files created')
	Global $_TempRemoved = IniRead($AppLang, 'Logs', 'TempRemoved', 'temporary files removed')
	Global $_SQLiteCreated = IniRead($AppLang, 'Logs', 'SQLiteCreated', 'SQLite temp database created')
	Global $_SQLiteRemoved = IniRead($AppLang, 'Logs', 'SQLiteRemoved', 'SQLite temp database removed')
	Global $_DDSoptimization = IniRead($AppLang, 'Logs', 'DDSoptimization', 'combined output optimization')
	Global $_BSAoptUnpacking = IniRead($AppLang, 'Logs', 'BSAoptUnpacking', 'input BSA file unpacking')
	Global $_BSAoptPacking = IniRead($AppLang, 'Logs', 'BSAoptPacking', 'combined output packing')
	Global $_ArchiveCreated = IniRead($AppLang, 'Logs', 'ArchiveCreated', 'Nexus Mod Manager-compatible combined archive created')
	Global $_7zCompressionUsed = IniRead($AppLang, 'Logs', '7zCompressionUsed', 'using ultra compression level')
	Global $_7zCompressionNotUsed = IniRead($AppLang, 'Logs', '7zCompressionNotUsed', 'using no compression')
	Global $_ModLogoCacheUpdate = IniRead($AppLang, 'Logs', 'ModLogoCacheUpdate', 'mod logo cache update')
	Global $_AppUpdate = IniRead($AppLang, 'Logs', 'AppUpdate', 'app update found')
	Global $_UpdateCancelled = IniRead($AppLang, 'Logs', 'UpdateCancelled', 'update process was cancelled by user')
	Global $_SpaceRequired = IniRead($AppLang, 'Logs', 'SpaceRequired', 'free space (required/available)')
	Global $_Unknown = IniRead($AppLang, 'Logs', 'Unknown', 'unknown')
	Global $_Error = IniRead($AppLang, 'Dialogs', 'Error', 'Error')
	Global $_Question = IniRead($AppLang, 'Dialogs', 'Question', 'Question')
	Global $_MoveArchiveInNMMPath = IniRead($AppLang, 'Dialogs', 'MoveArchiveInNMMPath', 'Move NMM-compatible archive to this directory')
	Global $_Interruption = IniRead($AppLang, 'Errors', 'Interruption', 'Interruption hotkey was pressed by user')
	Global $_DDSoptWarning = IniRead($AppLang, 'Errors', 'DDSoptWarning', 'DDSopt executable file was not found')
	Global $_DDSoptError = IniRead($AppLang, 'Errors', 'DDSoptError', 'encountered some problems while processing with DDSopt')
	Global $_BSAoptWarning = IniRead($AppLang, 'Errors', 'BSAoptWarning', 'BSAopt executable file was not found')
	Global $_BSAoptError = IniRead($AppLang, 'Errors', 'BSAoptError', 'encountered some problems while processing with BSAopt')
	Global $_7zWarning = IniRead($AppLang, 'Errors', '7zWarning', '7-Zip executable file or engine module was not found')
	Global $_7zError = IniRead($AppLang, 'Errors', '7zError', 'encountered some problems while processing with 7-Zip, error code')
	Global $_iniWriteError = IniRead($AppLang, 'Errors', 'iniWriteError', 'SMC settings file could not be rewritten')
	Global $_iniWriteFix = IniRead($AppLang, 'Errors', 'iniWriteFix', 'Please, create the file or uncheck the read-only attribute')
	Global $_iniModsError = IniRead($AppLang, 'Errors', 'iniModsError', 'SMC settings file has incorrect structure or could not be read')
	Global $_iniCPsWarning = IniRead($AppLang, 'Errors', 'iniCPsWarning', 'SMC settings file section [Compatibility Patches] not found or contain no values and will be skipped')
	Global $_ListError = IniRead($AppLang, 'Errors', 'ListError', 'Compatible list file not found or reading error')
	Global $_SQLiteInitError = IniRead($AppLang, 'Errors', 'SQLiteInitError', 'SQLite initializion error')
	Global $_SQLiteWriteError = IniRead($AppLang, 'Errors', 'SQLiteWriteError', 'Error writing SQLite DB')
	Global $_SQLiteError = IniRead($AppLang, 'Errors', 'SQLiteError', 'encountered some problems while processing with SQLite, error code')
	Global $_AppUpdateError = IniRead($AppLang, 'Errors', 'AppUpdateError', 'update file is not available at the moment')
	Global $_ButtonBeginTip = IniRead($AppLang, 'Tips', 'Begin', 'Begin mod combining process')
	Global $_ButtonCancelTip = IniRead($AppLang, 'Tips', 'Cancel', 'Cancel update process')
	Global $_CanBeCombined = IniRead($AppLang, 'Tips', 'CanBeCombined', 'This mod can be combined')
	Global $_CanNotBeCombined = IniRead($AppLang, 'Tips', 'CanNotBeCombined', "This mod can't be combined")
	Global $_FoundBoth = IniRead($AppLang, 'Tips', 'FoundBoth', 'found extracted and archived')
	Global $_FoundArchived = IniRead($AppLang, 'Tips', 'FoundArchived', 'found archived')
	Global $_FoundExtracted = IniRead($AppLang, 'Tips', 'FoundExtracted', 'found extracted')
	Global $_FoundUnsupported = IniRead($AppLang, 'Tips', 'FoundUnsupported', 'found archived, but unsupported')
	Global $_FoundNone = IniRead($AppLang, 'Tips', 'FoundNone', 'not found')
	Global $_SetCleanInstall = IniRead($AppLang, 'Tips', 'SetCleanInstall', 'Check this if you want to completely erase all the output folder contents before compilation')
	Global $_SetDDSoptimize = IniRead($AppLang, 'Tips', 'SetDDSoptimize', 'Check this if you want to optimize combined output with DDSopt')
	Global $_SetBSAoptUnpack = IniRead($AppLang, 'Tips', 'SetBSAoptUnpack', 'Check this if you want to unpack input BSA files with BSAopt')
	Global $_SetBSAoptPack = IniRead($AppLang, 'Tips', 'SetBSAoptPack', 'Check this if you want to pack combined output with BSAopt')
	Global $_SetCreateArchive = IniRead($AppLang, 'Tips', 'SetCreateArchive', 'Check this if you want to pack combined output as an NMM-compatible archive (an old version would be overwritten, if exists)')
	Global $_SetUseCompression = IniRead($AppLang, 'Tips', 'SetUseCompression', 'Check this if you want to use 7-Zip ultra compression level while creating NMM-compatible archive')
	Global $_SetNoAutoUpdate = IniRead($AppLang, 'Tips', 'SetNoAutoUpdate', 'Check this if you want to completely disable application update functions (self-updating, definitions file update, mod graphic logos cache update)')
	Global $_SetNoLogging = IniRead($AppLang, 'Tips', 'SetNoLogging', 'Check this if you want to completely disable application logging')
	Global $_SetNoParallax = IniRead($AppLang, 'Tips', 'SetNoParallax', 'Check this if you want to completely exclude parallax maps from compilation')
	Global $_SetEditorMode = IniRead($AppLang, 'Tips', 'SetEditorMode', 'Check this if you want to enable mod settings editor')
	Global $_Previous = IniRead($AppLang, 'Statuses', 'Previous', 'previous')
	Global $_StatusMsg = IniRead($AppLang, 'Statuses', 'Status', 'status')
	Global $_Presetting = IniRead($AppLang, 'Statuses', 'Presetting', 'presetting')
	Global $_ModsChecked = IniRead($AppLang, 'Statuses', 'ModsChecked', 'mods checked')
	Global $_ModsCheckedSize = IniRead($AppLang, 'Statuses', 'ModsCheckedSize', 'free space required, Mb')
	Global $_SizeMb = IniRead($AppLang, 'Statuses', 'SizeMb', 'Mb')
	Global $_SizeGb = IniRead($AppLang, 'Statuses', 'SizeGb', 'Gb')
	Global $_Combining = IniRead($AppLang, 'Statuses', 'Combining', 'combining')
	Global $_Extracting = IniRead($AppLang, 'Statuses', 'Extracting', 'extracting')
	Global $_OutputSelect = IniRead($AppLang, 'Statuses', 'OutputSelect', 'waiting for output path location selection')
	Global $_NMMCreating = IniRead($AppLang, 'Statuses', 'NMMCreating', 'creating NMM-compatible archive')
	Global $MainProgress, $LabelProgressWhite, $LabelProgressBlack, $HelpFocus, $GUIMainContextMenu, $MainButton, $Quality, $Path, $Query, $Row, $MainPic, $MainPicStripe, $ListView, $hListView, $NMMExists, $NMMLocated, $HelpTreeView
	Global $HelpEdit, $HelpListView, $MainPicWidth, $CheckedSumSize, $SizeUnit, $UpdateProgress, $UpdateButtonProceed, $UpdateButtonSkip, $MenuMainAppPageOpen, $MenuMainModPageOpen, $MenuMainSettings, $MenuMainHelp, $SettingsCleanInstall
	Global $SettingsDDSoptimize, $SettingsBSAoptPack, $SettingsBSAoptUnpack, $SettingsCreateArchive, $SettingsUseCompression, $SettingsNoParallax, $SettingsNoAutoUpdate, $SettingsNoLogging, $SettingsEditorMode, $SettingsButton
	Global $SettingsArray, $SortDirection, $HelpSection, $HelpTreeViewItem, $FAQSection, $FAQTreeViewItem, $FAQTreeView, $ChangelogSection, $ChangelogTreeViewItem, $ChangelogTreeView, $CompatibilityTreeView, $NMMClientModsFolder
	Global $GUIMain = 0, $GUIHelp = 0, $GUISettings = 0, $GUIUpdate = 0, $GUIListSelect = 0, $ListViewItemSelected = '', $ProgressBarLastStoredText = '', $NoParallax = ''
	Global $SkyrimDLC[][] = [['Hearthfire', 'HearthFires.esm', 0], ['Dawnguard', 'Dawnguard.esm', 0], ['Dragonborn', 'Dragonborn.esm', 0]], $QualityArray[] = ['UQ', 'HQ', 'MQ', 'LQ']
	Global $GUIModEditorLastActive = -1, $GUIMainWidthMin = 500, $GUIMainHeightMin = 200, $GUIHelpTreeViewWidthMin = 180, $MilestoneOne = 25, $MilestoneTwo = 60, $ListSelectExit = 0, $OverallErrors = 0
	Global $HelpFocusFirstOpen = 0, $PathExistsSize = 0, $Unsupported = 0, $CheckedSumSizeAbs = 0, $CombineInterruptSwitch = 0, $RefreshTrigger = 0, $ProgressBarLastStoredPercent = 0, $ProgressBarLastStoredState = 0, $CheckedSum = 0
	Global $ListViewPageSize = 0, $ListViewItemsCounter = 0, $ListViewItemsSum = 0, $ModLogoSelected = 0, $HelpCreateItemsCreated = 0, $UpdateCancel = 0, $ModNameLongest = 0, $LogCreate = 0, $AppConfigUpdated = 0, $ModRemovedNameLongest = 0
	Global $GUIMainSize[] = [1000, 664], $GUIHelpSize[] = [1000, 664], $GUIUpdateSize[] = [500, 100], $GUIMainPosition[] = [-1, -1], $GUIHelpPosition[] = [-1, -1], $ModArray[1] = [0], $ModRemovedArray[1] = [0], $ListOverwrite[0][5]
	Global $ModEditorButton[0], $ModEditorEdit[0], $ModEditorText[0], $ModEditorTreeView[0], $ModEditorTreeViewItem[0], $ModName[0], $ModStatus[0], $ModRemovedStatus[0], $ModRemovedName[0], $ModLink[0], $ModAuthor[0], $ModESP[0], $ModScripts[0]
	Global $ModLogo[0], $ModSize[0], $ModState[0], $ModChecked[0], $ModListArray[0], $ModToolTip[0], $ModQuality[0], $ListOptimize[0][3], $ListViewItem[0], $HelpListViewItem[0], $ListViewItemContextMenu[0], $MenuListPathAll[0], $MenuListPathAllUnspec[0]
	Global $MenuListCheckAll[0], $MenuListUncheckAll[0], $MenuListClearCurrent[0], $MenuListClearAllEx[0], $MenuListClearAll[0], $MenuListSpecifyPath[0], $MenuListEditor[0], $MenuListDownload[0], $MenuListOpen[0], $GUIModEditor[0], $ListRadio[0]
	If $PreviousWindowSettings Then
		$PrevWinPos = StringSplit(StringStripWS($PreviousWindowSettings, 8), ',')
		If $PrevWinPos[0] > 3 Then
			If $PrevWinPos[1] < 675 Then $PrevWinPos[1] = 675
			If $PrevWinPos[1] > @DesktopWidth Then $PrevWinPos[1] = 1000
			If $PrevWinPos[2] < 290 Then $PrevWinPos[2] = 290
			If $PrevWinPos[2] > @DesktopHeight Then $PrevWinPos[2] = 664
			If $PrevWinPos[3] < 0 Then $PrevWinPos[3] = 0
			If $PrevWinPos[3] > @DesktopWidth - $PrevWinPos[1] Then $PrevWinPos[3] = @DesktopWidth - $PrevWinPos[1]
			If $PrevWinPos[4] < 0 Then $PrevWinPos[4] = 0
			If $PrevWinPos[4] > @DesktopHeight - $PrevWinPos[2] Then $PrevWinPos[4] = @DesktopHeight - $PrevWinPos[2]
			Global $GUIMainSize[] = [$PrevWinPos[1], $PrevWinPos[2]], $GUIHelpSize[] = [$PrevWinPos[1], $PrevWinPos[2]], $GUIMainPosition[] = [$PrevWinPos[3], $PrevWinPos[4]]
		EndIf
	EndIf
	Global $ButtonLen[] = [StringLen($_BeginButton), StringLen($_CancelButton), StringLen($_Interrupt)]
	Global $BeginButtonWidth = _ArrayMax($ButtonLen, 1) * 6 + 10
	Global $ColorArray, $ColorFoundBoth, $ColorFoundArchived, $ColorFoundExtracted, $ColorFoundUnsupported, $ColorFoundNone, $ColorContainsBoth, $ColorContainsESP, $ColorContainsScripts
#EndRegion Variables

#Region Includes Overwritten
	#include <GDIPlus.au3>
	#include <SQLite.au3>
	#include <Include\Icons.au3>
#EndRegion Includes Overwritten

#Region Main
	CheckAppConfigUpdate()
	If @Compiled = 0 Then
		IniWriteSection($AppConfig, 'Checksums', '')
		$FileList = _FileListToArray(@ScriptDir & '\Pics\', '*.jpg', 1)
		If $FileList <> 0 Then
			For $i = 1 To $FileList[0]
				IniWrite($AppConfig, 'Checksums', 'Pics\' & $FileList[$i], FileGetSize(@ScriptDir & '\Pics\' & $FileList[$i]))
			Next
		EndIf
		$FileList = _FileListToArray(@ScriptDir & '\Lang\', '*.lng', 1)
		If $FileList <> 0 Then
			For $i = 1 To $FileList[0]
				IniWrite($AppConfig, 'Checksums', 'Lang\' & $FileList[$i], FileGetSize(@ScriptDir & '\Lang\' & $FileList[$i]))
			Next
		EndIf
		$FileList = _FileListToArray(@ScriptDir & '\List\', '*.lst', 1)
		If $FileList <> 0 Then
			For $i = 1 To $FileList[0]
				IniWrite($AppConfig, 'Checksums', 'List\' & $FileList[$i], FileGetSize(@ScriptDir & '\List\' & $FileList[$i]))
			Next
		EndIf
	EndIf
	Global $AppBuild = (@AutoItX64 ? 'x64 ' & $_Language & ' (' & $AppBuildTime[2] & '.' & $AppBuildTime[1] & '.' & StringRight($AppBuildTime[0], 2) & ')' : 'x86 ' & $_Language & ' (' & $AppBuildTime[2] & '.' & $AppBuildTime[1] & '.' & StringRight($AppBuildTime[0], 2) & ')')
	Global $ModFolder = @ScriptDir
	Global $Date = @YEAR & '_' & @MON & '_' & @MDAY & '-' & @HOUR & '_' & @MIN & '_' & @SEC
	If $NoLogging <> 1 Then
		Global $LogPath = @ScriptDir & '\Logs\' & $Date & '.log'
		DirCreate(@ScriptDir & '\Logs\')
	EndIf
	Global $x = 2, $y, $yh = 21, $xw = $ModNameLongest + 10 + 3 * 5.5 + 10, $n = 21
	If StringRight($ListPath, 4) <> '.lst' Then $ListPath &= '.lst'
	If StringLeft($ListPath, 2) <> '\\' And StringRight(StringLeft($ListPath, 3), 2) <> ':\' Then $ListPath = @ScriptDir & '\List\' & $ListPath
	AutoItWinSetTitle(@ScriptName)
	LogWrite(@ScriptName & ' ' & $AppVer[1] & '.' & $AppVer[2] & '.' & $AppVer[3] & '.' & $AppVer[4] & ' ' & $AppBuild & ' @ ' & @OSVersion & ' ' & @OSBuild & ' ' & @OSArch & ' ' & @OSLang & @CRLF)
	CheckAppUpdate()
	GUIColorsDefine()
	MainCreate()
#EndRegion Main

#Region Functions
	Func CheckAppConfigUpdate()
		If @Compiled And $NoAutoUpdate <> 1 Then
			CheckFileUpdate($TempFile, 1)
			GUISetCursor(15)
			If FileExists($TempFile) And IniRead($TempFile, 'System', 'DefinitionsVersion', 0) > $DefinitionsVersion Then
				$SectionNamesArray = IniReadSectionNames($TempFile)
				If Not @error Then
					For $i = 1 To $SectionNamesArray[0]
						If StringCompare($SectionNamesArray[$i], 'System') = 0 Then
							IniWrite($AppConfig, 'System', 'ListPath', IniRead($TempFile, 'System', 'ListPath', $ListPath))
							IniWrite($AppConfig, 'System', 'ListLink', IniRead($TempFile, 'System', 'ListLink', $ListLink))
							IniWrite($AppConfig, 'System', 'LinkHome', IniRead($TempFile, 'System', 'LinkHome', $LinkHome))
							IniWrite($AppConfig, 'System', 'LinkUpdate', IniRead($TempFile, 'System', 'LinkUpdate', $LinkUpdate))
							IniWrite($AppConfig, 'System', 'LinkComponents', IniRead($TempFile, 'System', 'LinkComponents', $LinkComponents))
							IniWrite($AppConfig, 'System', 'LinkPics', IniRead($TempFile, 'System', 'LinkPics', $LinkPics))
							IniWrite($AppConfig, 'System', 'VersionLatest', IniRead($TempFile, 'System', 'VersionLatest', $VersionLatest))
							IniWrite($AppConfig, 'System', 'DefinitionsVersion', IniRead($TempFile, 'System', 'DefinitionsVersion', $DefinitionsVersion))
						Else
							If StringCompare($SectionNamesArray[$i], 'Defined Paths') <> 0 Then IniWriteSection($AppConfig, $SectionNamesArray[$i], IniReadSection($TempFile, $SectionNamesArray[$i]))
						EndIf
					Next
				EndIf
				$ListPath = IniRead($AppConfig, 'System', 'ListPath', 'SMC')
				If StringRight($ListPath, 4) <> '.lst' Then $ListPath &= '.lst'
				If StringLeft($ListPath, 2) <> '\\' And StringRight(StringLeft($ListPath, 3), 2) <> ':\' Then $ListPath = @ScriptDir & '\List\' & $ListPath
				$ListLink = IniRead($AppConfig, 'System', 'ListLink', '')
				$DefinitionsVersion = IniRead($AppConfig, 'System', 'DefinitionsVersion', 0)
				$LinkHome = IniRead($AppConfig, 'System', 'LinkHome', $LinkHome)
				$LinkUpdate = IniRead($AppConfig, 'System', 'LinkUpdate', $LinkUpdate)
				$LinkComponents = IniRead($AppConfig, 'System', 'LinkComponents', $LinkComponents)
				$LinkPics = IniRead($AppConfig, 'System', 'LinkPics', $LinkPics)
				$VersionLatest = IniRead($AppConfig, 'System', 'VersionLatest', $VersionLatest)
				CheckFileUpdate($AppLang)
				$AppConfigUpdated = 1
			EndIf
			FileDelete($TempFile)
			GUISetCursor(2)
		EndIf
	EndFunc   ;==>CheckAppConfigUpdate

	Func CheckAppUpdate()
		If @Compiled And $NoAutoUpdate <> 1 Then
			If $VersionLatest > $AppVer[1] & '.' & $AppVer[2] & '.' & $AppVer[3] & '.' & $AppVer[4] And InetGetSize($LinkUpdate & 'SMC_' & $VersionLatest & '.7z', 1) Then
				$UpdateCancel = 0
				$_UpdateMsg = @CRLF & 'SMC ' & $_UpdateAvailable & '!' & @CRLF & @CRLF & $_VersionCurrent & ' = ' & $AppVer[1] & '.' & $AppVer[2] & '.' & $AppVer[3] & '.' & $AppVer[4] & @CRLF & $_VersionLatest & ' = ' & $VersionLatest & @CRLF
				$AppLangTempSection = IniReadSection($AppLang, 'Changelog')
				If Not @error Then $_UpdateMsg &= @CRLF & $_VersionLatestChanges & ':' & @CRLF & StringReplace($AppLangTempSection[1][1], '|', @CRLF) & @CRLF
				$_UpdateMsg &= @CRLF & $_UpdateProceed & '?'
				$GUIUpdateTempWidth = _StringSize($_UpdateMsg, 10, Default, Default, 'Calibri')[2] + 50
				If $GUIUpdateTempWidth > 400 And $GUIUpdateTempWidth < 800 Then $GUIUpdateSize[0] = $GUIUpdateTempWidth
				$GUIUpdateTempHeight = _StringSize($_UpdateMsg, 10, Default, Default, 'Calibri', $GUIUpdateSize[0] - 10)[3] + 70 + $yh
				If $GUIUpdateSize[1] < $GUIUpdateTempHeight Then $GUIUpdateSize[1] = $GUIUpdateTempHeight
				$GUIUpdate = GUICreate('SMC ' & $_UpdateAvailable, $GUIUpdateSize[0], $GUIUpdateSize[1])
				GUISetFont(10, Default, Default, 'Calibri', $GUIUpdate, 6)
				GUICtrlCreateLabel($_UpdateMsg, 0, 0, $GUIUpdateSize[0], $GUIUpdateSize[1] - 3 * $yh + 10, $SS_CENTER, 0)
				If StringInStr('WIN_7 WIN_8 WIN_81 WIN_10 WIN_2008 WIN_2008R2 WIN_2012 WIN_2012R2 WIN_2016', @OSVersion) Then
					$sDLL = "imageres.dll"
					$iIcon_Style = 106
				Else
					$sDLL = 'user32.dll'
					$iIcon_Style = -5
				EndIf
				GUICtrlCreateIcon($sDLL, $iIcon_Style, 10, 10, 32, 32)
				$UpdateProgress = GUICtrlCreatePic('', 2, $GUIUpdateSize[1] - (55 + $yh), $GUIUpdateSize[0] - 4, $yh - 1)
				GUICtrlSetState(-1, $GUI_DISABLE)
				$UpdateButtonProceed = GUICtrlCreateButton($_Proceed, $GUIUpdateSize[0] / 2 - 105, $GUIUpdateSize[1] - 35, 100, 25, $BS_DEFPUSHBUTTON)
				GUICtrlSetOnEvent(-1, 'CheckAppUpdateProceed')
				$UpdateButtonSkip = GUICtrlCreateButton($_Skip, $GUIUpdateSize[0] / 2 + 5, $GUIUpdateSize[1] - 35, 100, 25)
				GUICtrlSetOnEvent(-1, 'UpdateCancel')
				GUISetOnEvent($GUI_EVENT_CLOSE, 'GUIClose', $GUIUpdate)
				_ITaskBar_CreateTaskBarObj()
				GUISetCursor(2, 1, $GUILoading)
				GUISetState(@SW_HIDE, $GUILoading)
				GUISetState(@SW_SHOW, $GUIUpdate)
				ProgressPaint(GUICtrlGetHandle($UpdateProgress), 0, 0)
				While Not $UpdateCancel
					Sleep(100)
				WEnd
				GUIDelete($GUIUpdate)
				GUISetState(@SW_SHOW, $GUILoading)
				GUISetCursor(1, 1, $GUILoading)
			EndIf
		EndIf
	EndFunc   ;==>CheckAppUpdate

	Func CheckAppUpdateProceed()
		GUISetCursor(15)
		GUICtrlSetState($UpdateButtonProceed, $GUI_HIDE)
		GUICtrlSetPos($UpdateButtonSkip, $GUIUpdateSize[0] / 2 - ((StringLen($_Interrupt) * 6 + 10 > 210) ? (StringLen($_Interrupt) * 6 + 10) / 2 : 105), $GUIUpdateSize[1] - 35, ((StringLen($_Interrupt) * 6 + 10 > 210) ? (StringLen($_Interrupt) * 6 + 10) : 210), 25)
		GUICtrlSetState($UpdateButtonSkip, $GUI_DISABLE)
		GUICtrlSetData($UpdateButtonSkip, $_Interrupt)
		_HotKey_Assign(BitOR($CK_CONTROL, $CK_ALT, 0x58), 'UpdateCancel', $HK_FLAG_DEFAULT, $GUIMain)
		LogWrite('[U] ' & $_AppUpdate & @CRLF)
		Local $AppUpdateDownloaded = 0
		Local $AppUpdateDownload = InetGet($LinkUpdate & 'SMC_' & $VersionLatest & '.7z', $TempFile, 16, 1)
		If Not @error Then
			LogWrite(' [+]' & @CRLF)
			LogWrite('  |- ' & $LinkUpdate & 'SMC_' & $VersionLatest & '.7z' & @CRLF)
			While Not InetGetInfo($AppUpdateDownload, 2) And Not $UpdateCancel
				If InetGetInfo($AppUpdateDownload, 1) And $AppUpdateDownloaded <> Int(InetGetInfo($AppUpdateDownload, 0) / InetGetInfo($AppUpdateDownload, 1) * 100) Then
					$AppUpdateDownloaded = Int(InetGetInfo($AppUpdateDownload, 0) / InetGetInfo($AppUpdateDownload, 1) * 100)
					ProgressPaint(GUICtrlGetHandle($UpdateProgress), $AppUpdateDownloaded, 0)
					If $TaskBarCompat = 1 Then
						If $AppUpdateDownloaded = 0 Then
							_ITaskBar_SetProgressState($GUIUpdate)
						Else
							_ITaskBar_SetProgressValue($GUIUpdate, $AppUpdateDownloaded)
						EndIf
					EndIf
				EndIf
				Sleep(100)
			WEnd
			InetClose($AppUpdateDownload)
			If $UpdateCancel Then
				LogWrite(' [X] ' & $_UpdateCancelled & @CRLF)
			Else
				LogWrite(' [-]' & @CRLF)
				If FileExists($TempFile) Then
					While Not FileExists($7zExe) Or Not FileExists(StringTrimRight($7zExe, 4) & '.dll')
						If _ExtMsgBox(48, $_Quit & '|&' & $_Proceed, $_Warning, $_7zWarning & ' :(' & @CRLF & @CRLF & $_SelectManually & '...') = 1 Then ExitClean(2)
						WinFlash($GUIUpdate, '', 3, 400)
						$7zExe = FileOpenDialog($_Choose7zExe, @ScriptDir & '\', $_Executable & ' (*.exe)', 3)
					WEnd
					DirCreate($TempDir & '\SMC_Update')
					If RunProcessing($TempFile, $TempDir & '\SMC_Update') = 0 And FileExists(@ScriptDir & '\SMC_Launcher.exe') Then
						ProcessClose('SMC_Launcher.exe')
						ProcessWaitClose('SMC_Launcher.exe')
						FileDelete(@ScriptDir & '\SMC_Launcher.exe')
						FileMove($TempDir & '\SMC_Update\SMC_Launcher.exe', @ScriptDir & '\SMC_Launcher.exe', 1)
						FileDelete(@ScriptDir & '\readme.md')
						FileMove($TempDir & '\SMC_Update\readme.md', @ScriptDir & '\readme.md', 1)
						FileMove($TempDir & '\SMC_Update\Pics\*.jpg', @ScriptDir & '\Pics\', 9)
						FileMove($TempDir & '\SMC_Update\Lang\*.lng', @ScriptDir & '\Lang\', 9)
						FileMove($TempDir & '\SMC_Update\List\*.lst', @ScriptDir & '\List\', 9)
					EndIf
					DirRemove($TempDir & '\SMC_Update', 1)
					ExitClean(3)
				EndIf
			EndIf
		Else
			LogWrite(' [X] ' & $_AppUpdateError & @CRLF)
		EndIf
		_HotKey_Assign(BitOR($CK_CONTROL, $CK_ALT, 0x58))
		GUISetCursor(2)
		UpdateCancel()
	EndFunc   ;==>CheckAppUpdateProceed

	Func CheckCPCurArchExists($CPCurArchSplit, $i1, $i2, $i4 = -1)
		Local $CheckPaths[18]
		If StringInStr('.zip .7z .rar', StringRight($CPCurArchSplit, 4)) = 0 Then $CPCurArchSplit &= '.*'
		$CheckPaths[0] = _GUICtrlListView_GetItemText($ListView, $i1 - 1, 1) & '\'
		$CheckPaths[1] = _GUICtrlListView_GetItemText($ListView, $i1 - 1, 1) & '\' & $ModArray[$i1] & '\'
		$CheckPaths[2] = _GUICtrlListView_GetItemText($ListView, $i1 - 1, 1) & '\' & $RequiredModsPath & '\'
		$CheckPaths[3] = _GUICtrlListView_GetItemText($ListView, $i1 - 1, 1) & '\' & $RequiredModsPath & '\' & $ModArray[$i1] & '\'
		$CheckPaths[4] = _GUICtrlListView_GetItemText($ListView, $i1 - 1, 1) & '\' & $OptionalModsPath & '\'
		$CheckPaths[5] = _GUICtrlListView_GetItemText($ListView, $i1 - 1, 1) & '\' & $OptionalModsPath & '\' & $ModArray[$i1] & '\'
		$CheckPaths[6] = _GUICtrlListView_GetItemText($ListView, $i2 - 1, 1) & '\'
		$CheckPaths[7] = _GUICtrlListView_GetItemText($ListView, $i2 - 1, 1) & '\' & $ModArray[$i2] & '\'
		$CheckPaths[8] = _GUICtrlListView_GetItemText($ListView, $i2 - 1, 1) & '\' & $RequiredModsPath & '\'
		$CheckPaths[9] = _GUICtrlListView_GetItemText($ListView, $i2 - 1, 1) & '\' & $RequiredModsPath & '\' & $ModArray[$i2] & '\'
		$CheckPaths[10] = _GUICtrlListView_GetItemText($ListView, $i2 - 1, 1) & '\' & $OptionalModsPath & '\'
		$CheckPaths[11] = _GUICtrlListView_GetItemText($ListView, $i2 - 1, 1) & '\' & $OptionalModsPath & '\' & $ModArray[$i2] & '\'
		For $i3 = 0 To 11
			If _FO_FileSearch($CheckPaths[$i3], $CPCurArchSplit, True, 0, 2, 0) Then Return $CheckPaths[$i3] & $CPCurArchSplit
			If _FO_FileSearch($CheckPaths[$i3], StringReplace($CPCurArchSplit, ' ', '_'), True, 0, 2, 0) Then Return $CheckPaths[$i3] & StringReplace($CPCurArchSplit, ' ', '_')
		Next
		If $i4 <> -1 Then
			$CheckPaths[12] = _GUICtrlListView_GetItemText($ListView, $i4 - 1, 1) & '\'
			$CheckPaths[13] = _GUICtrlListView_GetItemText($ListView, $i4 - 1, 1) & '\' & $ModArray[$i4] & '\'
			$CheckPaths[14] = _GUICtrlListView_GetItemText($ListView, $i4 - 1, 1) & '\' & $RequiredModsPath & '\'
			$CheckPaths[15] = _GUICtrlListView_GetItemText($ListView, $i4 - 1, 1) & '\' & $RequiredModsPath & '\' & $ModArray[$i4] & '\'
			$CheckPaths[16] = _GUICtrlListView_GetItemText($ListView, $i4 - 1, 1) & '\' & $OptionalModsPath & '\'
			$CheckPaths[17] = _GUICtrlListView_GetItemText($ListView, $i4 - 1, 1) & '\' & $OptionalModsPath & '\' & $ModArray[$i4] & '\'
			For $i3 = 12 To 17
				If _FO_FileSearch($CheckPaths[$i3], $CPCurArchSplit, True, 0, 2, 0) Then Return $CheckPaths[$i3] & $CPCurArchSplit
				If _FO_FileSearch($CheckPaths[$i3], StringReplace($CPCurArchSplit, ' ', '_'), True, 0, 2, 0) Then Return $CheckPaths[$i3] & StringReplace($CPCurArchSplit, ' ', '_')
			Next
		EndIf
		Return ''
	EndFunc   ;==>CheckCPCurArchExists

	Func CheckDLC($ListContents)
		If @Compiled And $ForceEnableDLC <> 1 Then
			If FileExists(@LocalAppDataDir & '\Skyrim\plugins.txt') Then
				LogWrite('[>] ' & $_DLCFound & ':' & @CRLF)
				LogWrite(' [+]' & @CRLF)
				For $i = 0 To UBound($SkyrimDLC) - 1
					Local $Plugins = FileOpen(@LocalAppDataDir & '\Skyrim\plugins.txt')
					If $Plugins <> -1 Then
						While Not @error
							Local $CurrentLine = FileReadLine($Plugins)
							If $CurrentLine = $SkyrimDLC[$i][1] And _ArraySearch($ListContents, '[Skyrim_' & $SkyrimDLC[$i][0] & ']', 1) Then
								$SkyrimDLC[$i][2] = 1
								LogWrite('  |- ' & $SkyrimDLC[$i][0] & @CRLF)
								ExitLoop
							EndIf
						WEnd
					EndIf
					FileClose($Plugins)
				Next
				LogWrite(' [-]' & @CRLF)
			EndIf
		Else
			LogWrite('[>] ' & $_DLCFound & ':' & @CRLF)
			LogWrite(' [+]' & @CRLF)
			For $i = 0 To UBound($SkyrimDLC) - 1
				$SkyrimDLC[$i][2] = 1
				LogWrite('  |- ' & $SkyrimDLC[$i][0] & @CRLF)
			Next
			LogWrite(' [-]' & @CRLF)
		EndIf
	EndFunc   ;==>CheckDLC

	Func CheckFileUpdate($DownloadPath, $NotExistsOnly = 0)
		If @Compiled Then
			GUISetCursor(15)
			If $NotExistsOnly Then
				If FileExists($DownloadPath) <> 1 Then
					$DownloadHandle = InetGet($LinkComponents & 'settings.ini', $DownloadPath, 16)
					InetClose($DownloadHandle)
				EndIf
			Else
				If $NoAutoUpdate <> 1 Then
					If StringLeft($DownloadPath, StringInStr($DownloadPath, '\', 0, -2)) = @ScriptDir & '\' Then
						If FileExists($DownloadPath) <> 1 Or FileGetSize($DownloadPath) <> IniRead($AppConfig, 'Checksums', StringTrimLeft($DownloadPath, StringInStr($DownloadPath, '\', 0, -2)), '') Then
							If StringSplit($DownloadPath, '\')[0] > 0 And FileExists(@ScriptDir & '\' & StringSplit($DownloadPath, '\')[StringSplit($DownloadPath, '\')[0] - 1]) = 0 Then DirCreate(@ScriptDir & '\' & StringSplit($DownloadPath, '\')[StringSplit($DownloadPath, '\')[0] - 1])
							$DownloadHandle = InetGet($LinkComponents & StringReplace(StringTrimLeft($DownloadPath, StringInStr($DownloadPath, '\', 0, -2)), '\', '/'), $DownloadPath, 16)
							InetClose($DownloadHandle)
						EndIf
					Else
						If FileExists($DownloadPath) <> 1 Or FileGetSize($DownloadPath) <> IniRead($AppConfig, 'Checksums', StringTrimLeft($DownloadPath, StringInStr($DownloadPath, '\', 0, -1)), '') Then
							InetGet($LinkComponents & StringTrimLeft($DownloadPath, StringInStr($DownloadPath, '\', 0, -1)), $DownloadPath, 16)
							InetClose($DownloadHandle)
						EndIf
					EndIf
				EndIf
			EndIf
			GUISetCursor(2)
		EndIf
	EndFunc   ;==>CheckFileUpdate

	Func CheckLanguageSystem()
		Select
			Case StringInStr('0419', @OSLang)
				Return 'ru'
			Case StringInStr('0407 0807 0c07 1007 1407', @OSLang)
				Return 'de'
			Case StringInStr('040c 080c 0c0c 100c 140c 180c', @OSLang)
				Return 'fr'
			Case StringInStr('0004 0404 0804 0C04 1004 1004 7C04', @OSLang)
				Return 'ch'
			Case StringInStr('0410 0810', @OSLang)
				Return 'it'
			Case StringInStr('040a 080a 0c0a 100a 140a 180a 1c0a 200a 240a 280a 2c0a 300a 340a 380a 3c0a 400a 440a 480a 4c0a 500a', @OSLang)
				Return 'es'
			Case StringInStr('0405', @OSLang)
				Return 'cs'
			Case StringInStr('0415', @OSLang)
				Return 'pl'
			Case StringInStr('0411', @OSLang)
				Return 'ja'
			Case Else
				Return 'en'
		EndSelect
	EndFunc   ;==>CheckLanguageSystem

	Func CheckModCurArchExists($i)
		Local $CheckPaths[6], $InputPath, $ModCurArch
		$InputPath = _GUICtrlListView_GetItemText($ListView, $i, 1) & (StringCompare(StringRight(_GUICtrlListView_GetItemText($ListView, $i, 1), 1), '\') = 0 ? '' : '\')
		$CheckPaths[0] = $InputPath
		$CheckPaths[1] = $InputPath & StringReplace($ModArray[$i + 1] & '\', '\\', '\')
		$CheckPaths[2] = $InputPath & StringReplace($RequiredModsPath & '\', '\\', '\')
		$CheckPaths[3] = $InputPath & StringReplace($RequiredModsPath & '\' & $ModArray[$i + 1] & '\', '\\', '\')
		$CheckPaths[4] = $InputPath & StringReplace($OptionalModsPath & '\', '\\', '\')
		$CheckPaths[5] = $InputPath & StringReplace($OptionalModsPath & '\' & $ModArray[$i + 1] & '\', '\\', '\')
		$Unsupported = 0
		For $i1 = 0 To 3
			If IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[$i1] & 'Archives', '') Then
				$ModCurArch = IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[$i1] & 'Archives', '')
				$ModCurArch = StringSplit($ModCurArch, ';')
				For $i2 = 1 To $ModCurArch[0]
					Local $ModCurArchSplit = StringSplit($ModCurArch[$i2], '|')
					$ModCurArchSplit[1] = StringStripWS($ModCurArchSplit[1], 3)
					If StringInStr('.zip .7z .rar', StringRight($ModCurArchSplit[1], 4)) = 0 Then $ModCurArchSplit[1] &= '.*'
					For $i3 = 0 To 5
						If _FO_FileSearch($CheckPaths[$i3], $ModCurArchSplit[1], True, 0, 2, 0) Or _FO_FileSearch($CheckPaths[$i3], StringReplace($ModCurArchSplit[1], ' ', '_'), True, 0, 2, 0) Then
							$Path = $CheckPaths[$i3]
							$Quality = $QualityArray[$i1]
							$ModQuality[$i] = $Quality
							Return 1
						EndIf
					Next
				Next
			EndIf
		Next
		If IniRead($AppConfig, $ModArray[$i + 1], 'Unsupported', '') Then
			$ModCurArch = IniRead($AppConfig, $ModArray[$i + 1], 'Unsupported', '')
			$ModCurArch = StringSplit($ModCurArch, ';')
			For $i1 = 1 To $ModCurArch[0]
				$ModCurArch[$i1] = StringStripWS($ModCurArch[$i1], 3)
				If StringInStr('.zip .7z .rar', StringRight($ModCurArch[$i1], 4)) = 0 Then $ModCurArch[$i1] &= '.*'
				For $i2 = 0 To 5
					If _FO_FileSearch($CheckPaths[$i2], $ModCurArch[$i1], True, 0, 2, 0) Or _FO_FileSearch($CheckPaths[$i2], StringReplace($ModCurArch[$i1], ' ', '_'), True, 0, 2, 0) Then
						$Unsupported = 1
						ExitLoop
					EndIf
				Next
			Next
		EndIf
	EndFunc   ;==>CheckModCurArchExists

	Func CheckModPathExists($i)
		Local $CheckPaths[6], $InputPath
		$InputPath = _GUICtrlListView_GetItemText($ListView, $i, 1) & (StringCompare(StringRight(_GUICtrlListView_GetItemText($ListView, $i, 1), 1), '\') = 0 ? '' : '\')
		$CheckPaths[0] = $InputPath
		$CheckPaths[1] = $InputPath & StringReplace($ModArray[$i + 1] & '\', '\\', '\')
		$CheckPaths[2] = $InputPath & StringReplace($RequiredModsPath & '\', '\\', '\')
		$CheckPaths[3] = $InputPath & StringReplace($RequiredModsPath & '\' & $ModArray[$i + 1] & '\', '\\', '\')
		$CheckPaths[4] = $InputPath & StringReplace($OptionalModsPath & '\', '\\', '\')
		$CheckPaths[5] = $InputPath & StringReplace($OptionalModsPath & '\' & $ModArray[$i + 1] & '\', '\\', '\')
		$Path = ''
		$PathExistsSize = 0
		If _SQLite_Query(-1, 'SELECT DISTINCT [FromFirst] FROM [SMC] WHERE [Mod]="' & $ModArray[$i + 1] & '"', $Query) <> $SQLITE_OK Then
			_ExtMsgBox(16, '&' & $_Quit, $_Error, StringUpper(StringLeft($_SQLiteError, 1)) & StringTrimLeft($_SQLiteError, 1) & '=' & _SQLite_ErrCode() & ' (' & _SQLite_ErrMsg() & ')')
			ExitClean(1)
		Else
			While _SQLite_FetchData($Query, $Row) = $SQLITE_OK
				For $i1 = 0 To 5
					If FileExists($CheckPaths[$i1] & $Row[0]) Then
						$Path = $CheckPaths[$i1]
						ExitLoop
					EndIf
				Next
			WEnd
			_SQLite_QueryFinalize($Query)
		EndIf
		If $Path Then
			If _SQLite_Query(-1, 'SELECT DISTINCT [From] FROM [SMC] WHERE [Mod]="' & $ModArray[$i + 1] & '"', $Query) <> $SQLITE_OK Then
				_ExtMsgBox(16, '&' & $_Quit, $_Error, StringUpper(StringLeft($_SQLiteError, 1)) & StringTrimLeft($_SQLiteError, 1) & '=' & _SQLite_ErrCode() & ' (' & _SQLite_ErrMsg() & ')')
				ExitClean(1)
			Else
				While _SQLite_FetchData($Query, $Row) = $SQLITE_OK
					If StringRight($Row[0], 3) <> '*.*' Then
						$PathExistsSize += FileGetSize($Path & $Row[0])
					Else
						$PathExistsSize += DirGetSize($Path & StringTrimRight($Row[0], 3), 2)
					EndIf
				WEnd
				_SQLite_QueryFinalize($Query)
			EndIf
			$PathExistsSize = Round($PathExistsSize / 1048576, 2)
			Return 1
		EndIf
	EndFunc   ;==>CheckModPathExists

	Func CheckNMM()
		$NMMClientModsFolder = IniRead($AppConfig, 'System', 'PreviousNNMPath', '')
		If FileExists($NMMClientModsFolder) Then
			Return 1
		Else
			$NMMClientModsFolder = 0
			$FileFindLast = 0
			$NMMPath1Handle = FileFindFirstFile($NMMPath & '*')
			If $NMMPath1Handle <> -1 Then
				While 1
					$FileFindNextHandle = FileFindNextFile($NMMPath1Handle)
					If @error Then ExitLoop
					If StringLeft($FileFindNextHandle, 15) = 'NexusClient.exe' Then
						$NMMPathClient = $NMMPath & $FileFindNextHandle & '\'
						$NMMPath2Handle = FileFindFirstFile($NMMPathClient & '*')
						If $NMMPath2Handle <> -1 Then
							While 1
								$FileFindNextHandle = FileFindNextFile($NMMPath2Handle)
								If @error Then ExitLoop
								$FileFindLast = $FileFindNextHandle
							WEnd
							If $FileFindLast Then
								$NMMPathFull = $NMMPathClient & $FileFindLast & '\'
								If FileExists($NMMPathFull & 'user.config') Then
									$NMMPathHandle = FileOpen($NMMPathFull & 'user.config', 0)
									If $NMMPathHandle <> -1 Then
										$FileReadLine = 0
										While 1
											$NMMFileLine = FileReadLine($NMMPathHandle)
											If Not @error Then
												$FileReadLine += 1
												If StringInStr($NMMFileLine, 'setting name="ModFolder"') Then $NMMLocated = 1
												If $NMMLocated = 1 And StringInStr($NMMFileLine, 'modeId="Skyrim"') And StringInStr(FileReadLine($NMMPathHandle, $FileReadLine + 1), '<string>') Then
													$NMMClientModsFolderTemp = StringTrimRight(StringTrimLeft(StringStripWS(FileReadLine($NMMPathHandle, $FileReadLine + 1), 3), 8), 9)
													If FileExists($NMMClientModsFolderTemp) Then
														$NMMClientModsFolder = $NMMClientModsFolderTemp
														FileClose($NMMPathHandle)
														FileClose($NMMPath2Handle)
														FileClose($NMMPath1Handle)
														IniWrite($AppConfig, 'System', 'PreviousNNMPath', $NMMClientModsFolder)
														Return 1
													EndIf
													$NMMLocated = 0
													ExitLoop
												EndIf
												If $NMMLocated = 1 And StringInStr($NMMFileLine, '</setting>') Then ExitLoop
											Else
												ExitLoop
											EndIf
										WEnd
									EndIf
									FileClose($NMMPathHandle)
								EndIf
							EndIf
						EndIf
						FileClose($NMMPath2Handle)
					EndIf
				WEnd
			EndIf
			FileClose($NMMPath1Handle)
		EndIf
	EndFunc   ;==>CheckNMM

	Func CheckOverwrite($i, $MoveFrom, $OutputPath, $MoveI = 0, $MoveC = 0)
		If UBound($ListOverwrite) > 0 Then
			For $i1 = 0 To UBound($ListOverwrite) - 1
				If $ModArray[$i + 1] = $ListOverwrite[$i1][0] Then
					Local $ListOverwriteCounter = 0
					Local $ListOverwriteSplit = StringSplit($ListOverwrite[$i1][1], ';')
					If $ListOverwriteSplit[1] <> '' Then
						For $i2 = 1 To $ListOverwriteSplit[0]
							For $i3 = 0 To UBound($SkyrimDLC) - 1
								If $ListOverwriteSplit[$i2] = $SkyrimDLC[$i3][0] And $SkyrimDLC[$i3][2] = 1 Then
									$ListOverwriteCounter += 1
									ExitLoop
								EndIf
							Next
						Next
					Else
						$ListOverwriteSplit[0] = 0
					EndIf
					If $ListOverwriteCounter = $ListOverwriteSplit[0] Then
						$ListOverwriteCounter = 0
						$ListOverwriteTo = StringSplit($ListOverwrite[$i1][2], ';')
						For $i2 = 1 To $ListOverwriteTo[0]
							If $MoveI <> 0 And IsArray($MoveI) And $MoveI[0] > 1 Then
								Local $MoveITemp = $MoveI
								For $i3 = 2 To $MoveITemp[0]
									If MoveFFTIC(0, $MoveFrom, '\' & $ListOverwriteTo[$i2], $OutputPath & StringTrimLeft($ListOverwriteTo[$i2], StringInStr($ListOverwriteTo[$i2], '\', 0, -1)), $MoveITemp, $MoveC) Then $ListOverwriteCounter += 1
									_ArrayDelete($MoveITemp, 2)
									$MoveITemp[0] -= 1
								Next
							Else
								If MoveFFTIC(0, $MoveFrom, '\' & $ListOverwriteTo[$i2], $OutputPath & StringTrimLeft($ListOverwriteTo[$i2], StringInStr($ListOverwriteTo[$i2], '\', 0, -1)), 0, $MoveC) Then $ListOverwriteCounter += 1
							EndIf
						Next
						If $ListOverwriteCounter > 0 Then ExitLoop
					EndIf
				EndIf
			Next
		EndIf
	EndFunc   ;==>CheckOverwrite

	Func CheckPath($i)
		Select
			Case CheckModPathExists($i) And CheckModCurArchExists($i)
				GUICtrlSetBkColor($ListViewItem[$i], $ColorFoundBoth)
				$ModState[$i] = $_CanBeCombined & ' [' & $_FoundBoth & ']'
				For $i1 = 0 To 3
					If $ModQuality[$i] = $QualityArray[$i1] Then
						_GUICtrlListView_SetItemText($ListView, $i, '[' & $QualityArray[$i1] & ']', 5 - $i1)
					Else
						If StringLeft(_GUICtrlListView_GetItemText($ListView, $i, 5 - $i1), 1) = '[' Then _GUICtrlListView_SetItemText($ListView, $QualityArray[$i1], $i, 5 - $i1)
					EndIf
				Next
				If $PathExistsSize > IniRead($AppConfig, $ModArray[$i + 1], $Quality & 'Size', 0) Then
					Return $PathExistsSize
				Else
					Return IniRead($AppConfig, $ModArray[$i + 1], $Quality & 'Size', 0)
				EndIf
			Case CheckModCurArchExists($i)
				GUICtrlSetBkColor($ListViewItem[$i], $ColorFoundArchived)
				$ModState[$i] = $_CanBeCombined & ' [' & $_FoundArchived & ']'
				For $i1 = 0 To 3
					If $ModQuality[$i] = $QualityArray[$i1] Then
						_GUICtrlListView_SetItemText($ListView, $i, '[' & $QualityArray[$i1] & ']', 5 - $i1)
					Else
						If StringLeft(_GUICtrlListView_GetItemText($ListView, $i, 5 - $i1), 1) = '[' Then _GUICtrlListView_SetItemText($ListView, $i, $QualityArray[$i1], 5 - $i1)
					EndIf
				Next
				Return IniRead($AppConfig, $ModArray[$i + 1], $Quality & 'Size', 0)
			Case CheckModPathExists($i)
				GUICtrlSetBkColor($ListViewItem[$i], $ColorFoundExtracted)
				$ModState[$i] = $_CanBeCombined & ' [' & $_FoundExtracted & ']'
				For $i1 = 0 To 3
					If StringLeft(_GUICtrlListView_GetItemText($ListView, $i, 5 - $i1), 1) = '[' Then _GUICtrlListView_SetItemText($ListView, $i, $QualityArray[$i1], 5 - $i1)
				Next
				Return $PathExistsSize
			Case $Unsupported
				GUICtrlSetState($ListViewItem[$i], $GUI_UNCHECKED)
				GUICtrlSetBkColor($ListViewItem[$i], $ColorFoundUnsupported)
				$ModState[$i] = $_CanNotBeCombined & ' [' & $_FoundUnsupported & ']'
				For $i1 = 0 To 3
					If StringLeft(_GUICtrlListView_GetItemText($ListView, $i, 5 - $i1), 1) = '[' Then _GUICtrlListView_SetItemText($ListView, $i, $QualityArray[$i1], 5 - $i1)
				Next
				Return 0
			Case Else
				GUICtrlSetState($ListViewItem[$i], $GUI_UNCHECKED)
				GUICtrlSetBkColor($ListViewItem[$i], $ColorFoundNone)
				$ModState[$i] = $_CanNotBeCombined & ' [' & $_FoundNone & ']'
				For $i1 = 0 To 3
					If StringLeft(_GUICtrlListView_GetItemText($ListView, $i, 5 - $i1), 1) = '[' Then _GUICtrlListView_SetItemText($ListView, $i, $QualityArray[$i1], 5 - $i1)
				Next
				Return 0
		EndSelect
	EndFunc   ;==>CheckPath

	Func CheckPathsRefresh()
		If $RefreshTrigger = 1 Then
			$RefreshTrigger = 0
			For $i = 0 To $ModArray[0] - 1
				If _GUICtrlListView_GetItemText($ListView, $i, 1) Then
					$ModSize[$i] = CheckPath($i)
					ListViewItemContextMenuChange($i, 1)
				EndIf
			Next
			CheckPathsSize()
			$RefreshTrigger = 1
		EndIf
	EndFunc   ;==>CheckPathsRefresh

	Func CheckPathsSize()
		$CheckedSum = 0
		$CheckedSumSizeAbs = 0
		$SizeUnit = $_SizeMb
		For $i = 0 To $ModArray[0] - 1
			If GUICtrlRead($ListViewItem[$i], 1) = $GUI_CHECKED Then
				$CheckedSum += 1
				If $ModSize[$i] > 0 Then $CheckedSumSizeAbs += $ModSize[$i]
			EndIf
		Next
		If $CheckedSumSizeAbs > 1024 Then
			$CheckedSumSize = Round($CheckedSumSizeAbs / 1024, 2)
			$SizeUnit = $_SizeGb
		Else
			$CheckedSumSize = $CheckedSumSizeAbs
		EndIf
		MainProgressSet(-1, $_StatusMsg & ': ' & $_ModsChecked & ' - ' & $CheckedSum & ', ' & $_ModsCheckedSize & ' - ' & $CheckedSumSize & ' ' & $SizeUnit, -1)
	EndFunc   ;==>CheckPathsSize

	Func CheckWorkingArea()
		Local $dRECT = DllStructCreate('long; long; long; long')
		Local $spiRet = DllCall('User32.dll', 'int', 'SystemParametersInfo', 'uint', 48, 'uint', 0, 'ptr', DllStructGetPtr($dRECT), 'uint', 0)
		If @error Then Return 0
		If $spiRet[0] = 0 Then Return 0
		Local $aRet[4] = [DllStructGetData($dRECT, 1), DllStructGetData($dRECT, 2), DllStructGetData($dRECT, 3), DllStructGetData($dRECT, 4)]
		Return $aRet
	EndFunc   ;==>CheckWorkingArea

	Func CombineBegin()
		MainProgressSet(0, $_StatusMsg & ': ' & $_CombiningStarted, 0)
		ListViewSelectionClear()
		ModLogoSet($ListViewItemSelected, 1)
		Local $CleanInstall = IniRead($AppConfig, 'System', 'CleanInstall', '')
		Local $DDSoptimize = StringInStr('WIN_VISTA WIN_7 WIN_8 WIN_81 WIN_10 WIN_2008 WIN_2008R2 WIN_2012 WIN_2012R2 WIN_2016', @OSVersion) ? IniRead($AppConfig, 'System', 'DDSoptimize', '') : 0
		Local $BSAoptUnpack = IniRead($AppConfig, 'System', 'BSAoptUnpack', '')
		Local $BSAoptPack = IniRead($AppConfig, 'System', 'BSAoptPack', '')
		Local $CreateArchive = IniRead($AppConfig, 'System', 'CreateArchive', '')
		$NoParallax = IniRead($AppConfig, 'System', 'NoParallax', '')
		Local $PreferredOutputPath = IniRead($AppConfig, 'System', 'PreferredOutputPath', '')
		GUICtrlSetState($MainButton, $GUI_DISABLE)
		GUICtrlSetData($MainButton, $_Interrupt)
		If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 2)
		GUISetState(@SW_LOCK)
		GUIHotKeys(1)
		_HotKey_Assign(BitOR($CK_CONTROL, $CK_ALT, 0x58), 'CombineInterrupt', $HK_FLAG_DEFAULT, $GUIMain)
		GUICtrlSetState($MenuMainSettings, $GUI_DISABLE)
		GUICtrlSetState($MenuMainAppPageOpen, $GUI_DISABLE)
		GUICtrlSetState($MenuMainModPageOpen, $GUI_DISABLE)
		GUICtrlSetState($MenuMainHelp, $GUI_DISABLE)
		For $i = 0 To $ModArray[0] - 1
			GUICtrlSetState($MenuListSpecifyPath[$i], $GUI_DISABLE)
			GUICtrlSetState($MenuListEditor[$i], $GUI_DISABLE)
			GUICtrlSetState($MenuListDownload[$i], $GUI_DISABLE)
			GUICtrlSetState($MenuListOpen[$i], $GUI_DISABLE)
			GUICtrlSetState($MenuListCheckAll[$i], $GUI_DISABLE)
			GUICtrlSetState($MenuListUncheckAll[$i], $GUI_DISABLE)
			GUICtrlSetState($MenuListPathAll[$i], $GUI_DISABLE)
			GUICtrlSetState($MenuListPathAllUnspec[$i], $GUI_DISABLE)
			GUICtrlSetState($MenuListClearCurrent[$i], $GUI_DISABLE)
			GUICtrlSetState($MenuListClearAllEx[$i], $GUI_DISABLE)
			GUICtrlSetState($MenuListClearAll[$i], $GUI_DISABLE)
		Next
		GUISetState(@SW_UNLOCK)
		If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 8)
		MainProgressSet(-1, $_StatusMsg & ': ' & $_OutputSelect, 2)
		If $PreferredOutputPath And FileExists($PreferredOutputPath) Then
			$OutputPath = $PreferredOutputPath
		Else
			$OutputPath = FileSelectFolder($_ChooseOutputFolder & ':', '', 7, StringStripWS(($PreviousOutputPath ? $PreviousOutputPath : @ScriptDir), 3))
			$PreviousOutputPath = $OutputPath
			IniWrite($AppConfig, 'System', 'PreviousOutputPath', $PreviousOutputPath)
		EndIf
		If $OutputPath <> '' Then
			$LogCreate = 1
			LogWrite('----------------------------------------------------------------------[	' & StringUpper($_CombiningStarted) & '	]----------------------------------------------------------------------' & @CRLF)
			$OutputPath &= (StringCompare(StringRight($OutputPath, 1), '\') = 0 ? 'SMC\' : '\SMC\')
			If $CombineInterruptSwitch <> -1 Then
				If $CleanInstall = 1 Then DirRemove($OutputPath, 1)
				LogWrite('[O] ' & $_OutputPathSelected & ': ' & $OutputPath & @CRLF)
				Local $OutputSpaceFreeAbs = DriveSpaceFree(StringLeft($OutputPath, 2)), $TempSpaceFreeAbs = DriveSpaceFree(StringLeft($TempDir, 2)), $OutputSpaceFree = $OutputSpaceFreeAbs, $TempSpaceFree = $TempSpaceFreeAbs, $OutputSizeUnit = $_SizeMb, $TempSizeUnit = $_SizeMb
				If $OutputSpaceFreeAbs > 1024 Then
					$OutputSpaceFree = Round($OutputSpaceFreeAbs / 1024, 2)
					$OutputSizeUnit = $_SizeGb
				EndIf
				If $TempSpaceFreeAbs > 1024 Then
					$TempSpaceFree = Round($TempSpaceFreeAbs / 1024, 2)
					$TempSizeUnit = $_SizeGb
				EndIf
				LogWrite('[>] ' & $_SpaceRequired & ':' & @CRLF)
				LogWrite(' [+]' & @CRLF)
				LogWrite('  |- ' & 'TMP ' & ': ' & $TempDir & ' (' & $CheckedSumSize & ' ' & $SizeUnit & '/' & ($TempSpaceFree = 0 ? $_Unknown : $TempSpaceFree & ' ' & $TempSizeUnit) & ')' & @CRLF)
				LogWrite('  |- ' & '>>> ' & ': ' & StringTrimRight($OutputPath, 5) & ' (' & $CheckedSumSize & ' ' & $SizeUnit & '/' & ($OutputSpaceFree = 0 ? $_Unknown : $OutputSpaceFree & ' ' & $OutputSizeUnit) & ')' & @CRLF)
				LogWrite(' [-]' & @CRLF)
				If $DDSoptimize = 1 Then LogWrite('[D] ' & $_DDSoptimizeSelected & @CRLF)
				If $BSAoptUnpack = 1 Then LogWrite('[B] ' & $_BSAoptUnpackSelected & @CRLF)
				If $BSAoptPack = 1 Then LogWrite('[B] ' & $_BSAoptPackSelected & @CRLF)
				If $CreateArchive = 1 Then LogWrite('[N] ' & $_ArchivingSelected & @CRLF)
				If $NoParallax = 1 Then LogWrite('[T] ' & $_NoParallaxSelected & @CRLF)
				LogWrite('[>] ' & $_ModsChecked & ':' & @CRLF)
				LogWrite(' [+]' & @CRLF)
				For $i = 0 To $ModArray[0] - 1
					If GUICtrlRead($ListViewItem[$i], 1) = $GUI_CHECKED Then LogWrite('  |- ' & $ModName[$i] & @CRLF)
				Next
				LogWrite(' [-]' & @CRLF)
				If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 2)
				DirCreate($TempDir & '\SMC\_\')
				MainProgressSet(1, $_StatusMsg & ': ' & $_TempCreated)
				Local $ProgressCounter = 3, $ExtraAction, $ExtraMove
				For $i = 0 To $ModArray[0] - 1
					If $CombineInterruptSwitch = -1 Then ExitLoop
					If GUICtrlRead($ListViewItem[$i], 1) = $GUI_CHECKED Then
						GUICtrlSetBkColor($ListViewItem[$i], 0xcdcdcd)
						ModLogoSet($i)
						MainProgressSet($ProgressCounter, $_Combining & ': ' & $ModName[$i])
						If $CheckedSumSizeAbs > 0 Then $ProgressCounter += Round((($DDSoptimize ? ($BSAoptPack ? $MilestoneOne : $MilestoneTwo) : ($BSAoptPack ? $MilestoneTwo : 95)) - 3) * $ModSize[$i] / $CheckedSumSizeAbs)
						If CheckModCurArchExists($i) Then
							LogWrite('[M] ' & $_ModChecked & ': ' & $ModName[$i] & @CRLF)
							LogWrite('[I] ' & $_InputPathSelected & ': ' & _GUICtrlListView_GetItemText($ListView, $i, 1) & @CRLF)
							Local $ModCurArch = IniRead($AppConfig, $ModArray[$i + 1], $Quality & 'Archives', '')
							$ModCurArch = StringSplit(StringReplace($ModCurArch, '?', ''), ';')
							For $i1 = 1 To $ModCurArch[0]
								If $CombineInterruptSwitch = -1 Then ExitLoop
								$ModCurArchPathOut = $TempDir & '\SMC\_\' & $ModArray[$i + 1]
								DirCreate($ModCurArchPathOut & '\')
								Local $ModCurArchIn, $ModCurArchPathIn = ''
								Local $ModCurArchSplit = StringSplit($ModCurArch[$i1], '|')
								For $i2 = 1 To $ModCurArchSplit[0]
									$ModCurArchSplit[$i2] = StringStripWS($ModCurArchSplit[$i2], 3)
								Next
								If StringInStr('.zip .7z .rar', StringRight($ModCurArchSplit[1], 4)) = 0 Then $ModCurArchSplit[1] &= '.*'
								Local $ModCurArchSplitTemp = $ModCurArchSplit
								If _FO_FileSearch($Path, $ModCurArchSplit[1], True, 0, 2, 0) Or _FO_FileSearch($Path, StringReplace($ModCurArchSplit[1], ' ', '_'), True, 0, 2, 0) Then
									If _FO_FileSearch($Path, $ModCurArchSplit[1], True, 0, 2, 0) Then
										$ModCurArchIn = StringSplit(_FO_FileSearch($Path, $ModCurArchSplit[1], True, 0, 2, 0), @CRLF, 1)[1]
										$ModCurArchPathIn = $Path & $ModCurArchIn
									Else
										$ModCurArchIn = StringSplit(_FO_FileSearch($Path, StringReplace($ModCurArchSplit[1], ' ', '_'), True, 0, 2, 0), @CRLF, 1)[1]
										$ModCurArchPathIn = $Path & $ModCurArchIn
									EndIf
									If $ModCurArchIn Then
										LogWrite('[A] ' & $_ArchiveFound & ' (' & $Quality & '): ' & StringReplace($ModCurArchIn, '.part01', '') & @CRLF)
										LogWrite(' [+]' & @CRLF)
										While Not FileExists($7zExe) Or Not FileExists(StringTrimRight($7zExe, 4) & '.dll')
											If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 8)
											MainProgressSet(-1, $_StatusMsg & ': ' & $_7zWarning, 2)
											If _ExtMsgBox(48, $_Quit & '|&' & $_Proceed, $_Warning, $_7zWarning & ' :(' & @CRLF & @CRLF & $_SelectManually & '...') = 1 Then ExitClean(2)
											WinFlash($GUIMain, '', 3, 400)
											$7zExe = FileOpenDialog($_Choose7zExe, @ScriptDir & '\', $_Executable & ' (*.exe)', 3)
										WEnd
										If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 2)
										MainProgressSet(-1, $_Extracting & ': ' & StringReplace($ModCurArchIn, '.part01', ''))
										If RunProcessing($ModCurArchPathIn, $ModCurArchPathOut, $_Extracting & ': ' & $ModCurArchIn, _
												Round((($DDSoptimize ? ($BSAoptPack ? $MilestoneOne : $MilestoneTwo) : ($BSAoptPack ? $MilestoneTwo : 95)) - 3) * $ModSize[$i] / $CheckedSumSizeAbs) / $ModCurArch[0], $ProgressCounter, $ModCurArch) > 0 Then $OverallErrors += 1
										If _SQLite_Query(-1, 'SELECT [From], [Extra] FROM [SMC] WHERE [Mod]="' & $ModArray[$i + 1] & '"', $Query) <> $SQLITE_OK Then
											LogWrite(' [X] ' & $_SQLiteError & '=' & _SQLite_ErrCode() & ' (' & _SQLite_ErrMsg() & ')' & @CRLF)
											$OverallErrors += 1
										EndIf
										If $ModCurArchSplit[0] > 2 Then
											For $i2 = 2 To $ModCurArchSplitTemp[0]
												While _SQLite_FetchData($Query, $Row, 0, 1) = $SQLITE_OK
													If $CombineInterruptSwitch = -1 Then ExitLoop
													If $Row[1] = '' Then
														MoveFFTIC($BSAoptUnpack, $ModCurArchPathOut, $Row[0], $TempDir & '\SMC\' & $ModArray[$i + 1] & $Row[0], $ModCurArchSplitTemp)
													Else
														$ExtraAction = StringLeft($Row[1], 1)
														If $ExtraAction = '-' Or $ExtraAction = '+' Then
															$ExtraMove = ($ExtraAction = '-' ? 1 : 0)
															For $k = 1 To $ModArray[0]
																If StringTrimLeft($Row[1], 1) = $ModArray[$k] And GUICtrlRead($ListViewItem[$k - 1], 1) = $GUI_CHECKED Then
																	$ExtraMove = ($ExtraAction = '-' ? 0 : 1)
																	ExitLoop 1
																EndIf
															Next
															If $ExtraMove = 1 Then MoveFFTIC($BSAoptUnpack, $ModCurArchPathOut, $Row[0], $TempDir & '\SMC\' & $ModArray[$i + 1] & $Row[0], $ModCurArchSplitTemp)
														EndIf
													EndIf
												WEnd
												_SQLite_QueryReset($Query)
												_ArrayDelete($ModCurArchSplitTemp, 2)
												$ModCurArchSplitTemp[0] -= 1
											Next
											_SQLite_QueryFinalize($Query)
										EndIf
										While _SQLite_FetchData($Query, $Row) = $SQLITE_OK
											If $CombineInterruptSwitch = -1 Then ExitLoop
											If $Row[1] = '' Then
												MoveFFTIC($BSAoptUnpack, $ModCurArchPathOut, $Row[0], $TempDir & '\SMC\' & $ModArray[$i + 1] & $Row[0], $ModCurArchSplitTemp)
											Else
												$ExtraAction = StringLeft($Row[1], 1)
												If $ExtraAction = '-' Or $ExtraAction = '+' Then
													$ExtraMove = ($ExtraAction = '-' ? 1 : 0)
													For $k = 1 To $ModArray[0]
														If StringTrimLeft($Row[1], 1) = $ModArray[$k] And GUICtrlRead($ListViewItem[$k - 1], 1) = $GUI_CHECKED Then
															$ExtraMove = ($ExtraAction = '-' ? 0 : 1)
															ExitLoop 1
														EndIf
													Next
													If $ExtraMove = 1 Then MoveFFTIC($BSAoptUnpack, $ModCurArchPathOut, $Row[0], $TempDir & '\SMC\' & $ModArray[$i + 1] & $Row[0], $ModCurArchSplitTemp)
												EndIf
											EndIf
										WEnd
										_SQLite_QueryFinalize($Query)
										CheckOverwrite($i, $ModCurArchPathOut, $OutputPath, $ModCurArchSplit)
										LogWrite(' [-]' & @CRLF)
									EndIf
								EndIf
								DirRemove($ModCurArchPathOut & '\', 1)
							Next
						EndIf
						If CheckModPathExists($i) Then
							LogWrite('[F] ' & $_FolderSearch & ': ' & $Path & @CRLF)
							LogWrite(' [+]' & @CRLF)
							If _SQLite_Query(-1, 'SELECT [From], [Extra] FROM [SMC] WHERE [Mod]="' & $ModArray[$i + 1] & '"', $Query) <> $SQLITE_OK Then
								LogWrite(' [X] ' & $_SQLiteError & '=' & _SQLite_ErrCode() & ' (' & _SQLite_ErrMsg() & ')' & @CRLF)
								$OverallErrors += 1
							EndIf
							While _SQLite_FetchData($Query, $Row) = $SQLITE_OK
								If $CombineInterruptSwitch = -1 Then ExitLoop
								If $Row[1] = '' Then
									MoveFFTIC($BSAoptUnpack, $Path, $Row[0], $TempDir & '\SMC\' & $ModArray[$i + 1] & $Row[0], 0, 1)
								Else
									$ExtraAction = StringLeft($Row[1], 1)
									If $ExtraAction = '-' Or $ExtraAction = '+' Then
										$ExtraMove = ($ExtraAction = '-' ? 1 : 0)
										For $k = 1 To $ModArray[0]
											If StringTrimLeft($Row[1], 1) = $ModArray[$k] And GUICtrlRead($ListViewItem[$k - 1], 1) = $GUI_CHECKED Then
												$ExtraMove = ($ExtraAction = '-' ? 0 : 1)
												ExitLoop 1
											EndIf
										Next
										If $ExtraMove = 1 Then MoveFFTIC($BSAoptUnpack, $Path, $Row[0], $TempDir & '\SMC\' & $ModArray[$i + 1] & $Row[0], 0, 1)
									EndIf
								EndIf
							WEnd
							_SQLite_QueryFinalize($Query)
							CheckOverwrite($i, $Path, $OutputPath, 0, 1)
							LogWrite(' [-]' & @CRLF)
						EndIf
					EndIf
				Next
				If $CombineInterruptSwitch <> -1 Then
					CombineCP($DDSoptimize, $BSAoptPack, $BSAoptUnpack)
					MainProgressSet(($DDSoptimize ? ($BSAoptPack ? $MilestoneOne : $MilestoneTwo) : ($BSAoptPack ? $MilestoneTwo : 95)) + 1, $_Combining & ': ' & $_Finalizing)
					If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 2)
					LogWrite('[>] ' & $_Finalizing & ': ' & $OutputPath & @CRLF)
					LogWrite(' [+]' & @CRLF)
					$PathFromFinalize = $TempDir & '\SMC\'
					If _SQLite_Query(-1, 'SELECT [Mod], [From], [To] FROM [SMC]', $Query) <> $SQLITE_OK Then
						LogWrite(' [X] ' & $_SQLiteError & '=' & _SQLite_ErrCode() & ' (' & _SQLite_ErrMsg() & ')' & @CRLF)
						$OverallErrors += 1
					EndIf
					While _SQLite_FetchData($Query, $Row) = $SQLITE_OK
						MoveFFTIC(0, $PathFromFinalize, $Row[0] & $Row[1], $OutputPath & $Row[2])
					WEnd
					_SQLite_QueryFinalize($Query)
					LogWrite(' [-]' & @CRLF)
					CombineDDSoptimize($DDSoptimize, $BSAoptPack, $OutputPath)
					CombineBSAoptPack($BSAoptPack, $OutputPath)
					MainProgressSet(($CreateArchive ? 97 : 99), $_StatusMsg & ': ' & $_TempRemoved)
					DirRemove($TempDir & '\SMC\', 1)
					If StringInStr('WIN_VISTA WIN_7 WIN_8 WIN_81 WIN_10 WIN_2008 WIN_2008R2 WIN_2012 WIN_2012R2 WIN_2016', @OSVersion) Then ShellExecute('icacls', 'SMC\ /T /C /Q /reset', $OutputPath, '', @SW_HIDE)
					CombineCreateArchive($OutputPath, $CreateArchive, $DDSoptimize, $BSAoptPack, $BSAoptUnpack)
				EndIf
				If $OverallErrors > 0 Or $CombineInterruptSwitch = -1 Then
					If $OverallErrors > 0 Then
						If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 4)
						MainProgressSet(100, $_StatusMsg & ': ' & $_ErrorsFound, 1)
						WinFlash($GUIMain, '', 3, 300)
						_ExtMsgBox(48, '&' & $_Skip, $_Warning, $_ErrorsFound & @CRLF & @CRLF & $_ErrorsCheckLog & ':' & @CRLF & ($NoLogging = 1 ? '(NoLogging=1 => NoLogging=0!)' : '(' & $LogPath & ')'))
					Else
						If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 8)
						MainProgressSet(100, $_StatusMsg & ': ' & $_Interruption, 2)
						WinFlash($GUIMain, '', 3, 300)
					EndIf
				Else
					MainProgressSet(100, $_StatusMsg & ': ' & $_CombiningFinished, -1)
					WinFlash($GUIMain, '', 1, 1000)
				EndIf
			Else
				WinFlash($GUIMain, '', 3, 400)
			EndIf
			LogWrite('----------------------------------------------------------------------[	' & StringUpper($_CombiningFinished) & '	]----------------------------------------------------------------------' & @CRLF)
		EndIf
		GUISetState(@SW_LOCK)
		_HotKey_Assign(BitOR($CK_CONTROL, $CK_ALT, 0x58))
		GUICtrlSetState($MenuMainAppPageOpen, $GUI_ENABLE)
		GUICtrlSetState($MenuMainModPageOpen, $GUI_ENABLE)
		GUICtrlSetState($MenuMainSettings, $GUI_ENABLE)
		GUICtrlSetState($MenuMainHelp, $GUI_ENABLE)
		For $i = 0 To $ModArray[0] - 1
			GUICtrlSetState($MenuListSpecifyPath[$i], $GUI_ENABLE)
			GUICtrlSetState($MenuListEditor[$i], $GUI_ENABLE)
			GUICtrlSetState($MenuListDownload[$i], $GUI_ENABLE)
			GUICtrlSetState($MenuListOpen[$i], $GUI_ENABLE)
			GUICtrlSetState($MenuListCheckAll[$i], $GUI_ENABLE)
			GUICtrlSetState($MenuListUncheckAll[$i], $GUI_ENABLE)
			GUICtrlSetState($MenuListPathAll[$i], $GUI_ENABLE)
			GUICtrlSetState($MenuListPathAllUnspec[$i], $GUI_ENABLE)
			GUICtrlSetState($MenuListClearCurrent[$i], $GUI_ENABLE)
			GUICtrlSetState($MenuListClearAllEx[$i], $GUI_ENABLE)
			GUICtrlSetState($MenuListClearAll[$i], $GUI_ENABLE)
		Next
		GUISetState(@SW_UNLOCK)
		ModLogoSet($ListViewItemSelected, 1)
		GUICtrlSetData($MainButton, $_BeginButton)
		GUICtrlSetState($MainButton, $GUI_ENABLE)
		If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 2)
		If $OutputPath <> '' Then
			MainProgressSet(0, $_Previous & ' ' & $_StatusMsg & ': ' & ($OverallErrors > 0 ? $_ErrorsFound : $CombineInterruptSwitch = -1 ? $_Interruption : $_CombiningFinished))
		Else
			MainProgressSet(0, $_StatusMsg & ': ' & $_ModsChecked & ' - ' & $CheckedSum & ', ' & $_ModsCheckedSize & ' - ' & $CheckedSumSize & ' ' & $SizeUnit)
		EndIf
		$CombineInterruptSwitch = 0
		$OverallErrors = 0
		GUIHotKeys()
	EndFunc   ;==>CombineBegin

	Func CombineBSAoptPack($BSAoptPack, $InputPath)
		If $BSAoptPack = 1 And $CombineInterruptSwitch <> -1 Then
			Local $TempSize, $TempSteps, $TempFiles, $PID, $BSAoptOutput, $TempPercent = 0, $TempPercentNew = 0, $BSAoptOutputError = 0
			Local $OutputPath = StringTrimRight($InputPath, 4) & 'SMC_BSA\'
			DirRemove($OutputPath, 1)
			While Not FileExists($BSAoptExe)
				If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 8)
				MainProgressSet(-1, $_StatusMsg & ': ' & $_BSAoptWarning, 2)
				If _ExtMsgBox(48, $_Quit & '|&' & $_Proceed, $_Warning, $_BSAoptWarning & ' :(' & @CRLF & @CRLF & $_SelectManually & '...') = 1 Then ExitClean(2)
				WinFlash($GUIMain, '', 3, 400)
				$BSAoptExe = FileOpenDialog($_ChooseBSAoptExe, @ScriptDir & '\', $_Executable & ' (*.exe)', 3)
			WEnd
			DirCreate($OutputPath)
			If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 2)
			MainProgressSet($MilestoneTwo + 2, $_StatusMsg & ': ' & $_BSAoptPacking)
			FileDelete($InputPath & 'SMC*.esp')
			FileDelete($InputPath & 'SMC*.bsa')
			FileMove($InputPath & '*.esp', $OutputPath, 9)
			FileMove($InputPath & '*.esm', $OutputPath, 9)
			FileMove($InputPath & '*.bsa', $OutputPath, 9)
			$TempSize = DirGetSize($InputPath)
			If $TempSize <= 2147483648 Then
				$PID = Run('"' & $BSAoptExe & '" -deployment -game sk "' & $InputPath & '" "' & $OutputPath & 'SMC.bsa"', @ScriptDir, @SW_HIDE, $STDERR_MERGED)
				ProcessWait($PID, 3)
				LogWrite('[B] ' & $_BSAoptPacking & @CRLF)
				While ProcessExists($PID)
					If $CombineInterruptSwitch = -1 Then ExitLoop
					$TempPercentNew = Round(ProcessGetStats($PID, 1)[3] / $TempSize, 2)
					If $TempPercentNew Then
						$TempPercentNew = ($TempPercentNew > 1 ? 1 : $TempPercentNew)
						If $TempPercent <> $TempPercentNew Then
							$TempPercent = $TempPercentNew
							MainProgressSet($MilestoneTwo + 2 + (94 - $MilestoneTwo) * ($TempPercent > 0.99 ? 1 : $TempPercent), $_StatusMsg & ': ' & $_BSAoptPacking & ' (' & $TempPercent * 100 & '%)')
						EndIf
					EndIf
					Sleep(300)
				WEnd
				LogWrite(' [+]' & @CRLF)
				$BSAoptOutput = StdoutRead($PID)
				If @error Then
					$BSAoptOutputError = 1
					LogWrite(' [X] ' & $_BSAoptError & @CRLF)
				Else
					LogWrite('  |- ' & StringTrimRight($LogPath, 4) & '.BSAopt.log' & @CRLF)
					FileWrite(StringTrimRight($LogPath, 4) & '.BSAopt.log', $BSAoptOutput & @CRLF & '----------------------------------------------------------------------[	' & @YEAR & '-' & @MON & '-' & @MDAY & ' ' & @HOUR & ':' & @MIN & ':' & @SEC & '	]----------------------------------------------------------------------' & @CRLF)
					FileCopy($AppESP, $OutputPath, 9)
				EndIf
				LogWrite(' [-]' & @CRLF)
			Else
				$TempSteps = Ceiling($TempSize / 2147483648)
				For $i = 1 To $TempSteps
					$TempFiles = _FileListToArrayRec($InputPath, '*', 1, 1, 0, 2)
					If Not @error Then
						For $i1 = 0 To $TempFiles[0]
							If DirGetSize(StringReplace($InputPath, 'SMC', 'SMC' & ($i < 10 ? '0' & $i : $i), -1)) + FileGetSize($TempFiles[$i1]) > 2147483648 Then
								ExitLoop
							Else
								FileMove($TempFiles[$i1], StringReplace($TempFiles[$i1], 'SMC', 'SMC' & ($i < 10 ? '0' & $i : $i), -1), 9)
							EndIf
						Next
					EndIf
					$PID = Run('"' & $BSAoptExe & '" -deployment -game sk "' & StringReplace($InputPath, 'SMC', 'SMC' & ($i < 10 ? '0' & $i : $i), -1) & '" "' & $OutputPath & 'SMC' & ($i < 10 ? '0' & $i : $i) & '.bsa"', @ScriptDir, @SW_HIDE, $STDERR_MERGED)
					ProcessWait($PID, 3)
					LogWrite('[B] ' & $_BSAoptPacking & ' (' & $i & '/' & $TempSteps & ')' & @CRLF)
					While ProcessExists($PID)
						If $CombineInterruptSwitch = -1 Then ExitLoop
						$TempPercentNew = Round(ProcessGetStats($PID, 1)[3] / $TempSize, 2)
						If $TempPercentNew Then
							$TempPercentNew = ($TempPercentNew > 1 ? 1 : $TempPercentNew)
							If $TempPercent <> $TempPercentNew Then
								$TempPercent = $TempPercentNew
								MainProgressSet($MilestoneTwo + 2 + (94 - $MilestoneTwo) * ($i - 1) / $TempSteps + (94 - $MilestoneTwo) * ($TempPercent > 0.99 ? 1 : $TempPercent) / $TempSteps, $_StatusMsg & ': ' & $_BSAoptPacking & ' (' & $i & '/' & $TempSteps & ', ' & $TempPercent * 100 & '%)')
							EndIf
						EndIf
						Sleep(300)
					WEnd
					LogWrite(' [+]' & @CRLF)
					$BSAoptOutput = StdoutRead($PID)
					If @error Then
						$BSAoptOutputError = 1
						$OverallErrors += 1
						LogWrite(' [X] ' & $_BSAoptError & @CRLF)
					Else
						LogWrite('  |- ' & StringTrimRight($LogPath, 4) & '.BSAopt.log' & @CRLF)
						FileWrite(StringTrimRight($LogPath, 4) & '.BSAopt.log', $BSAoptOutput & @CRLF & '----------------------------------------------------------------------[	' & @YEAR & '-' & @MON & '-' & @MDAY & ' ' & @HOUR & ':' & @MIN & ':' & @SEC & '	]----------------------------------------------------------------------' & @CRLF)
						FileCopy($AppESP, $OutputPath & 'SMC' & ($i < 10 ? '0' & $i : $i) & '.esp', 9)
						DirRemove(StringReplace($InputPath, 'SMC', 'SMC' & ($i < 10 ? '0' & $i : $i), -1), 1)
					EndIf
					LogWrite(' [-]' & @CRLF)
				Next
			EndIf
			If $BSAoptOutputError = 0 Then
				DirRemove($InputPath, 1)
				DirMove($OutputPath, $InputPath, 1)
			EndIf
		EndIf
	EndFunc   ;==>CombineBSAoptPack

	Func CombineBSAoptUnpack($BSAoptUnpack, $InputPath)
		If $BSAoptUnpack = 1 And $CombineInterruptSwitch <> -1 Then
			Local $TempSize, $PID, $BSAoptOutput, $BSAoptOutputError = 0, $TempPercent = 0, $TempPercentNew = 0
			While Not FileExists($BSAoptExe)
				If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 8)
				MainProgressSet(-1, $_StatusMsg & ': ' & $_BSAoptWarning, 2)
				If _ExtMsgBox(48, $_Quit & '|&' & $_Proceed, $_Warning, $_BSAoptWarning & ' :(' & @CRLF & @CRLF & $_SelectManually & '...') = 1 Then ExitClean(2)
				WinFlash($GUIMain, '', 3, 400)
				$BSAoptExe = FileOpenDialog($_ChooseBSAoptExe, @ScriptDir & '\', $_Executable & ' (*.exe)', 3)
			WEnd
			$TempSize = FileGetSize($InputPath)
			$PID = Run('"' & $BSAoptExe & '" -deployment -game sk "' & $InputPath & '" "' & StringLeft($InputPath, StringInStr($InputPath, '\', 0, -1)) & '"', @ScriptDir, @SW_HIDE, $STDERR_MERGED)
			ProcessWait($PID, 3)
			LogWrite('[B] ' & $_BSAoptUnpacking & @CRLF)
			While ProcessExists($PID)
				If $CombineInterruptSwitch = -1 Then ExitLoop
				$TempPercentNew = Round(ProcessGetStats($PID, 1)[3] / $TempSize, 2)
				If $TempPercentNew Then
					$TempPercentNew = ($TempPercentNew > 1 ? 1 : $TempPercentNew)
					If $TempPercent <> $TempPercentNew Then
						$TempPercent = $TempPercentNew
						MainProgressSet(-1, $_StatusMsg & ': ' & $_BSAoptUnpacking & ' [' & StringTrimLeft($InputPath, StringInStr($InputPath, '\', 0, -1)) & '] (' & $TempPercent * 100 & '%)')
					EndIf
				EndIf
				Sleep(300)
			WEnd
			LogWrite(' [+]' & @CRLF)
			$BSAoptOutput = StdoutRead($PID)
			If @error Then
				$BSAoptOutputError = 1
				$OverallErrors += 1
				LogWrite(' [X] ' & $_BSAoptError & @CRLF)
			Else
				LogWrite('  |- ' & StringTrimRight($LogPath, 4) & '.BSAopt.log' & @CRLF)
				FileWrite(StringTrimRight($LogPath, 4) & '.BSAopt.log', $BSAoptOutput & @CRLF & '----------------------------------------------------------------------[	' & @YEAR & '-' & @MON & '-' & @MDAY & ' ' & @HOUR & ':' & @MIN & ':' & @SEC & '	]----------------------------------------------------------------------' & @CRLF)
			EndIf
			LogWrite(' [-]' & @CRLF)
			If $BSAoptOutputError = 0 And StringInStr($InputPath, $TempDir & '\SMC\_\') <> 0 Then FileDelete($InputPath)
		EndIf
	EndFunc   ;==>CombineBSAoptUnpack

	Func CombineCP($DDSoptimize, $BSAoptPack, $BSAoptUnpack)
		Local $CPSection = IniReadSection($AppConfig, 'Compatibility Patches')
		If Not @error And $CPSection[0][0] > 0 Then
			LogWrite('[C] ' & $_CPSectionFound & @CRLF)
			MainProgressSet(($DDSoptimize ? ($BSAoptPack ? $MilestoneOne : $MilestoneTwo) : ($BSAoptPack ? $MilestoneTwo : 95)), $_Combining & ': ' & $_CPSectionFound)
			For $i = 1 To $CPSection[0][0]
				If $CombineInterruptSwitch = -1 Then ExitLoop
				Local $CP = StringSplit($CPSection[$i][0], '|')
				For $i1 = 1 To $ModArray[0]
					If $ModArray[$i1] = $CP[1] And GUICtrlRead($ListViewItem[$i1 - 1], 1) = $GUI_CHECKED Then
						For $i2 = 1 To $ModArray[0]
							If $ModArray[$i2] = $CP[2] And GUICtrlRead($ListViewItem[$i2 - 1], 1) = $GUI_CHECKED Then
								Local $CPCurArch = StringSplit(StringReplace($CPSection[$i][1], '?', ''), ';')
								For $i3 = 1 To $CPCurArch[0]
									If $CombineInterruptSwitch = -1 Then ExitLoop
									Local $CPCurArchPathIn = ''
									Local $CPCurArchSplit = StringSplit($CPCurArch[$i3], '|')
									For $i4 = 1 To $CPCurArchSplit[0]
										$CPCurArchSplit[$i4] = StringStripWS($CPCurArchSplit[$i4], 3)
									Next
									If $CP[0] = 2 Then
										$CPCurArchPathIn = CheckCPCurArchExists($CPCurArchSplit[1], $i1, $i2)
									ElseIf $CP[0] = 3 Then
										For $i4 = 1 To $ModArray[0]
											If $ModArray[$i4] = $CP[3] And GUICtrlRead($ListViewItem[$i4 - 1], 1) = $GUI_CHECKED Then
												$CPCurArchPathIn = CheckCPCurArchExists($CPCurArchSplit[1], $i1, $i2, $i4)
											EndIf
										Next
									EndIf
									If _FO_FileSearch(StringLeft($CPCurArchPathIn, StringInStr($CPCurArchPathIn, '\', 0, -1)), StringTrimLeft($CPCurArchPathIn, StringInStr($CPCurArchPathIn, '\', 0, -1)), True, 0, 2, 0) Then
										$CPCurArchPathIn = StringSplit(_FO_FileSearch(StringLeft($CPCurArchPathIn, StringInStr($CPCurArchPathIn, '\', 0, -1)), StringTrimLeft($CPCurArchPathIn, StringInStr($CPCurArchPathIn, '\', 0, -1)), True, 0, 1, 0), @CRLF, 1)[1]
										$CPCurArchPathOut = $TempDir & '\SMC\_\' & StringReplace($CPSection[$i][0], '|', '+')
										DirCreate($CPCurArchPathOut & '\')
										LogWrite('[A] ' & $_CPArchiveFound & ': ' & StringTrimLeft($CPCurArchPathIn, StringInStr($CPCurArchPathIn, '\', 0, -1)) & @CRLF)
										LogWrite(' [+]' & @CRLF)
										While Not FileExists($7zExe) Or Not FileExists(StringTrimRight($7zExe, 4) & '.dll')
											If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 8)
											MainProgressSet(-1, $_StatusMsg & ': ' & $_7zWarning, 2)
											If _ExtMsgBox(48, $_Quit & '|&' & $_Proceed, $_Warning, $_7zWarning & ' :(' & @CRLF & @CRLF & $_SelectManually & '...') = 1 Then ExitClean(2)
											WinFlash($GUIMain, '', 3, 400)
											$7zExe = FileOpenDialog($_Choose7zExe, @ScriptDir & '\', $_Executable & ' (*.exe)', 3)
										WEnd
										If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 2)
										MainProgressSet(-1, $_Extracting & ': ' & StringTrimLeft($CPCurArchPathIn, StringInStr($CPCurArchPathIn, '\', 0, -1)))
										If RunProcessing($CPCurArchPathIn, $CPCurArchPathOut, $_Extracting & ': ' & StringTrimLeft($CPCurArchPathIn, StringInStr($CPCurArchPathIn, '\', 0, -1))) > 0 Then $OverallErrors += 1
										If _SQLite_Query(-1, 'SELECT [Mod], [From] FROM [SMC]', $Query) <> $SQLITE_OK Then
											LogWrite(' [X] ' & $_SQLiteError & '=' & _SQLite_ErrCode() & ' (' & _SQLite_ErrMsg() & ')' & @CRLF)
											$OverallErrors += 1
										EndIf
										While _SQLite_FetchData($Query, $Row) = $SQLITE_OK
											If $CombineInterruptSwitch = -1 Then ExitLoop
											If $CP[0] = 2 Then
												If $Row[0] = $ModArray[$i1] Or $Row[0] = $ModArray[$i2] Then
													If $CPCurArchSplit[0] = 3 Then
														MoveFFTIC($BSAoptUnpack, $CPCurArchPathOut, $Row[1], $TempDir & '\SMC\' & $CP[2] & '\' & $CPCurArchSplit[3] & $Row[1], $CPCurArchSplit)
													Else
														MoveFFTIC($BSAoptUnpack, $CPCurArchPathOut, $Row[1], $TempDir & '\SMC\' & $CP[2] & $Row[1], $CPCurArchSplit)
													EndIf
												EndIf
											ElseIf $CP[0] = 3 Then
												For $i5 = 1 To $ModArray[0]
													If $CombineInterruptSwitch = -1 Then ExitLoop
													If $ModArray[$i5] = $CP[3] And GUICtrlRead($ListViewItem[$i5 - 1], 1) = $GUI_CHECKED Then
														If $Row[0] = $ModArray[$i1] Or $Row[0] = $ModArray[$i2] Or $Row[0] = $ModArray[$i5] Then
															If $CPCurArchSplit[0] = 3 Then
																MoveFFTIC($BSAoptUnpack, $CPCurArchPathOut, $Row[1], $TempDir & '\SMC\' & $CP[3] & '\' & $CPCurArchSplit[3] & $Row[1], $CPCurArchSplit)
															Else
																MoveFFTIC($BSAoptUnpack, $CPCurArchPathOut, $Row[1], $TempDir & '\SMC\' & $CP[3] & $Row[1], $CPCurArchSplit)
															EndIf
														EndIf
													EndIf
												Next
											EndIf
										WEnd
										_SQLite_QueryFinalize($Query)
										LogWrite(' [-]' & @CRLF)
										DirRemove($CPCurArchPathOut & '\', 1)
									EndIf
								Next
							EndIf
						Next
					EndIf
				Next
			Next
		Else
			MainProgressSet(($DDSoptimize ? ($BSAoptPack ? $MilestoneOne : $MilestoneTwo) : ($BSAoptPack ? $MilestoneTwo : 95)), $_StatusMsg & ': ' & $_iniCPsWarning, 2)
		EndIf
	EndFunc   ;==>CombineCP

	Func CombineCreateArchive($OutputPath, $CreateArchive, $DDSoptimize, $BSAoptPack, $BSAoptUnpack)
		If $CreateArchive = 1 And $CombineInterruptSwitch <> -1 Then
			Local $7zCompression = IniRead($AppConfig, 'System', 'UseCompression', 0)
			MainProgressSet(97, $_StatusMsg & ': ' & $_NMMCreating)
			$FOMODInfoHandle = FileOpen($OutputPath & 'fomod\info.xml', 10)
			If $FOMODInfoHandle <> -1 Then
				FileWriteLine($FOMODInfoHandle, '<fomod>')
				FileWriteLine($FOMODInfoHandle, '  <Name>Skyrim Mod Combiner</Name>')
				FileWriteLine($FOMODInfoHandle, '  <Version>' & $DefinitionsVersion & '</Version>')
				FileWriteLine($FOMODInfoHandle, '  <Author>drigger</Author>')
				FileWriteLine($FOMODInfoHandle, '  <Description>[center][color=#FDD017][b][size=7][font=impact]SKYRIM MOD COMBINER[/font][/size][/b][/color]')
				FileWriteLine($FOMODInfoHandle, '')
				FileWriteLine($FOMODInfoHandle, '  [color=#FDD017][size=5][i] - [u]Where worlds come to[/u]g[u]ether[/u][/i][/size][/color]')
				FileWriteLine($FOMODInfoHandle, '')
				FileWriteLine($FOMODInfoHandle, '  [color=#EE9A4D][size=5][b][u]COMBINED MODS[/u][/b][/size][/color]')
				FileWriteLine($FOMODInfoHandle, '  [size=4]')
				For $i = 0 To $ModArray[0] - 1
					If GUICtrlRead($ListViewItem[$i], 1) = $GUI_CHECKED Then FileWriteLine($FOMODInfoHandle, '  ' & StringReplace($ModName[$i], '&', 'And'))
				Next
				FileWriteLine($FOMODInfoHandle, '  [/size]')
				FileWriteLine($FOMODInfoHandle, '')
				FileWriteLine($FOMODInfoHandle, '  [color=yellow][size=2.5]All the info and installation instructions you can find on the [url=http://www.nexusmods.com/skyrim/mods/51467]Skyrim Mod Combiner homepage[/url][/size][/color]')
				FileWriteLine($FOMODInfoHandle, '  [color=yellow][size=2.5]Based on the work by Cestral, [url=http://www.nexusmods.com/skyrim/mods/20801]Texture Pack Combiner homepage[/url][/size][/color]')
				FileWriteLine($FOMODInfoHandle, '')
				FileWriteLine($FOMODInfoHandle, '  [size=2.5][i]Created with SMC v' & $AppVer[1] & '.' & $AppVer[2] & '.' & $AppVer[3] & '.' & $AppVer[4])
				If $DDSoptimize Then FileWriteLine($FOMODInfoHandle, 'Optimized with DDSopt')
				If $BSAoptUnpack Then FileWriteLine($FOMODInfoHandle, 'Input unpacked with BSAopt')
				If $BSAoptPack Then FileWriteLine($FOMODInfoHandle, 'Output packed with BSAopt')
				FileWriteLine($FOMODInfoHandle, '  [/i][/size]')
				FileWriteLine($FOMODInfoHandle, '  [/center]</Description>')
				FileWriteLine($FOMODInfoHandle, '  <Website>http://www.nexusmods.com/skyrim/mods/51467/?</Website>')
				FileWriteLine($FOMODInfoHandle, '</fomod>')
			EndIf
			FileClose($FOMODInfoHandle)
			While Not FileExists($7zExe) Or Not FileExists(StringTrimRight($7zExe, 4) & '.dll')
				If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 8)
				MainProgressSet(-1, $_StatusMsg & ': ' & $_7zWarning, 2)
				If _ExtMsgBox(48, $_Quit & '|&' & $_Proceed, $_Warning, $_7zWarning & ' :(' & @CRLF & @CRLF & $_SelectManually & '...') = 1 Then ExitClean(2)
				WinFlash($GUIMain, '', 3, 400)
				$7zExe = FileOpenDialog($_Choose7zExe, @ScriptDir & '\', $_Executable & ' (*.exe)', 3)
			WEnd
			If CheckNMM() And _ExtMsgBox(32, $_Proceed & '|&' & $_Skip, $_Question, $_NMMFound & ': ' & @CRLF & $NMMClientModsFolder & @CRLF & @CRLF & $_MoveArchiveInNMMPath & '?') = 1 Then
				$CreateArchivePath = $NMMClientModsFolder
			Else
				$CreateArchivePath = StringTrimRight($OutputPath, 5)
			EndIf
			If FileExists($CreateArchivePath & '\SMC.7z') Then FileDelete($CreateArchivePath & '\SMC.7z')
			If RunProcessing($OutputPath, $CreateArchivePath & '\SMC.7z', $_NMMCreating & ': ' & $CreateArchivePath & '\SMC.7z', -1, -1, 1, ($7zCompression = 1 ? '-mx9 -y -r -sdel' : '-mx0 -y -r -sdel')) > 0 Then $OverallErrors += 1
			DirRemove($OutputPath, 1)
			LogWrite('[N] ' & $_ArchiveCreated & ': ' & $CreateArchivePath & '\SMC.7z (' & ($7zCompression = 1 ? $_7zCompressionUsed : $_7zCompressionNotUsed) & ')' & @CRLF)
		EndIf
	EndFunc   ;==>CombineCreateArchive

	Func CombineDDSoptimize($DDSoptimize, $BSAoptPack, $InputPath)
		If $DDSoptimize = 1 And $CombineInterruptSwitch <> -1 Then
			Local $OutputPath = StringTrimRight($InputPath, 4) & 'SMC_OPT\'
			DirRemove($OutputPath, 1)
			If UBound($ListOptimize) > 0 Then
				For $i = 0 To UBound($ListOptimize) - 1
					Switch $ListOptimize[$i][0]
						Case 'skip'
							If $ListOptimize[$i][2] = '' Then
								If FileExists($InputPath & $ListOptimize[$i][1]) Then FileMove($InputPath & $ListOptimize[$i][1], $OutputPath & $ListOptimize[$i][1], 9)
							Else
								For $i1 = 1 To $ModArray[0]
									If GUICtrlRead($ListViewItem[$i1 - 1], 1) = $GUI_CHECKED And $ModArray[$i1] = $ListOptimize[$i][2] Then
										If $ListOptimize[$i][1] = '' Then
											If _SQLite_Query(-1, 'SELECT [From], [To] FROM [SMC] WHERE [Mod]="' & $ModArray[$i1] & '"', $Query) <> $SQLITE_OK Then
												LogWrite(' [X] ' & $_SQLiteError & '=' & _SQLite_ErrCode() & ' (' & _SQLite_ErrMsg() & ')' & @CRLF)
												$OverallErrors += 1
											EndIf
											While _SQLite_FetchData($Query, $Row, 0, 1) = $SQLITE_OK
												$FileName = StringTrimLeft($Row[0], StringInStr($Row[0], '\', 0, -1))
												If StringLeft($Row[1], 1) = '\' Then $Row[1] = StringTrimLeft($Row[1], 1)
												If FileExists($InputPath & $Row[1] & $FileName) Then FileMove($InputPath & $Row[1] & $FileName, $OutputPath & $Row[1] & $FileName, 9)
											WEnd
											_SQLite_QueryFinalize($Query)
										Else
											If FileExists($InputPath & $ListOptimize[$i][1]) Then FileMove($InputPath & $ListOptimize[$i][1], $OutputPath & $ListOptimize[$i][1], 9)
										EndIf
									EndIf
								Next
							EndIf
					EndSwitch
				Next
			EndIf
			While Not FileExists($DDSoptExe)
				If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 8)
				MainProgressSet(-1, $_StatusMsg & ': ' & $_DDSoptWarning, 2)
				If _ExtMsgBox(48, $_Quit & '|&' & $_Proceed, $_Warning, $_DDSoptWarning & ' :(' & @CRLF & @CRLF & $_SelectManually & '...') = 1 Then ExitClean(2)
				WinFlash($GUIMain, '', 3, 400)
				$DDSoptExe = FileOpenDialog($_ChooseDDSoptExe, @ScriptDir & '\', $_Executable & ' (*.exe)', 3)
			WEnd
			If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 2)
			MainProgressSet(($BSAoptPack ? $MilestoneOne : $MilestoneTwo) + 2, $_StatusMsg & ': ' & $_DDSoptimization)
			DirCreate($OutputPath)
			Local $TempSize = DirGetSize($InputPath), $TempPercent = 0, $TempPercentNew = 0
			Local $PID = Run('"' & $DDSoptExe & '" -deployment -game sk "' & $InputPath & '" "' & $OutputPath & '"', @ScriptDir, @SW_HIDE, $STDERR_MERGED)
			ProcessWait($PID, 3)
			ProcessSetPriority($PID, 1)
			While ProcessExists($PID)
				If $CombineInterruptSwitch = -1 Then ExitLoop
				$TempPercentNew = Round(ProcessGetStats($PID, 1)[3] / $TempSize, 2)
				If $TempPercentNew Then
					$TempPercentNew = ($TempPercentNew > 1 ? 1 : $TempPercentNew)
					If $TempPercent <> $TempPercentNew Then
						$TempPercent = $TempPercentNew
						MainProgressSet(($BSAoptPack ? $MilestoneOne : $MilestoneTwo) + 2 + ($BSAoptPack ? $MilestoneTwo - $MilestoneOne : 94 - $MilestoneTwo) * ($TempPercent > 0.99 ? 1 : $TempPercent), $_StatusMsg & ': ' & $_DDSoptimization & ' (' & $TempPercent * 100 & '%)')
					EndIf
				EndIf
				Sleep(300)
			WEnd
			Local $DDSoptOutput = StdoutRead($PID)
			If @error Then
				LogWrite('[X] ' & $_DDSoptimization & ': ' & $_DDSoptError & @CRLF)
			Else
				LogWrite('[D] ' & $_DDSoptimization & ': ' & StringTrimRight($LogPath, 4) & '.DDSopt.log' & @CRLF)
				FileWrite(StringTrimRight($LogPath, 4) & '.DDSopt.log', $DDSoptOutput & @CRLF & '----------------------------------------------------------------------[	' & @YEAR & '-' & @MON & '-' & @MDAY & ' ' & @HOUR & ':' & @MIN & ':' & @SEC & '	]----------------------------------------------------------------------' & @CRLF)
				Local $OutputPathList = _FileListToArrayRec($OutputPath, '*', 1, 1, 0, 2)
				If Not @error Then
					For $i = 1 To $OutputPathList[0]
						FileMove($OutputPathList[$i], StringReplace($OutputPathList[$i], 'SMC_OPT', 'SMC', -1), 9)
					Next
				EndIf
				DirRemove($OutputPath, 1)
			EndIf
		EndIf
	EndFunc   ;==>CombineDDSoptimize

	Func CombineInterrupt()
		LogWrite(' [X] ' & $_Interruption & @CRLF)
		$CombineInterruptSwitch = -1
	EndFunc   ;==>CombineInterrupt

	Func ExitClean($ExitLevel = 0)
		If $ExitLevel > 0 Then
			If $AppConfig Then
				IniWrite($AppConfig, 'System', 'VersionCurrent', (@Compiled ? $AppVer[1] & '.' & $AppVer[2] & '.' & $AppVer[3] & '.' & $AppVer[4] : ''))
				If $ExitLevel < 3 And WinExists($GUIMain) Then
					$PrevWinPos = WinGetPos($GUIMain)
					If $PrevWinPos[2] < 675 Then $PrevWinPos[2] = 675
					If $PrevWinPos[2] > @DesktopWidth Then $PrevWinPos[2] = 1000
					If $PrevWinPos[3] < 290 Then $PrevWinPos[3] = 290
					If $PrevWinPos[3] > @DesktopHeight Then $PrevWinPos[3] = 664
					If $PrevWinPos[0] < 0 Then $PrevWinPos[0] = 0
					If $PrevWinPos[0] > @DesktopWidth - $PrevWinPos[2] Then $PrevWinPos[0] = @DesktopWidth - $PrevWinPos[2]
					If $PrevWinPos[1] < 0 Then $PrevWinPos[1] = 0
					If $PrevWinPos[1] > @DesktopHeight - $PrevWinPos[3] Then $PrevWinPos[1] = @DesktopHeight - $PrevWinPos[3]
					If @Compiled Then IniWrite($AppConfig, 'System', 'PreviousWindowSettings', $PrevWinPos[2] & ', ' & $PrevWinPos[3] & ', ' & $PrevWinPos[0] & ', ' & $PrevWinPos[1])
					IniDelete($AppConfig, 'Defined Paths')
					For $i = 1 To $ModArray[0]
						If _GUICtrlListView_GetItemText($ListView, $i - 1, 1) And BitAND(GUICtrlGetState($ListViewItem[$i - 1]), $GUI_SHOW) Then IniWrite($AppConfig, 'Defined Paths', $ModArray[$i], _GUICtrlListView_GetItemText($ListView, $i - 1, 1))
					Next
				EndIf
			EndIf
			If $ExitLevel > 1 Then
				GUIRegisterMsg($WM_NOTIFY, '')
				GUIRegisterMsg($WM_COMMAND, '')
				GUIDelete($GUILoading)
				GUIDelete($GUIUpdate)
				GUIDelete($GUIHelp)
				GUIDelete($GUISettings)
				For $i = 0 To $ModArray[0] - 1
					GUIDelete($GUIModEditor[$i])
				Next
				GUIDelete($GUIMain)
				If _SQLite_Exec(-1, 'DROP TABLE [SMC]') <> $SQLITE_OK Then
					LogWrite(' [X] ' & $_SQLiteError & '=' & _SQLite_ErrCode() & ' (' & _SQLite_ErrMsg() & ')' & @CRLF)
				Else
					_SQLite_Close()
					_SQLite_Shutdown()
					LogWrite('[S] ' & $_SQLiteRemoved & @CRLF)
				EndIf
			EndIf
			While IniFormat($AppConfig) = -1
				If _ExtMsgBox(48, $_Quit & '|&' & $_Retry, $_Warning, $_iniWriteError & '!' & @CRLF & @CRLF & $_iniWriteFix & '...') = 1 Then ExitClean()
			WEnd
			If $LogCreate = 0 Then FileDelete($LogPath)
		EndIf
		_WinAPI_RemoveFontResourceEx(@ScriptDir & '\Temp\Calibri.ttf')
		_WinAPI_RemoveFontResourceEx(@ScriptDir & '\Temp\Consolas.ttf')
		If $ExitLevel > 2 And FileExists(@ScriptDir & '\SMC_Launcher.exe') Then Run(@ScriptDir & '\SMC_Launcher.exe', @ScriptDir & '\')
		Exit
	EndFunc   ;==>ExitClean
	Func GetSysColor($i)
		Return (BitAND(_WinAPI_GetSysColor($i), 0xff) * 0x10000) + BitAND(_WinAPI_GetSysColor($i), 0xff00) + (BitAND(_WinAPI_GetSysColor($i), 0xff0000) / 0x10000)
	EndFunc   ;==>GetSysColor

	Func GUIClose()
		Switch @GUI_WinHandle
			Case $GUIMain
				ExitClean(2)
			Case $GUILoading
				GUIDelete($GUILoading)
			Case $GUIHelp
				GUISetState(@SW_HIDE, $GUIHelp)
				GUISwitch($GUIMain)
			Case $GUIListSelect
				MainListInitExit()
			Case $GUIUpdate
				UpdateCancel()
			Case $GUISettings
				If BitAND(GUICtrlGetState($SettingsButton), $GUI_ENABLE) And _ExtMsgBox(48, $_Apply & '|&' & $_Skip, $_Warning, $_ApplyModChanges & '?') = 1 Then
					GUISetState(@SW_HIDE, $GUISettings)
					SettingsApply()
				Else
					GUISetState(@SW_HIDE, $GUISettings)
					GUICtrlSetState($SettingsButton, $GUI_DISABLE)
					For $i = 0 To UBound($SettingsArray) - 1
						If $SettingsArray[$i][2] <> $SettingsArray[$i][3] Then $SettingsArray[$i][3] = $SettingsArray[$i][2]
						GUICtrlSetState($SettingsArray[$i][1], $SettingsArray[$i][3] ? $GUI_CHECKED : $GUI_UNCHECKED)
					Next
					If GUICtrlRead($SettingsArray[4][1]) = $GUI_CHECKED Then
						GUICtrlSetState($SettingsUseCompression, $GUI_ENABLE)
					Else
						GUICtrlSetState($SettingsUseCompression, $GUI_UNCHECKED)
						GUICtrlSetState($SettingsUseCompression, $GUI_DISABLE)
					EndIf
				EndIf
				GUISwitch($GUIMain)
			Case Else
				For $i = 0 To $ModArray[0] - 1
					If $GUIModEditor[$i] = @GUI_WinHandle Then
						_GUICtrlTreeView_SelectItem($ModEditorTreeView[$i], $ModEditorTreeViewItem[$i][0], $TVGN_CARET)
						ModEditorItem()
						_GUICtrlTreeView_SelectItem($ModEditorTreeView[$i], $ModEditorTreeViewItem[$i][2], $TVGN_CARET)
					EndIf
				Next
				GUISetState(@SW_HIDE, @GUI_WinHandle)
				GUISwitch($GUIMain)
		EndSwitch
	EndFunc   ;==>GUIClose

	Func GUIColorsDefine()
		$ColorArray = StringSplit(StringStripWS(IniRead($AppConfig, 'System', 'ColorFoundBoth', ''), 8), ',', 2)
		If UBound($ColorArray) <> 3 Or $ColorArray[0] < 0 Or $ColorArray[0] > 255 Or $ColorArray[1] < 0 Or $ColorArray[1] > 255 Or $ColorArray[2] < 0 Or $ColorArray[2] > 255 Then Global $ColorArray = [239, 239, 205] ; YELLOW
		$ColorFoundBoth = '0x' & Hex($ColorArray[0], 2) & Hex($ColorArray[1], 2) & Hex($ColorArray[2], 2)
		$ColorArray = StringSplit(StringStripWS(IniRead($AppConfig, 'System', 'ColorFoundArchived', ''), 8), ',', 2)
		If UBound($ColorArray) <> 3 Or $ColorArray[0] < 0 Or $ColorArray[0] > 255 Or $ColorArray[1] < 0 Or $ColorArray[1] > 255 Or $ColorArray[2] < 0 Or $ColorArray[2] > 255 Then Global $ColorArray = [205, 205, 239] ; BLUE
		$ColorFoundArchived = '0x' & Hex($ColorArray[0], 2) & Hex($ColorArray[1], 2) & Hex($ColorArray[2], 2)
		$ColorArray = StringSplit(StringStripWS(IniRead($AppConfig, 'System', 'ColorFoundExtracted', ''), 8), ',', 2)
		If UBound($ColorArray) <> 3 Or $ColorArray[0] < 0 Or $ColorArray[0] > 255 Or $ColorArray[1] < 0 Or $ColorArray[1] > 255 Or $ColorArray[2] < 0 Or $ColorArray[2] > 255 Then Global $ColorArray = [205, 239, 205] ; GREEN
		$ColorFoundExtracted = '0x' & Hex($ColorArray[0], 2) & Hex($ColorArray[1], 2) & Hex($ColorArray[2], 2)
		$ColorArray = StringSplit(StringStripWS(IniRead($AppConfig, 'System', 'ColorFoundUnsupported', ''), 8), ',', 2)
		If UBound($ColorArray) <> 3 Or $ColorArray[0] < 0 Or $ColorArray[0] > 255 Or $ColorArray[1] < 0 Or $ColorArray[1] > 255 Or $ColorArray[2] < 0 Or $ColorArray[2] > 255 Then Global $ColorArray = [139, 139, 139] ; DARK GRAY
		$ColorFoundUnsupported = '0x' & Hex($ColorArray[0], 2) & Hex($ColorArray[1], 2) & Hex($ColorArray[2], 2)
		$ColorArray = StringSplit(StringStripWS(IniRead($AppConfig, 'System', 'ColorFoundNone', ''), 8), ',', 2)
		If UBound($ColorArray) <> 3 Or $ColorArray[0] < 0 Or $ColorArray[0] > 255 Or $ColorArray[1] < 0 Or $ColorArray[1] > 255 Or $ColorArray[2] < 0 Or $ColorArray[2] > 255 Then Global $ColorArray = [239, 205, 205] ; RED
		$ColorFoundNone = '0x' & Hex($ColorArray[0], 2) & Hex($ColorArray[1], 2) & Hex($ColorArray[2], 2)
		$ColorArray = StringSplit(StringStripWS(IniRead($AppConfig, 'System', 'ColorContainsBoth', ''), 8), ',', 2)
		If UBound($ColorArray) <> 3 Or $ColorArray[0] < 0 Or $ColorArray[0] > 255 Or $ColorArray[1] < 0 Or $ColorArray[1] > 255 Or $ColorArray[2] < 0 Or $ColorArray[2] > 255 Then Global $ColorArray = [189, 0, 94] ; PURPLE
		$ColorContainsBoth = '0x' & Hex($ColorArray[2], 2) & Hex($ColorArray[1], 2) & Hex($ColorArray[0], 2)
		$ColorArray = StringSplit(StringStripWS(IniRead($AppConfig, 'System', 'ColorContainsESP', ''), 8), ',', 2)
		If UBound($ColorArray) <> 3 Or $ColorArray[0] < 0 Or $ColorArray[0] > 255 Or $ColorArray[1] < 0 Or $ColorArray[1] > 255 Or $ColorArray[2] < 0 Or $ColorArray[2] > 255 Then Global $ColorArray = [189, 94, 0] ; ORANGE
		$ColorContainsESP = '0x' & Hex($ColorArray[2], 2) & Hex($ColorArray[1], 2) & Hex($ColorArray[0], 2)
		$ColorArray = StringSplit(StringStripWS(IniRead($AppConfig, 'System', 'ColorContainsScripts', ''), 8), ',', 2)
		If UBound($ColorArray) <> 3 Or $ColorArray[0] < 0 Or $ColorArray[0] > 255 Or $ColorArray[1] < 0 Or $ColorArray[1] > 255 Or $ColorArray[2] < 0 Or $ColorArray[2] > 255 Then Global $ColorArray = [109, 78, 53] ; BROWN
		$ColorContainsScripts = '0x' & Hex($ColorArray[2], 2) & Hex($ColorArray[1], 2) & Hex($ColorArray[0], 2)
	EndFunc   ;==>GUIColorsDefine

	Func GUICommand($hWnd, $iMsg, $wParam, $lParam)
		#forceref $hWnd, $iMsg, $lParam
		Local $nNotifyCode = BitShift($wParam, 16), $nID = BitAND($wParam, 0xFFFF), $ChangeCounter = 0
		Select
			Case WinActive($GUISettings) And $nID <> $SettingsButton
				For $i = 0 To UBound($SettingsArray) - 1
					If $SettingsArray[$i][1] = $nID Then
						If $i = 4 Then
							If GUICtrlRead($SettingsArray[4][1]) = $GUI_CHECKED Then
								GUICtrlSetState($SettingsUseCompression, $GUI_ENABLE)
							Else
								GUICtrlSetState($SettingsUseCompression, $GUI_UNCHECKED)
								GUICtrlSetState($SettingsUseCompression, $GUI_DISABLE)
							EndIf
						EndIf
						For $i1 = 0 To UBound($SettingsArray) - 1
							$SettingsArray[$i1][3] = GUICtrlRead($SettingsArray[$i1][1]) = $GUI_CHECKED ? 1 : 0
							If $SettingsArray[$i1][2] <> $SettingsArray[$i1][3] Then $ChangeCounter += 1
						Next
					EndIf
				Next
				GUICtrlSetState($SettingsButton, ($ChangeCounter > 0 ? $GUI_ENABLE : $GUI_DISABLE))
			Case Else
				For $i = 0 To $ModArray[0] - 1
					If $ModEditorEdit[$i] = $nID Then
						Switch $nNotifyCode
							Case $EN_CHANGE
								If $ModEditorText[$i][1] = 'Directory' Then
									GUICtrlSetState($ModEditorButton[$i], (GUICtrlRead($ModEditorEdit[$i]) = $ModEditorText[$i][0] ? $GUI_DISABLE : (GUICtrlRead($ModEditorEdit[$i]) = '' ? $GUI_DISABLE : $GUI_ENABLE)))
								Else
									GUICtrlSetState($ModEditorButton[$i], (GUICtrlRead($ModEditorEdit[$i]) = $ModEditorText[$i][0] ? $GUI_DISABLE : $GUI_ENABLE))
								EndIf
							Case $EN_UPDATE
								If StringRight($ModEditorText[$i][1], 4) = 'Size' Then
									If StringRegExp(GUICtrlRead($ModEditorEdit[$i]), '^\d{0,5}\.?\d{0,2}$') Then
										$ModEditorText[$i][2] = GUICtrlRead($ModEditorEdit[$i])
									Else
										GUICtrlSetData($ModEditorEdit[$i], $ModEditorText[$i][2])
										DllCall("User32.dll", "int", "MessageBeep", "int", -1)
									EndIf
								EndIf
						EndSwitch
					EndIf
				Next
		EndSelect
		Return $GUI_RUNDEFMSG
	EndFunc   ;==>GUICommand

	Func GUICtrlGetBkColor($hWnd)
		If IsHWnd($hWnd) = 0 Then $hWnd = GUICtrlGetHandle($hWnd)
		Local $hDC = _WinAPI_GetDC($hWnd)
		Local $iColor = _WinAPI_GetPixel($hDC, 0, 0)
		_WinAPI_ReleaseDC($hWnd, $hDC)
		Return Hex($iColor, 6)
	EndFunc   ;==>GUICtrlGetBkColor

	Func GUIDoubleClick($hWnd, $iMsg, $wParam, $lParam)
		#forceref $hWnd, $iMsg, $wParam, $lParam
		SettingsCreate()
		Return $GUI_RUNDEFMSG
	EndFunc   ;==>GUIDoubleClick

	Func GUIHotKeys($Unset = 0)
		If $Unset Then
			_HotKey_Assign(BitOR($CK_CONTROL, 0x50))
			_HotKey_Assign(0x70)
			_HotKey_Assign(0x74)
			_HotKey_Assign(0x1B)
			_HotKey_Assign(0x09)
		Else
			_HotKey_Assign(BitOR($CK_CONTROL, 0x50), 'SettingsCreate', 0, $GUIMain)
			_HotKey_Assign(0x70, 'HelpCreate', 0, $GUIMain)
			_HotKey_Assign(0x74, 'CheckPathsRefresh', 0, $GUIMain)
			_HotKey_Assign(0x1B, 'ListViewSelectionClear', 0, $GUIMain)
			_HotKey_Assign(0x09, 'ListViewSelectionTab', 0, $GUIMain)
		EndIf
	EndFunc   ;==>GUIHotKeys

	Func GUIMove($hWnd, $iMsg, $wParam = 0, $lParam = 0)
		#forceref $iMsg, $wParam, $lParam
		If $hWnd = $GUIMain Then $GUIMainPosition = WinGetPos($GUIMain)
		Return $GUI_RUNDEFMSG
	EndFunc   ;==>GUIMove

	Func GUINotify($hWnd, $iMsg, $wParam, $lParam)
		#forceref $wParam
		$tNMHDR = DllStructCreate($tagNMHDR, $lParam)
		$hWnd = HWnd(DllStructGetData($tNMHDR, 'hWndFrom'))
		$iMsg = DllStructGetData($tNMHDR, 'Code')
		Local $tInfo, $iItem, $iSubItem, $iSubItem0Start, $iSubItem0End, $iSubItem1Start, $iSubItem1End, $iSubItemTop, $MousePosX, $MousePosY, $WinPosX, $WinPosY, $WinPosW
		Switch $hWnd
			Case GUICtrlGetHandle($ListView)
				Switch $iMsg
					Case $LVN_HOTTRACK
						$MousePosX = MouseGetPos()[0]
						$MousePosY = MouseGetPos()[1]
						$WinPosX = WinGetPos($hWnd)[0]
						$WinPosY = WinGetPos($hWnd)[1]
						$WinPosW = WinGetPos($hWnd)[2]
						$tInfo = DllStructCreate($tagNMLISTVIEW, $lParam)
						$iItem = DllStructGetData($tInfo, 'Item')
						$iSubItem = DllStructGetData($tInfo, 'SubItem')
						$iSubItem0Start = $WinPosX + 22
						$iSubItem0End = $iSubItem0Start + _StringSize(_GUICtrlListView_GetItemText($ListView, $iItem, 0), 9, Default, Default, 'Calibri')[2]
						$iSubItem1Start = $WinPosX + $xw + 6
						$iSubItem1End = $iSubItem1Start + _StringSize(_GUICtrlListView_GetItemText($ListView, $iItem, 1), 9, Default, Default, 'Calibri')[2]
						$iSubItemBottom = $WinPosY + _GUICtrlListView_GetSubItemRect($hWnd, $iItem, $iSubItem)[1] - 1
						$iSubItemTop = $WinPosY + _GUICtrlListView_GetSubItemRect($hWnd, $iItem, $iSubItem)[3] - 1
						If $iItem >= 0 And $iSubItem = 0 And $MousePosX > $iSubItem0Start And $MousePosX < $iSubItem0End And $ModStatus[$iItem] And StringInStr($ModStatus[$iItem], '|', 0, 3) And $MousePosY > $iSubItemBottom And $MousePosY < $iSubItemTop Then
							If $ModToolTip[$iItem] = 0 Then
								ToolTip(StringTrimLeft($ModStatus[$iItem], StringInStr($ModStatus[$iItem], '|', 0, 3)), ($iSubItem0End > $WinPosX + $WinPosW ? $WinPosX + $WinPosW - 18 : $iSubItem0End + 4), $iSubItemBottom, '', 0, 4)
								$ModToolTip[$iItem] = 1
							EndIf
						ElseIf $iItem >= 0 And $iSubItem = 1 And $MousePosX > $iSubItem1Start And $MousePosX < $iSubItem1End And $MousePosY > $iSubItemBottom And $MousePosY < $iSubItemTop Then
							If $ModToolTip[$iItem] = 0 Then
								ToolTip($ModState[$iItem], ($iSubItem1End > $WinPosX + $WinPosW ? $WinPosX + $WinPosW - 18 : $iSubItem1End + 4), $iSubItemBottom, '', 0, 4)
								$ModToolTip[$iItem] = 1
							EndIf
						Else
							ToolTip('')
							For $i = 0 To UBound($ModToolTip) - 1
								$ModToolTip[$i] = 0
							Next
						EndIf
					Case $NM_DBLCLK
						MainSpecifyPath(Int(_GUICtrlListView_GetSelectedIndices($ListView)))
					Case $NM_RCLICK
						MainLoop(0)
					Case $NM_CUSTOMDRAW
						Local $tCustDraw = DllStructCreate($tagNMLVCUSTOMDRAW, $lParam)
						$iSubItem = DllStructGetData($tCustDraw, 'iSubItem')
						$iItem = DllStructGetData($tCustDraw, 'dwItemSpec')
						Select
							Case $ModESP[$iItem] = 1 And $ModScripts[$iItem] = 1
								DllStructSetData($tCustDraw, 'clrText', $ColorContainsBoth)
							Case $ModESP[$iItem] = 1 And $ModScripts[$iItem] = 0
								DllStructSetData($tCustDraw, 'clrText', $ColorContainsESP)
							Case $ModESP[$iItem] = 0 And $ModScripts[$iItem] = 1
								DllStructSetData($tCustDraw, 'clrText', $ColorContainsScripts)
						EndSelect
					Case $LVN_BEGINDRAG
						Return 0
				EndSwitch
			Case GUICtrlGetHandle($HelpListView)
				Switch $iMsg
					Case $LVN_COLUMNCLICK
						$tInfo = DllStructCreate($tagNMLISTVIEW, $lParam)
						$iSubItem = DllStructGetData($tInfo, 'SubItem')
						_GUICtrlListView_SimpleSort($HelpListView, $SortDirection, $iSubItem)
					Case $LVN_BEGINDRAG
						Return 0
				EndSwitch
		EndSwitch
		Return $GUI_RUNDEFMSG
	EndFunc   ;==>GUINotify

	Func GUIRefresh($hWnd, $iMsg, $wParam, $lParam)
		#forceref $hWnd, $iMsg, $wParam, $lParam
		_GIF_RefreshGIF($SMCLoading)
	EndFunc   ;==>GUIRefresh

	Func GUIResize($hWnd, $iMsg, $wParam = 0, $lParam = 0)
		#forceref $iMsg, $wParam, $lParam
		Switch $hWnd
			Case $GUIMain
				_GUICtrlListView_BeginUpdate($ListView)
				$GUIMainPosition = WinGetPos($GUIMain)
				$GUIMainSize = WinGetClientSize($GUIMain)
				$MainPicWidth = $GUIMainSize[1] * 290 / 675
				GUICtrlSetPos($MainPic, 0, 0, $MainPicWidth, $GUIMainSize[1])
				GUICtrlSetPos($MainPicStripe, $MainPicWidth - 10, $GUIMainSize[1] - 2878, 10, 2878)
				ModLogoSet($ModLogoSelected, ($ModLogoSelected ? 0 : 1))
				_GUICtrlListView_SetColumnWidth($ListView, 1, $GUIMainSize[0] - $MainPicWidth - $xw - 174)
				GUICtrlSetPos($ListView, $MainPicWidth + 1, 0, $GUIMainSize[0] - $MainPicWidth - 1, $GUIMainSize[1] - $yh - 1)
				GUICtrlSetPos($MainProgress, $MainPicWidth + 1, $GUIMainSize[1] - $yh, $GUIMainSize[0] - $MainPicWidth - $BeginButtonWidth - 1, $yh - 1)
				GUICtrlSetPos($LabelProgressBlack, $MainPicWidth + 7, $GUIMainSize[1] - $yh + 3, $GUIMainSize[0] - $MainPicWidth - $BeginButtonWidth - 10, $yh - 6)
				GUICtrlSetPos($LabelProgressWhite, $MainPicWidth + 6, $GUIMainSize[1] - $yh + 2, $GUIMainSize[0] - $MainPicWidth - $BeginButtonWidth - 10, $yh - 6)
				GUICtrlSetPos($MainButton, $GUIMainSize[0] - $BeginButtonWidth, $GUIMainSize[1] - $yh - 1, $BeginButtonWidth, $yh + 1)
				$ListViewPageSize = _GUICtrlListView_GetCounterPage($ListView)
				_GUICtrlListView_EndUpdate($ListView)
			Case $GUIHelp
				_GUICtrlListView_BeginUpdate($HelpTreeView)
				$GUIHelpSize = WinGetClientSize($GUIHelp)
				GUICtrlSetPos($HelpTreeView, 0, -2, $GUIHelpTreeViewWidthMin, $GUIHelpSize[1] + 4)
				GUICtrlSetPos($HelpEdit, $GUIHelpTreeViewWidthMin + 4, 2, $GUIHelpSize[0] - $GUIHelpTreeViewWidthMin - 5, $GUIHelpSize[1] - 4)
				GUICtrlSetPos($HelpListView, $GUIHelpTreeViewWidthMin, 0, $GUIHelpSize[0] - $GUIHelpTreeViewWidthMin - 1, $GUIHelpSize[1] - 1)
				_GUICtrlListView_SetColumnWidth($HelpListView, 4, $GUIHelpSize[0] - $GUIHelpTreeViewWidthMin - _GUICtrlListView_GetColumnWidth($HelpListView, 0) - _GUICtrlListView_GetColumnWidth($HelpListView, 1) - _GUICtrlListView_GetColumnWidth($HelpListView, 2) - _GUICtrlListView_GetColumnWidth($HelpListView, 3) - 22)
				_GUICtrlListView_EndUpdate($HelpTreeView)
		EndSwitch
		Return $GUI_RUNDEFMSG
	EndFunc   ;==>GUIResize

	Func GUIResizeBounds($hWnd, $iMsg, $wParam, $lParam)
		#forceref $iMsg, $wParam
		Switch $hWnd
			Case $GUIMain
				$GUIWidthMin = $MainPicWidth + $GUIMainWidthMin
				$GUIHeightMin = $GUIMainHeightMin
			Case $GUIHelp
				$GUIWidthMin = $GUIMainWidthMin - 100
				$GUIHeightMin = $GUIMainHeightMin
			Case Else
				$GUIWidthMin = $GUIMainHeightMin - 100
				$GUIHeightMin = $GUIMainHeightMin - 100
		EndSwitch
		$GUISizeInfo = DllStructCreate('int;int;int;int;int;int;int;int', $lParam)
		DllStructSetData($GUISizeInfo, 7, $GUIWidthMin)
		DllStructSetData($GUISizeInfo, 8, $GUIHeightMin)
		Return $GUI_RUNDEFMSG
	EndFunc   ;==>GUIResizeBounds

	Func HelpCreate($HelpFocus = 0)
		If FileExists($AppLang) Then
			If $HelpFocus = 0 And $HelpFocusFirstOpen = 0 Then $HelpFocus = 1
			If BitAND(WinGetState($GUIHelp), 1) Then
				If BitAND(WinGetState($GUIHelp), 2) Then
					WinActivate($GUIHelp)
				Else
					GUISetState(@SW_SHOW, $GUIHelp)
				EndIf
			Else
				$GUIHelpPosition = WinGetPos($GUIMain)
				$GUIHelp = GUICreate($_HelpTitle & ' - SMC', $GUIHelpSize[0] - 1, $GUIHelpSize[1] - 54, $GUIHelpPosition[0], ($GUIHelpPosition[1] = -1 ? $GUIHelpPosition[1] : $GUIHelpPosition[1] + 27), BitOR($WS_MINIMIZEBOX, $WS_MAXIMIZEBOX, $WS_SIZEBOX))
				GUISetFont(9, Default, Default, 'Calibri', $GUIHelp, 6)
				$HelpTreeView = GUICtrlCreateTreeView(0, -2, $GUIHelpTreeViewWidthMin, $GUIHelpSize[1] + 4, BitOR($TVS_HASBUTTONS, $TVS_HASLINES, $TVS_LINESATROOT, $TVS_DISABLEDRAGDROP, $TVS_SHOWSELALWAYS, $TVS_TRACKSELECT), $WS_EX_CLIENTEDGE)
				$HelpEdit = GUICtrlCreateEdit('', $GUIHelpTreeViewWidthMin + 4, 2, $GUIHelpSize[0] - $GUIHelpTreeViewWidthMin - 10, $GUIHelpSize[1] - 4, BitOR($WS_VSCROLL, $ES_READONLY, $ES_NOHIDESEL), 0)
				GUICtrlSetFont(-1, 9, Default, Default, 'Consolas', 6)
				$HelpSection = IniReadSection($AppLang, 'Help')
				If Not @error Then
					$HelpCreateItemsCreated += 1
					Global $HelpTreeViewItem[$HelpSection[0][0] + 1]
					For $i = 1 To $HelpSection[0][0]
						$HelpTreeViewItem[$i] = GUICtrlCreateTreeViewItem($HelpSection[$i][0], $HelpTreeView)
						GUICtrlSetOnEvent(-1, 'HelpItem')
					Next
				EndIf
				$FAQSection = IniReadSection($AppLang, 'FAQ')
				If Not @error Then
					$HelpCreateItemsCreated += 2
					Global $FAQTreeViewItem[$FAQSection[0][0] + 1]
					$FAQTreeView = GUICtrlCreateTreeViewItem($_FAQTitle, $HelpTreeView)
					GUICtrlSetOnEvent(-1, 'HelpItem')
					For $i = 1 To $FAQSection[0][0]
						$FAQTreeViewItem[$i] = GUICtrlCreateTreeViewItem($FAQSection[$i][0], $FAQTreeView)
						GUICtrlSetOnEvent(-1, 'HelpItem')
					Next
				EndIf
				$ChangelogSection = IniReadSection($AppLang, 'Changelog')
				If Not @error Then
					$HelpCreateItemsCreated += 4
					Global $ChangelogTreeViewItem[$ChangelogSection[0][0] + 1]
					$ChangelogTreeView = GUICtrlCreateTreeViewItem($_ChangelogTitle, $HelpTreeView)
					GUICtrlSetOnEvent(-1, 'HelpItem')
					For $i = 1 To $ChangelogSection[0][0]
						$ChangelogTreeViewItem[$i] = GUICtrlCreateTreeViewItem($ChangelogSection[$i][0], $ChangelogTreeView)
						GUICtrlSetOnEvent(-1, 'HelpItem')
					Next
				EndIf
				If FileExists($AppConfig) Then
					$HelpCreateItemsCreated += 8
					$CompatibilityTreeView = GUICtrlCreateTreeViewItem($_CompatibilityTitle, $HelpTreeView)
					GUICtrlSetOnEvent(-1, 'HelpItem')
				EndIf
				$HelpListView = GUICtrlCreateListView(_StringTitleCase($_ModName & '|' & $_StatusMsg & '|' & $_LastEdit & '|' & $_VersionLatest & '|' & $_AdditionalInfo), $GUIHelpTreeViewWidthMin - 1, -2, $GUIHelpSize[0] - $GUIHelpTreeViewWidthMin - 2, $GUIHelpSize[1] - 75, _
						BitOR($LVS_REPORT, $LVS_SHOWSELALWAYS, $LVS_SINGLESEL), _
						BitOR($LVS_EX_HEADERDRAGDROP, $LVS_EX_FULLROWSELECT, $LVS_EX_DOUBLEBUFFER))
				GUICtrlSetBkColor($HelpListView, GetSysColor(15))
				_GUICtrlListView_SetColumnWidth($HelpListView, 0, ($ModNameLongest > $ModRemovedNameLongest ? $ModNameLongest : $ModRemovedNameLongest) + 10)
				_GUICtrlListView_JustifyColumn($HelpListView, 0, 0)
				_GUICtrlListView_SetColumnWidth($HelpListView, 1, _StringSize(_StringTitleCase($_StatusMsg), 9, Default, Default, 'Calibri')[2] + 12)
				_GUICtrlListView_JustifyColumn($HelpListView, 1, 2)
				_GUICtrlListView_SetColumnWidth($HelpListView, 2, _StringSize($_LastEdit, 9, Default, Default, 'Calibri')[2] + 12)
				_GUICtrlListView_JustifyColumn($HelpListView, 2, 2)
				_GUICtrlListView_SetColumnWidth($HelpListView, 3, _StringSize($_VersionLatest, 9, Default, Default, 'Calibri')[2] + 12)
				_GUICtrlListView_JustifyColumn($HelpListView, 3, 2)
				_GUICtrlListView_SetColumnWidth($HelpListView, 4, $GUIHelpSize[0] - $GUIHelpTreeViewWidthMin - _GUICtrlListView_GetColumnWidth($HelpListView, 0) - _GUICtrlListView_GetColumnWidth($HelpListView, 1) - _GUICtrlListView_GetColumnWidth($HelpListView, 2) - _GUICtrlListView_GetColumnWidth($HelpListView, 3))
				_GUICtrlListView_JustifyColumn($HelpListView, 4, 0)
				For $i = 0 To $ModArray[0] - 1
					$HelpListViewItem[$i] = GUICtrlCreateListViewItem($ModName[$i] & '|' & $ModStatus[$i], $HelpListView)
				Next
				For $i = 0 To $ModRemovedArray[0] - 1
					$HelpListViewItem[$i] = GUICtrlCreateListViewItem($ModRemovedName[$i] & '|' & $ModRemovedStatus[$i], $HelpListView)
				Next
				Global $SortDirection[_GUICtrlListView_GetColumnCount($HelpListView)]
				If $HelpFocus = 3 And StringInStr('15 14 13 12 11 10 9 8', $HelpCreateItemsCreated) Then
					Local $SortDirection = True
					_GUICtrlListView_SimpleSort($HelpListView, $SortDirection, 2)
				Else
					_GUICtrlListView_SimpleSort($HelpListView, $SortDirection, 0)
				EndIf
				GUICtrlSetState($HelpListView, $GUI_HIDE)
				GUICtrlSetState($HelpEdit, $GUI_HIDE)
				GUISetOnEvent($GUI_EVENT_CLOSE, 'GUIClose', $GUIHelp)
				If $HelpCreateItemsCreated > 0 Then
					GUISetState(@SW_SHOW, $GUIHelp)
					Select
						Case $HelpFocus = 1 And StringInStr('15 13 11 9 7 5 3 1', $HelpCreateItemsCreated)
							If $HelpSection[0][0] > 1 Then _GUICtrlTreeView_SelectItem($HelpTreeView, $HelpTreeViewItem[2], $TVGN_CARET)
						Case $HelpFocus = 2 And StringInStr('15 14 13 12 7 6 5 4', $HelpCreateItemsCreated)
							If $HelpSection[0][0] > 0 Then _GUICtrlTreeView_SelectItem($HelpTreeView, $ChangelogTreeViewItem[1], $TVGN_CARET)
						Case $HelpFocus = 3 And StringInStr('15 14 13 12 11 10 9 8', $HelpCreateItemsCreated)
							If $HelpSection[0][0] > 0 Then _GUICtrlTreeView_SelectItem($HelpTreeView, $CompatibilityTreeView, $TVGN_CARET)
					EndSelect
				Else
					GUIDelete($GUIHelp)
				EndIf
			EndIf
			$HelpFocusFirstOpen = 1
			$HelpFocus = 0
		EndIf
	EndFunc   ;==>HelpCreate

	Func HelpItem()
		GUICtrlSetState($HelpListView, $GUI_HIDE)
		GUICtrlSetState($HelpEdit, $GUI_HIDE)
		GUICtrlSetData($HelpEdit, '')
		If StringInStr('15 13 11 9 7 5 3 1', $HelpCreateItemsCreated) Then
			For $i = 1 To $HelpSection[0][0]
				If $HelpTreeViewItem[$i] = @GUI_CtrlId Then
					GUICtrlSetState($HelpEdit, $GUI_SHOW)
					GUICtrlSetData($HelpEdit, StringReplace(StringReplace($HelpSection[$i][1], '|', @CRLF), '¦', '|'))
				EndIf
			Next
		EndIf
		If StringInStr('15 14 11 10 7 6 3 2', $HelpCreateItemsCreated) Then
			If $FAQTreeView = @GUI_CtrlId Then GUICtrlSetData($HelpEdit, StringReplace(StringReplace($_FAQDesc, '|', @CRLF), '¦', '|'))
			For $i = 1 To $FAQSection[0][0]
				If $FAQTreeViewItem[$i] = @GUI_CtrlId Then
					GUICtrlSetState($HelpEdit, $GUI_SHOW)
					GUICtrlSetData($HelpEdit, StringReplace(StringReplace($FAQSection[$i][1], '|', @CRLF), '¦', '|'))
				EndIf
			Next
		EndIf
		If StringInStr('15 14 13 12 7 6 5 4', $HelpCreateItemsCreated) Then
			If $ChangelogTreeView = @GUI_CtrlId Then GUICtrlSetData($HelpEdit, StringReplace(StringReplace($_ChangelogDesc, '|', @CRLF), '¦', '|'))
			For $i = 1 To $ChangelogSection[0][0]
				If $ChangelogTreeViewItem[$i] = @GUI_CtrlId Then
					GUICtrlSetState($HelpEdit, $GUI_SHOW)
					GUICtrlSetData($HelpEdit, StringReplace(StringReplace($ChangelogSection[$i][1], '|', @CRLF), '¦', '|'))
				EndIf
			Next
		EndIf
		If StringInStr('15 14 13 12 11 10 9 8', $HelpCreateItemsCreated) Then
			If $CompatibilityTreeView = @GUI_CtrlId Then
				GUICtrlSetState($HelpListView, $GUI_SHOW)
			EndIf
		EndIf
	EndFunc   ;==>HelpItem

	Func IniFormat($iniInput)
		If FileExists($iniInput) Then
			FileDelete($TempFile)
			Local $FileMoveRetries = 0, $FileMoveResult = 0, $iniLineLongest = 0
			IniFormatSection($iniInput, 'System')
			IniFormatSection($iniInput, 'Checksums')
			IniFormatSection($iniInput, 'Defined Paths')
			IniFormatSection($iniInput, 'Compatibility Patches')
			Local $iniFileIn = FileOpen($iniInput, 0)
			If $iniFileIn = -1 Then Return -1
			$iniLineLongest = 0
			While 1
				Local $iniLine = FileReadLine($iniFileIn)
				If @error = -1 Then ExitLoop
				$iniLine = StringSplit($iniLine, '=')
				If $iniLine[0] > 1 And StringLen(StringStripWS($iniLine[1], 3)) > $iniLineLongest Then $iniLineLongest = StringLen(StringStripWS($iniLine[1], 3))
			WEnd
			FileClose($iniFileIn)
			Local $iniFileTemp = FileOpen($TempFile, 9)
			Local $iniFileOut = FileOpen($iniInput, 0)
			If $iniFileTemp = -1 Or $iniFileOut = -1 Then Return -1
			While 1
				$iniLine = FileReadLine($iniFileOut)
				If @error = -1 Then ExitLoop
				$iniLine = StringSplit($iniLine, '=')
				$iniLine[1] = StringStripWS($iniLine[1], 3)
				If $iniLine[0] > 1 Then
					For $i = 1 To $iniLineLongest - StringLen($iniLine[1])
						$iniLine[1] &= ' '
					Next
					$iniLine[1] &= ' = ' & StringReplace(StringReplace((StringRight(StringStripWS($iniLine[2], 3), 1) = ';' ? StringTrimRight(StringStripWS($iniLine[2], 3), 1) : StringRight(StringStripWS($iniLine[2], 3), 1) = '|') ? StringTrimRight(StringStripWS($iniLine[2], 3), 1) : StringStripWS($iniLine[2], 3), ';;', ';'), '; ;', ';')
				EndIf
				FileWriteLine($iniFileTemp, $iniLine[1])
			WEnd
			FileClose($iniFileTemp)
			FileClose($iniFileOut)
			Do
				$FileMoveResult = FileMove($TempFile, $iniInput, 9)
				If $FileMoveResult <> 1 Then
					$FileMoveRetries += 1
					Sleep(100)
				EndIf
				If $FileMoveRetries > 10 Then Return -1
			Until $FileMoveResult = 1
		Else
			Sleep(10)
			Return -1
		EndIf
	EndFunc   ;==>IniFormat

	Func IniFormatSection($iniInput, $Section)
		Local $iniInputSection = IniReadSection($iniInput, $Section)
		If Not @error Then
			If IniDelete($iniInput, $Section) = 0 Then Return -1
			$iniLineLongest = 0
			For $i = 1 To $iniInputSection[0][0]
				If StringLen(StringStripWS($iniInputSection[$i][0], 3)) > $iniLineLongest Then $iniLineLongest = StringLen(StringStripWS($iniInputSection[$i][0], 3))
			Next
			Local $iniFileTemp = FileOpen($TempFile, 9)
			If $iniFileTemp = -1 Then Return -1
			FileWriteLine($iniFileTemp, '[' & $Section & ']')
			For $i = 1 To $iniInputSection[0][0]
				If $iniInputSection[0][0] > 1 Then
					For $i1 = 1 To $iniLineLongest - StringLen($iniInputSection[$i][0])
						$iniInputSection[$i][0] &= ' '
					Next
				EndIf
				FileWriteLine($iniFileTemp, $iniInputSection[$i][0] & ' = ' & (StringRight(StringStripWS($iniInputSection[$i][1], 3), 1) = ';' ? StringTrimRight(StringStripWS($iniInputSection[$i][1], 3), 1) : _
						StringRight(StringStripWS($iniInputSection[$i][1], 3), 1) = '|' ? StringTrimRight(StringStripWS($iniInputSection[$i][1], 3), 1) : StringStripWS($iniInputSection[$i][1], 3)))
			Next
			FileClose($iniFileTemp)
		EndIf
	EndFunc   ;==>IniFormatSection

	Func ListViewItemContextMenuChange($i, $ModFound = 0)
		GUICtrlDelete($ListViewItemContextMenu[$i])
		$ListViewItemContextMenu[$i] = GUICtrlCreateContextMenu($ListViewItem[$i])
		If $ModFound Then
			$MenuListSpecifyPath[$i] = GUICtrlCreateMenuItem($_SpecifyModPath, $ListViewItemContextMenu[$i])
			GUICtrlSetOnEvent(-1, 'MainMenu')
			GUICtrlCreateMenuItem('', $ListViewItemContextMenu[$i])
			If $EditorMode = 1 Then
				$MenuListEditor[$i] = GUICtrlCreateMenuItem($_EditModSettings, $ListViewItemContextMenu[$i])
				GUICtrlSetOnEvent(-1, 'MainMenu')
				GUICtrlCreateMenuItem('', $ListViewItemContextMenu[$i])
			EndIf
			$MenuListDownload[$i] = GUICtrlCreateMenuItem($_DownloadMod, $ListViewItemContextMenu[$i])
			GUICtrlSetOnEvent(-1, 'MainMenu')
			GUICtrlCreateMenuItem('', $ListViewItemContextMenu[$i])
			If _GUICtrlListView_GetItemText($ListView, $i, 1) Then
				$MenuListOpen[$i] = GUICtrlCreateMenuItem($_OpenPath, $ListViewItemContextMenu[$i])
				GUICtrlSetOnEvent(-1, 'MainMenu')
				GUICtrlCreateMenuItem('', $ListViewItemContextMenu[$i])
			EndIf
			$MenuListCheckAll[$i] = GUICtrlCreateMenuItem($_CheckAll, $ListViewItemContextMenu[$i])
			GUICtrlSetOnEvent(-1, 'MainMenu')
			$MenuListUncheckAll[$i] = GUICtrlCreateMenuItem($_UncheckAll, $ListViewItemContextMenu[$i])
			GUICtrlSetOnEvent(-1, 'MainMenu')
			GUICtrlCreateMenuItem('', $ListViewItemContextMenu[$i])
			$MenuListPathAll[$i] = GUICtrlCreateMenuItem($_SpecifyPath & ' "' & _GUICtrlListView_GetItemText($ListView, $i, 1) & '" ' & $_PathAll, $ListViewItemContextMenu[$i])
			GUICtrlSetOnEvent(-1, 'MainMenu')
			$MenuListPathAllUnspec[$i] = GUICtrlCreateMenuItem($_SpecifyPath & ' "' & _GUICtrlListView_GetItemText($ListView, $i, 1) & '" ' & $_PathAllUnspec, $ListViewItemContextMenu[$i])
			GUICtrlSetOnEvent(-1, 'MainMenu')
			GUICtrlCreateMenuItem('', $ListViewItemContextMenu[$i])
			$MenuListClearCurrent[$i] = GUICtrlCreateMenuItem($_ClearCurrent, $ListViewItemContextMenu[$i])
			GUICtrlSetOnEvent(-1, 'MainMenu')
			$MenuListClearAllEx[$i] = GUICtrlCreateMenuItem($_ClearAllEx, $ListViewItemContextMenu[$i])
			GUICtrlSetOnEvent(-1, 'MainMenu')
			$MenuListClearAll[$i] = GUICtrlCreateMenuItem($_ClearAll, $ListViewItemContextMenu[$i])
			GUICtrlSetOnEvent(-1, 'MainMenu')
		Else
			GUICtrlSetState($ListViewItem[$i], $GUI_UNCHECKED)
			_GUICtrlListView_SetItemText($ListView, $i, '', 1)
			For $i1 = 0 To 3
				If StringLeft(_GUICtrlListView_GetItemText($ListView, $i, 5 - $i1), 1) = '[' Then _GUICtrlListView_SetItemText($ListView, $i, $QualityArray[$i1], 5 - $i1)
			Next
			GUICtrlSetBkColor($ListViewItem[$i], GetSysColor(15))
			$MenuListSpecifyPath[$i] = GUICtrlCreateMenuItem($_SpecifyModPath, $ListViewItemContextMenu[$i])
			GUICtrlSetOnEvent(-1, 'MainMenu')
			GUICtrlCreateMenuItem('', $ListViewItemContextMenu[$i])
			If $EditorMode = 1 Then
				$MenuListEditor[$i] = GUICtrlCreateMenuItem($_EditModSettings, $ListViewItemContextMenu[$i])
				GUICtrlSetOnEvent(-1, 'MainMenu')
				GUICtrlCreateMenuItem('', $ListViewItemContextMenu[$i])
			EndIf
			$MenuListDownload[$i] = GUICtrlCreateMenuItem($_DownloadMod, $ListViewItemContextMenu[$i])
			GUICtrlSetOnEvent(-1, 'MainMenu')
			GUICtrlCreateMenuItem('', $ListViewItemContextMenu[$i])
			If _GUICtrlListView_GetItemText($ListView, $i, 1) Then
				$MenuListOpen[$i] = GUICtrlCreateMenuItem($_OpenPath, $ListViewItemContextMenu[$i])
				GUICtrlSetOnEvent(-1, 'MainMenu')
				GUICtrlCreateMenuItem('', $ListViewItemContextMenu[$i])
			EndIf
			$MenuListCheckAll[$i] = GUICtrlCreateMenuItem($_CheckAll, $ListViewItemContextMenu[$i])
			GUICtrlSetOnEvent(-1, 'MainMenu')
			$MenuListUncheckAll[$i] = GUICtrlCreateMenuItem($_UncheckAll, $ListViewItemContextMenu[$i])
			GUICtrlSetOnEvent(-1, 'MainMenu')
			GUICtrlCreateMenuItem('', $ListViewItemContextMenu[$i])
			$MenuListClearAll[$i] = GUICtrlCreateMenuItem($_ClearAll, $ListViewItemContextMenu[$i])
			GUICtrlSetOnEvent(-1, 'MainMenu')
		EndIf
	EndFunc   ;==>ListViewItemContextMenuChange

	Func ListViewSelectionClear()
		_GUICtrlListView_SetItemSelected($ListView, -1, 0, 0)
	EndFunc   ;==>ListViewSelectionClear

	Func ListViewSelectionTab()
		If BitAND(GUICtrlGetState($MainButton), $GUI_ENABLE) Then
			_GUICtrlButton_SetFocus($MainButton)
		Else
			ListViewSelectionClear()
		EndIf
	EndFunc   ;==>ListViewSelectionTab

	Func LogWrite($LogMsg)
		If $NoLogging <> 1 Then _FileWriteLog($LogPath, $LogMsg)
	EndFunc   ;==>LogWrite

	Func MainCreate()
		MainListInit()
		ModListInit()
		Local $WorkingArea = CheckWorkingArea()
		If $WorkingArea <> 0 Then
			If $GUIMainSize[0] > $WorkingArea[2] - $WorkingArea[0] Then $GUIMainSize[0] = $WorkingArea[2] - $WorkingArea[0] - 20
			If $GUIMainSize[1] > $WorkingArea[3] - $WorkingArea[1] Then $GUIMainSize[1] = $WorkingArea[3] - $WorkingArea[1] - 20
			If $GUIHelpSize[0] > $WorkingArea[2] - $WorkingArea[0] Then $GUIHelpSize[0] = $WorkingArea[2] - $WorkingArea[0] - 20
			If $GUIHelpSize[1] > $WorkingArea[3] - $WorkingArea[1] Then $GUIHelpSize[1] = $WorkingArea[3] - $WorkingArea[1] - 20
		EndIf
		$GUIMain = GUICreate('SMC ' & $AppVer[1] & '.' & $AppVer[2] & '.' & $AppVer[3] & '.' & $AppVer[4] & ' ' & $AppBuild, $GUIMainSize[0], $GUIMainSize[1], $GUIMainPosition[0], $GUIMainPosition[1], BitOR($WS_MINIMIZEBOX, $WS_MAXIMIZEBOX, $WS_SIZEBOX))
		GUISetFont(9, Default, Default, 'Calibri', $GUIMain, 6)
		$GUIMainPosition = WinGetPos($GUIMain)
		$GUIMainSize = WinGetClientSize($GUIMain)
		$MainPicWidth = $GUIMainSize[1] * 290 / 675
		GUISetOnEvent($GUI_EVENT_CLOSE, 'GUIClose', $GUIMain)
		$GUIMainContextMenu = GUICtrlCreateContextMenu(-1)
		$MenuMainAppPageOpen = GUICtrlCreateMenuItem($_OpenAppPage, $GUIMainContextMenu)
		GUICtrlSetOnEvent($MenuMainAppPageOpen, 'PageOpenApp')
		GUICtrlCreateMenuItem('', $GUIMainContextMenu)
		$MenuMainModPageOpen = GUICtrlCreateMenuItem($_OpenModPage, $GUIMainContextMenu)
		GUICtrlSetOnEvent($MenuMainModPageOpen, 'PageOpenMod')
		GUICtrlCreateMenuItem('', $GUIMainContextMenu)
		$MenuMainSettings = GUICtrlCreateMenuItem($_OpenSettings, $GUIMainContextMenu)
		GUICtrlSetOnEvent($MenuMainSettings, 'SettingsCreate')
		$MenuMainHelp = GUICtrlCreateMenuItem($_OpenHelp, $GUIMainContextMenu)
		GUICtrlSetOnEvent($MenuMainHelp, 'HelpCreate')
		$MainPic = GUICtrlCreatePic('', 0, 0, $MainPicWidth, $GUIMainSize[1], $SS_CENTERIMAGE, $GUI_WS_EX_PARENTDRAG)
		_SetImage($MainPic, $SMCLogo)
		$MainPicStripe = GUICtrlCreatePic('', $MainPicWidth - 10, $GUIMainSize[1] - 2878, 10, 2878, $SS_RIGHTJUST, $GUI_WS_EX_PARENTDRAG)
		_SetImage($MainPicStripe, $SMCStripe)
		$ModLogoSelected = 0
		GUICtrlSetState($MainPic, $GUI_DISABLE)
		$ListView = GUICtrlCreateListView($_ModName & '|' & $_SpecifiedPath & '|' & $QualityArray[3] & '|' & $QualityArray[2] & '|' & $QualityArray[1] & '|' & $QualityArray[0], $MainPicWidth + 1, 0, $GUIMainSize[0] - $MainPicWidth - 1, $GUIMainSize[1] - $yh - 1, _
				BitOR($LVS_REPORT, $LVS_SHOWSELALWAYS, $LVS_SINGLESEL, $LVS_NOSORTHEADER, $LVS_NOCOLUMNHEADER), _
				BitOR($LVS_EX_CHECKBOXES, $LVS_EX_HEADERDRAGDROP, $LVS_EX_FULLROWSELECT, $LVS_EX_DOUBLEBUFFER))
		GUICtrlSetBkColor($ListView, GetSysColor(15))
		_GUICtrlListView_SetColumnWidth($ListView, 0, $xw)
		_GUICtrlListView_SetColumnWidth($ListView, 1, $GUIMainSize[0] - $MainPicWidth - $xw - 174)
		_GUICtrlListView_SetColumn($ListView, 2, '', 38, 2)
		_GUICtrlListView_SetColumn($ListView, 3, '', 38, 2)
		_GUICtrlListView_SetColumn($ListView, 4, '', 38, 2)
		_GUICtrlListView_SetColumn($ListView, 5, '', 38, 2)
		For $i = 0 To $ModArray[0] - 1
			If IniRead($AppConfig, 'Defined Paths', $ModArray[$i + 1], '') Then
				$ListViewItem[$i] = GUICtrlCreateListViewItem($ModName[$i] & '|' & IniRead($AppConfig, 'Defined Paths', $ModArray[$i + 1], '') & '|' & _
						(IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[3] & 'Archives', '') ? $QualityArray[3] : '') & '|' & _
						(IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[2] & 'Archives', '') ? $QualityArray[2] : '') & '|' & _
						(IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[1] & 'Archives', '') ? $QualityArray[1] : '') & '|' & _
						(IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[0] & 'Archives', '') ? $QualityArray[0] : ''), $ListView)
				$ModSize[$i] = CheckPath($i)
				ListViewItemContextMenuChange($i, 1)
				$ModFolder = IniRead($AppConfig, 'Defined Paths', $ModArray[$i + 1], '')
			Else
				$ListViewItem[$i] = GUICtrlCreateListViewItem($ModName[$i] & '||' & _
						(IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[3] & 'Archives', '') ? $QualityArray[3] : '') & '|' & _
						(IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[2] & 'Archives', '') ? $QualityArray[2] : '') & '|' & _
						(IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[1] & 'Archives', '') ? $QualityArray[1] : '') & '|' & _
						(IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[0] & 'Archives', '') ? $QualityArray[0] : ''), $ListView)
			EndIf
			If $ModESP[$i] Then _GUICtrlTreeView_SetState($ListView, $ListViewItem[$i], $TVIS_BOLD)
			ListViewItemContextMenuChange($i, (_GUICtrlListView_GetItemText($ListView, $i, 1) ? 1 : 0))
		Next
		$MainProgress = GUICtrlCreatePic('', $MainPicWidth + 1, $GUIMainSize[1] - $yh, $GUIMainSize[0] - $MainPicWidth - $BeginButtonWidth - 1, $yh - 1)
		GUICtrlSetState($MainProgress, $GUI_DISABLE)
		$LabelProgressBlack = GUICtrlCreateLabel($_StatusMsg & ': ' & $_Presetting, $MainPicWidth + 7, $GUIMainSize[1] - $yh + 3, $GUIMainSize[0] - $MainPicWidth - $BeginButtonWidth - 10, $yh - 6, $SS_LEFTNOWORDWRAP, $WS_EX_TRANSPARENT)
		GUICtrlSetColor($LabelProgressBlack, 0x000000)
		GUICtrlSetBkColor($LabelProgressBlack, $GUI_BKCOLOR_TRANSPARENT)
		$LabelProgressWhite = GUICtrlCreateLabel($_StatusMsg & ': ' & $_Presetting, $MainPicWidth + 6, $GUIMainSize[1] - $yh + 2, $GUIMainSize[0] - $MainPicWidth - $BeginButtonWidth - 10, $yh - 6, $SS_LEFTNOWORDWRAP, $WS_EX_TRANSPARENT)
		GUICtrlSetColor($LabelProgressWhite, 0xFFFFFF)
		GUICtrlSetBkColor($LabelProgressWhite, $GUI_BKCOLOR_TRANSPARENT)
		GUICtrlSetState($LabelProgressWhite, $GUI_ONTOP)
		$MainButton = GUICtrlCreateButton($_BeginButton, $GUIMainSize[0] - $BeginButtonWidth, $GUIMainSize[1] - $yh - 1, $BeginButtonWidth, $yh, $BS_DEFPUSHBUTTON)
		GUICtrlSetState($MainButton, $GUI_ONTOP)
		GUICtrlSetState($MainButton, $GUI_DISABLE)
		$ListViewPageSize = _GUICtrlListView_GetCounterPage($ListView)
		GUIHotKeys()
		GUISetCursor(2, 1, $GUILoading)
		For $i = 255 To 0 Step -2.5
			WinSetTrans($GUILoading, '', $i)
			Sleep(1)
		Next
		GUIDelete($GUILoading)
		GUIRegisterMsg($WM_NOTIFY, 'GUINotify')
		GUIRegisterMsg($WM_COMMAND, 'GUICommand')
		_ITaskBar_CreateTaskBarObj()
		GUIRegisterMsg($WM_SIZE, 'GUIResize')
		GUIRegisterMsg($WM_MOVE, 'GUIMove')
		GUIRegisterMsg($WM_GETMINMAXINFO, 'GUIResizeBounds')
		GUIRegisterMsg($WM_LBUTTONDBLCLK, 'GUIDoubleClick')
		GUISetState(@SW_SHOW, $GUIMain)
		ModLogoCacheUpdate()
		Select
			Case IniRead($AppConfig, 'System', 'VersionCurrent', '') = '' Or IniRead($AppConfig, 'System', 'VersionCurrent', '') = '0.0.0.0'
				HelpCreate(1)
			Case IniRead($AppConfig, 'System', 'VersionCurrent', '') <> $AppVer[1] & '.' & $AppVer[2] & '.' & $AppVer[3] & '.' & $AppVer[4]
				HelpCreate(2)
			Case Else
				If $AppConfigUpdated = 1 Then HelpCreate(3)
		EndSelect
		$RefreshTrigger = 1
		GUICtrlSetState($MainButton, $GUI_DISABLE)
		GUICtrlSetData($MainButton, $_BeginButton)
		GUICtrlSetTip($MainButton, $_ButtonBeginTip)
		GUICtrlSetOnEvent($MainButton, 'CombineBegin')
		MainProgressSet(0, $_StatusMsg & ': ' & $_Presetting, 0)
		If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 2)
		MainLoop()
	EndFunc   ;==>MainCreate

	Func MainDownload($i)
		If $ModLink[$i] Then
			$Links = StringSplit($ModLink[$i], ';')
			For $i = 1 To $Links[0]
				If StringLeft(StringStripWS($Links[$i], 3), 4) = 'http' Then ShellExecute(StringStripWS($Links[$i], 3))
			Next
		EndIf
	EndFunc   ;==>MainDownload

	Func MainListInit()
		Local $ListContents
		If @Compiled Then
			DirCreate($TempDir & '\SMC_List')
			If _IsPressed(11) Then MainListInitFirst()
			While 1
				CheckFileUpdate($ListPath)
				If FileExists($ListPath) Then
					While Not FileExists($7zExe) Or Not FileExists(StringTrimRight($7zExe, 4) & '.dll')
						If _ExtMsgBox(48, $_Quit & '|&' & $_Proceed, $_Warning, $_7zWarning & ' :(' & @CRLF & @CRLF & $_SelectManually & '...') = 1 Then ExitClean(2)
						WinFlash($GUIMain, '', 3, 400)
						$7zExe = FileOpenDialog($_Choose7zExe, @ScriptDir & '\', $_Executable & ' (*.exe)', 3)
					WEnd
					If RunProcessing($ListPath, $TempDir & '\SMC_List') = 0 Then
						_FileReadToArray($TempDir & '\SMC_List' & '\' & StringTrimRight(StringTrimLeft($ListPath, StringInStr($ListPath, '\', 0, -1)), 4) & '.ini', $ListContents)
						If @error Or $ListContents[1] <> '[Skyrim]' Then
							MainListInitError()
						Else
							ExitLoop
						EndIf
					Else
						MainListInitError()
					EndIf
				Else
					MainListInitError()
				EndIf
				Sleep(10)
			WEnd
			DirRemove($TempDir & '\SMC_List', 1)
		Else
			$ListPath = @ScriptDir & '\List\SMC.ini'
			_FileReadToArray($ListPath, $ListContents)
			If @error Or $ListContents[1] <> '[Skyrim]' Then MainListInitError()
		EndIf
		LogWrite('[L] ' & $_ListSelected & ': ' & $ListPath & @CRLF)
		CheckDLC($ListContents)
		Local $ListContentsSection = 0
		If @OSArch = 'X86' Then
			_SQLite_Startup(@ScriptDir & '\' & (@Compiled ? 'Temp' : 'x86') & '\sqlite3.dll')
		Else
			_SQLite_Startup(@ScriptDir & '\' & (@Compiled ? 'Temp' : 'x64') & '\sqlite3_x64.dll')
		EndIf
		If @Compiled = 0 Then ConsoleWrite('SQLite_LibVersion = ' & _SQLite_LibVersion() & @CRLF)
		If @error Then
			GUISetCursor(2, 1, $GUILoading)
			GUISetState(@SW_HIDE, $GUILoading)
			_ExtMsgBox(16, '&' & $_Quit, $_Error, $_SQLiteInitError & ' :(')
			ExitClean(1)
		EndIf
		_SQLite_Open()
		If @error Then
			GUISetCursor(2, 1, $GUILoading)
			GUISetState(@SW_HIDE, $GUILoading)
			_ExtMsgBox(16, '&' & $_Quit, $_Error, $_SQLiteWriteError & ' :(')
			ExitClean(1)
		EndIf
		_SQLite_Exec(-1, 'CREATE TABLE [SMC] ([Mod] TEXT, [From] TEXT, [To] TEXT, [FromFirst] TEXT, [Extra] TEXT)')
		_SQLite_Exec(-1, 'BEGIN')
		For $i = 1 To $ListContents[0]
			Switch $ListContentsSection
				Case 1
					$ListContentsSplit = StringSplit($ListContents[$i], '|')
					If $ListContentsSplit[0] = 4 Or $ListContentsSplit[0] = 5 Then _SQLite_Exec(-1, 'INSERT INTO [SMC] VALUES ("' & $ListContentsSplit[1] & '","' & _
							(StringLeft($ListContentsSplit[2], 1) = '\' ? '' : '\') & ($ListContentsSplit[3] ? $ListContentsSplit[2] & '\' & $ListContentsSplit[3] : $ListContentsSplit[2]) & '","' & _
							(StringLeft($ListContentsSplit[4], 1) = '\' ? '' : '\') & $ListContentsSplit[4] & '","' & ($ListContentsSplit[3] ? $ListContentsSplit[2] & '\' : $ListContentsSplit[2]) & '","' & ($ListContentsSplit[0] = 5 ? $ListContentsSplit[5] : '') & '");')
				Case 2
					Local $ListOverwriteSize = UBound($ListOverwrite)
					Local $ListOverwriteSplit = StringSplit($ListContents[$i], '|')
					If $ListOverwriteSplit[0] = 3 Then
						ReDim $ListOverwrite[$ListOverwriteSize + 1][5]
						For $i1 = 1 To $ListOverwriteSplit[0]
							$ListOverwrite[$ListOverwriteSize][$i1 - 1] = $ListOverwriteSplit[$i1]
						Next
					EndIf
				Case 3
					Local $ListOptimizeSize = UBound($ListOptimize)
					Local $ListOptimizeSplit = StringSplit($ListContents[$i], '|')
					If $ListOptimizeSplit[0] = 3 Then
						ReDim $ListOptimize[$ListOptimizeSize + 1][3]
						For $i1 = 1 To $ListOptimizeSplit[0]
							$ListOptimize[$ListOptimizeSize][$i1 - 1] = $ListOptimizeSplit[$i1]
						Next
					EndIf
			EndSwitch
			If StringLeft($ListContents[$i], 1) = '[' And StringRight($ListContents[$i], 1) = ']' Then
				$ListContentsSection = 0
				For $i1 = 0 To UBound($SkyrimDLC) - 1
					If ($ListContents[$i] = '[Skyrim]') Or ($ListContents[$i] = '[Skyrim_' & $SkyrimDLC[$i1][0] & ']' And $SkyrimDLC[$i1][2] = 1) Then $ListContentsSection = 1
				Next
				If $ListContents[$i] = '[Skyrim_Overwrite]' Then $ListContentsSection = 2
				If $ListContents[$i] = '[Skyrim_Optimize]' Then $ListContentsSection = 3
			EndIf
		Next
		If _SQLite_Query(-1, 'SELECT DISTINCT [Mod] FROM [SMC]', $Query) <> $SQLITE_OK Then
			GUISetCursor(2, 1, $GUILoading)
			GUISetState(@SW_HIDE, $GUILoading)
			_ExtMsgBox(16, '&' & $_Quit, $_Error, StringUpper(StringLeft($_SQLiteError, 1)) & StringTrimLeft($_SQLiteError, 1) & '=' & _SQLite_ErrCode() & ' (' & _SQLite_ErrMsg() & ')')
			ExitClean(1)
		Else
			While _SQLite_FetchData($Query, $Row) = $SQLITE_OK
				ReDim $ModListArray[UBound($ModListArray) + 1]
				$ModListArray[UBound($ModListArray) - 1] = $Row[0]
				Sleep(10)
			WEnd
			_SQLite_QueryFinalize($Query)
		EndIf
		If _SQLite_Exec(-1, 'COMMIT') <> $SQLITE_OK Then
			LogWrite(' [X] ' & $_SQLiteError & '=' & _SQLite_ErrCode() & ' (' & _SQLite_ErrMsg() & ')' & @CRLF)
			ExitClean(1)
		Else
			LogWrite('[S] ' & $_SQLiteCreated & @CRLF)
		EndIf
		_ArraySort($ModListArray, 0, 0)
		_ArrayInsert($ModListArray, 0, UBound($ModListArray))
	EndFunc   ;==>MainListInit

	Func MainListInitError()
		GUISetCursor(2, 1, $GUILoading)
		GUISetState(@SW_HIDE, $GUILoading)
		Switch _ExtMsgBox(48, $_Quit & '|&' & $_Proceed, $_Warning, $_ListError & ' :(' & @CRLF & @CRLF & $_SelectManually & '...')
			Case 1
				ExitClean(1)
			Case 2
				$ListPath = FileOpenDialog($_ChooseList, @ScriptDir & '\', $_List & ' (*.lst)', 3)
		EndSwitch
		GUISetState(@SW_SHOW, $GUILoading)
		GUISetCursor(1, 1, $GUILoading)
	EndFunc   ;==>MainListInitError

	Func MainListInitExit()
		$ListSelectExit = 1
	EndFunc   ;==>MainListInitExit

	Func MainListInitFirst()
		$ListArray = _FileListToArray(@ScriptDir & '\List', '*.lst', 1)
		If $ListArray[0] > 1 Then
			Local $ListSelectWidth = _StringSize($_ListSelect & ':', 11, Default, Default, 'Calibri')[2]
			Local $ListSelectHeight = $ListArray[0] * 25 + 82
			ReDim $ListRadio[$ListArray[0]]
			For $i = 1 To $ListArray[0]
				Local $ListSelectRadioLen = _StringSize(StringTrimRight($ListArray[$i], 4), 10, Default, Default, 'Calibri')[2] + 30
				If $ListSelectRadioLen > $ListSelectWidth Then $ListSelectWidth = $ListSelectRadioLen
			Next
			$GUIListSelect = GUICreate('SMC ' & $_ListSelection, $ListSelectWidth + 20, $ListSelectHeight, -1, -1, $WS_DLGFRAME)
			GUISetFont(10, Default, Default, 'Calibri', $GUIListSelect, 6)
			Local $ListSelectLabel = GUICtrlCreateLabel($_ListSelect & ':', 5, 5, $ListSelectWidth + 10, 20, $SS_CENTER, 0)
			GUISetFont(11, Default, Default, 'Calibri', $ListSelectLabel, 6)
			For $i = 1 To $ListArray[0]
				$ListRadio[$i - 1] = GUICtrlCreateRadio(StringTrimRight($ListArray[$i], 4), 10, $i * 25, Default, Default)
			Next
			$ListSelectButton = GUICtrlCreateButton($_Proceed, 5, $ListSelectHeight - 50, $ListSelectWidth + 10, 25, $BS_DEFPUSHBUTTON)
			GUICtrlSetOnEvent($ListSelectButton, 'MainListInitSelect')
			GUISetOnEvent($GUI_EVENT_CLOSE, 'GUIClose', $GUIListSelect)
			GUISetCursor(2, 1, $GUILoading)
			GUISetState(@SW_HIDE, $GUILoading)
			GUISetState(@SW_SHOW, $GUIListSelect)
			While 1
				If $ListSelectExit = 1 Then ExitLoop
				Sleep(100)
			WEnd
			GUIDelete($GUIListSelect)
			GUISetState(@SW_SHOW, $GUILoading)
			GUISetCursor(1, 1, $GUILoading)
		EndIf
		If $ListArray[0] = 1 Then $ListPath = @ScriptDir & '\List\' & $ListArray[1]
	EndFunc   ;==>MainListInitFirst

	Func MainListInitSelect()
		For $i = 0 To UBound($ListRadio) - 1
			If GUICtrlRead($ListRadio[$i]) = $GUI_CHECKED Then
				$ListPathFile = GUICtrlRead($ListRadio[$i], 1)
				$ListPath = @ScriptDir & '\List\' & $ListPathFile & '.lst'
				IniWrite($AppConfig, 'System', 'ListPath', $ListPathFile)
			EndIf
		Next
		MainListInitExit()
	EndFunc   ;==>MainListInitSelect

	Func MainLoop($Loop = 1)
		While 1
			If WinExists($GUIMain) Then
				If WinActive($GUIMain) Then
					If _GUICtrlListView_GetItemCount($ListView) > 0 Then
						If $ListViewItemSelected <> _GUICtrlListView_GetSelectedIndices($ListView) Then
							If $ListViewItemSelected <> '' Then _GUICtrlListView_SetItemText($ListView, $ListViewItemSelected, $ModName[$ListViewItemSelected])
							$ListViewItemSelected = _GUICtrlListView_GetSelectedIndices($ListView)
							If $ListViewItemSelected <> '' Then _GUICtrlListView_SetItemText($ListView, $ListViewItemSelected, '> ' & $ModName[$ListViewItemSelected])
							ModLogoSet($ListViewItemSelected)
						EndIf
						$ListViewItemsCounter = 0
						For $i = 0 To $ModArray[0] - 1
							If _GUICtrlListView_GetItemChecked($ListView, $i) Then
								If $ModChecked[$i] <> 1 Then
									If _GUICtrlListView_GetItemText($ListView, $i, 1) Then
										$ModSize[$i] = CheckPath($i)
										$ModChecked[$i] = 1
									Else
										GUICtrlSetState($ListViewItem[$i], $GUI_UNCHECKED)
									EndIf
								EndIf
								If $ModSize[$i] > 0 Then $ListViewItemsCounter += 1
							Else
								$ModChecked[$i] = 0
							EndIf
						Next
						If $ListViewItemsSum <> $ListViewItemsCounter Then
							GUICtrlSetState($MainButton, ($ListViewItemsCounter > 0 ? $GUI_ENABLE : $GUI_DISABLE))
							$ListViewItemsSum = $ListViewItemsCounter
							CheckPathsSize()
						EndIf
					EndIf
				Else
					For $i = 0 To $ModArray[0] - 1
						If WinActive($GUIModEditor[$i]) And $i <> $GUIModEditorLastActive Then
							$GUIModEditorLastActive = $i
							_HotKey_Assign(BitOR($CK_CONTROL, 0x53), 'ModEditorApply', $HK_FLAG_DEFAULT, $GUIModEditor[$i])
							ExitLoop
						EndIf
					Next
					If $GUIModEditorLastActive <> -1 And WinActive($GUIModEditor[$GUIModEditorLastActive]) = 0 Then
						$GUIModEditorLastActive = -1
						_HotKey_Assign(BitOR($CK_CONTROL, 0x53))
					EndIf
				EndIf
			EndIf
			If $Loop = 0 Then ExitLoop
			Sleep(100)
		WEnd
	EndFunc   ;==>MainLoop

	Func MainMenu()
		For $i = 0 To $ModArray[0] - 1
			Switch @GUI_CtrlId
				Case $MenuListPathAll[$i]
					For $i1 = 0 To $ModArray[0] - 1
						If _GUICtrlListView_GetItemText($ListView, $i1, 1) <> _GUICtrlListView_GetItemText($ListView, $i, 1) Then
							_GUICtrlListView_SetItemText($ListView, $i1, _GUICtrlListView_GetItemText($ListView, $i, 1), 1)
							$ModSize[$i1] = CheckPath($i1)
							ListViewItemContextMenuChange($i1, 1)
						EndIf
					Next
				Case $MenuListPathAllUnspec[$i]
					For $i1 = 0 To $ModArray[0] - 1
						If _GUICtrlListView_GetItemText($ListView, $i1, 1) = '' Then
							_GUICtrlListView_SetItemText($ListView, $i1, _GUICtrlListView_GetItemText($ListView, $i, 1), 1)
							$ModSize[$i1] = CheckPath($i1)
							ListViewItemContextMenuChange($i1, 1)
						EndIf
					Next
				Case $MenuListCheckAll[$i]
					For $i1 = 0 To $ModArray[0] - 1
						If GUICtrlRead($ListViewItem[$i1], 1) = $GUI_UNCHECKED And _GUICtrlListView_GetItemText($ListView, $i1, 1) Then GUICtrlSetState($ListViewItem[$i1], $GUI_CHECKED)
					Next
				Case $MenuListUncheckAll[$i]
					For $i1 = 0 To $ModArray[0] - 1
						If GUICtrlRead($ListViewItem[$i1], 1) = $GUI_CHECKED Then GUICtrlSetState($ListViewItem[$i1], $GUI_UNCHECKED)
					Next
				Case $MenuListClearCurrent[$i]
					ListViewItemContextMenuChange($i)
				Case $MenuListClearAllEx[$i]
					For $i1 = 0 To $ModArray[0] - 1
						If $i1 <> $i And _GUICtrlListView_GetItemText($ListView, $i1, 1) Then ListViewItemContextMenuChange($i1)
					Next
				Case $MenuListClearAll[$i]
					For $i1 = 0 To $ModArray[0] - 1
						If _GUICtrlListView_GetItemText($ListView, $i1, 1) Then ListViewItemContextMenuChange($i1)
					Next
				Case $MenuListDownload[$i]
					MainDownload($i)
				Case $MenuListOpen[$i]
					ModPathOpen($i)
				Case $MenuListEditor[$i]
					ModEditorCreate($i)
				Case $MenuListSpecifyPath[$i]
					MainSpecifyPath($i)
			EndSwitch
		Next
	EndFunc   ;==>MainMenu

	Func MainProgressSet($ProgressBarPercent = 0, $ProgressBarText = '', $ProgressBarState = 0)
		Local $hWnd
		If $ProgressBarPercent <> -1 Or $ProgressBarState <> -1 Then
			If $ProgressBarPercent = -1 Then
				$ProgressBarPercent = $ProgressBarLastStoredPercent
			Else
				$ProgressBarLastStoredPercent = $ProgressBarPercent
			EndIf
			If $ProgressBarState = -1 Then
				$ProgressBarState = $ProgressBarLastStoredState
			Else
				$ProgressBarLastStoredState = $ProgressBarState
			EndIf
			If $TaskBarCompat = 1 Then
				If $ProgressBarPercent = 0 Then
					_ITaskBar_SetProgressState($GUIMain)
				Else
					_ITaskBar_SetProgressValue($GUIMain, $ProgressBarPercent)
				EndIf
			EndIf
			If Not IsHWnd($hWnd) Then
				$hWnd = GUICtrlGetHandle($MainProgress)
				If Not $hWnd Then Return 0
			EndIf
			ProgressPaint($hWnd, $ProgressBarPercent, $ProgressBarState)
		EndIf
		If $ProgressBarLastStoredText <> $ProgressBarText Or $ProgressBarPercent <> -1 Or $ProgressBarState <> -1 Then
			If $ProgressBarText = '' Then $ProgressBarText = StringTrimLeft(GUICtrlRead($LabelProgressWhite), 2)
			If $TaskBarCompat = 1 Then _ITaskBar_SetThumbNailToolTip($GUIMain, $ProgressBarText)
			GUICtrlSetData($LabelProgressBlack, StringStripWS('> ' & StringLower(StringLeft($ProgressBarText, 1)) & StringTrimLeft($ProgressBarText, 1), 4))
			GUICtrlSetData($LabelProgressWhite, StringStripWS('> ' & StringLower(StringLeft($ProgressBarText, 1)) & StringTrimLeft($ProgressBarText, 1), 4))
			$ProgressBarLastStoredText = $ProgressBarText
		EndIf
	EndFunc   ;==>MainProgressSet

	Func MainSpecifyPath($i)
		_GUICtrlListView_BeginUpdate($ListView)
		$ModFolder = (($PreferredInputPath And FileExists($PreferredInputPath)) ? $PreferredInputPath : FileSelectFolder($_ChooseModFolder & ':', '', 6, StringStripWS($ModFolder, 3)))
		If $ModFolder And FileExists($ModFolder) Then
			If StringCompare(StringRight($ModFolder, 1), '\') <> 0 Then $ModFolder &= '\'
			_GUICtrlListView_SetItemText($ListView, $i, $ModFolder, 1)
			GUICtrlSetData($MenuListPathAll[$i], $_SpecifyPath & ' "' & _GUICtrlListView_GetItemText($ListView, $i, 1) & '" ' & $_PathAll)
			GUICtrlSetData($MenuListPathAllUnspec[$i], $_SpecifyPath & ' "' & _GUICtrlListView_GetItemText($ListView, $i, 1) & '" ' & $_PathAllUnspec)
			$ModSize[$i] = CheckPath($i)
			ListViewItemContextMenuChange($i, 1)
		EndIf
		_GUICtrlListView_EndUpdate($ListView)
	EndFunc   ;==>MainSpecifyPath

	Func ModEditorApply()
		Local $ModStatusCurrent, $HelpListViewItemFound, $ModEditorTextString = ''
		Local $ModEditorTextStrings = StringSplit(GUICtrlRead($ModEditorEdit[$GUIModEditorLastActive]), @CR)
		For $i = 1 To $ModEditorTextStrings[0]
			If StringStripWS($ModEditorTextStrings[$i], 3) <> '' Then $ModEditorTextString &= StringStripWS($ModEditorTextStrings[$i], 3) & '; '
		Next
		$ModEditorTextString = StringTrimRight($ModEditorTextString, 2)
		Switch $ModEditorText[$GUIModEditorLastActive][1]
			Case 'Status'
				If $ModEditorTextString <> '' Then
					IniWrite($AppConfig, $ModArray[$GUIModEditorLastActive + 1], $ModEditorText[$GUIModEditorLastActive][1], $ModEditorTextString)
				Else
					IniDelete($AppConfig, $ModArray[$GUIModEditorLastActive + 1], $ModEditorText[$GUIModEditorLastActive][1])
				EndIf
				$ModEditorText[$GUIModEditorLastActive][0] = GUICtrlRead($ModEditorEdit[$GUIModEditorLastActive])
				$ModStatus[$GUIModEditorLastActive] = IniRead($AppConfig, $ModArray[$GUIModEditorLastActive + 1], 'Status', '(x)|20120720')
				If StringInStr($ModStatus[$GUIModEditorLastActive], '|', 0, 4) Then $ModStatus[$GUIModEditorLastActive] = StringLeft($ModStatus[$GUIModEditorLastActive], StringInStr($ModStatus[$GUIModEditorLastActive], '|', 0, 4) - 1)
				$ModStatus[$GUIModEditorLastActive] = StringReplace(StringReplace(StringReplace(StringReplace($ModStatus[$GUIModEditorLastActive], '(U)', $_Updated), '(+)', $_Added), '(-)', $_Removed), '(x)', '')
				$ModStatusCurrent = StringSplit($ModStatus[$GUIModEditorLastActive], '|')
				$HelpListViewItemFound = _GUICtrlListView_FindInText($HelpListView, $ModName[$GUIModEditorLastActive])
				If $HelpListViewItemFound <> -1 Then
					Switch $ModStatusCurrent[0]
						Case 0
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, '', 1)
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, '20120720', 2)
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, '', 3)
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, '', 4)
						Case 1
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, $ModStatusCurrent[1], 1)
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, '20120720', 2)
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, '', 3)
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, '', 4)
						Case 2
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, $ModStatusCurrent[1], 1)
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, $ModStatusCurrent[2], 2)
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, '', 3)
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, '', 4)
						Case 3
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, $ModStatusCurrent[1], 1)
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, $ModStatusCurrent[2], 2)
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, $ModStatusCurrent[3], 3)
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, '', 4)
						Case 4
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, $ModStatusCurrent[1], 1)
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, $ModStatusCurrent[2], 2)
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, $ModStatusCurrent[3], 3)
							_GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, $ModStatusCurrent[4], 4)
					EndSwitch
				EndIf
			Case 'Name'
				$HelpListViewItemFound = _GUICtrlListView_FindInText($HelpListView, $ModName[$GUIModEditorLastActive])
				If $ModEditorTextString <> '' Then
					IniWrite($AppConfig, $ModArray[$GUIModEditorLastActive + 1], $ModEditorText[$GUIModEditorLastActive][1], $ModEditorTextString)
				Else
					IniDelete($AppConfig, $ModArray[$GUIModEditorLastActive + 1], $ModEditorText[$GUIModEditorLastActive][1])
				EndIf
				$ModEditorText[$GUIModEditorLastActive][0] = GUICtrlRead($ModEditorEdit[$GUIModEditorLastActive])
				$ModName[$GUIModEditorLastActive] = IniRead($AppConfig, $ModArray[$GUIModEditorLastActive + 1], 'Name', $ModArray[$GUIModEditorLastActive + 1])
				_GUICtrlListView_SetItemText($ListView, $GUIModEditorLastActive, (StringLeft(_GUICtrlListView_GetItemText($ListView, $GUIModEditorLastActive), 2) = '> ' ? '> ' & $ModName[$GUIModEditorLastActive] : $ModName[$GUIModEditorLastActive]))
				If $HelpListViewItemFound <> -1 Then _GUICtrlListView_SetItemText($HelpListView, $HelpListViewItemFound, $ModName[$GUIModEditorLastActive])
			Case 'Directory'
				If $ModEditorTextString <> '' Then
					IniRenameSection($AppConfig, $ModArray[$GUIModEditorLastActive + 1], $ModEditorTextString, 1)
					IniWrite($AppConfig, 'Checksums', 'Pics\' & $ModEditorTextString & '.jpg', IniRead($AppConfig, 'Checksums', 'Pics\' & $ModArray[$GUIModEditorLastActive + 1] & '.jpg', ''))
					IniDelete($AppConfig, 'Checksums', 'Pics\' & $ModArray[$GUIModEditorLastActive + 1] & '.jpg')
					FileMove('Pics\' & $ModArray[$GUIModEditorLastActive + 1] & '.jpg', 'Pics\' & $ModEditorTextString & '.jpg', 9)
					$ModEditorText[$GUIModEditorLastActive][0] = GUICtrlRead($ModEditorEdit[$GUIModEditorLastActive])
					$ModArray[$GUIModEditorLastActive + 1] = $ModEditorTextString
					$ModName[$GUIModEditorLastActive] = IniRead($AppConfig, $ModArray[$GUIModEditorLastActive + 1], 'Name', $ModArray[$GUIModEditorLastActive + 1])
					_GUICtrlListView_SetItemText($ListView, $GUIModEditorLastActive, (StringLeft(_GUICtrlListView_GetItemText($ListView, $GUIModEditorLastActive), 2) = '> ' ? '> ' & $ModName[$GUIModEditorLastActive] : $ModName[$GUIModEditorLastActive]))
					WinSetTitle($GUIModEditor[$GUIModEditorLastActive], '', $_Editing & ': ' & $ModArray[$GUIModEditorLastActive + 1])
				EndIf
			Case 'Compatibility Patches'
				Local $CPTextString = StringReplace(StringReplace(StringReplace(StringReplace($ModEditorTextString, ':; > ', '='), '; ', @CRLF), @CRLF & '> ', '; '), @CRLF & '>', '; ')
				Local $CPTextArray = StringSplit($CPTextString, @CRLF)
				Local $CPData = ''
				Local $CPSection = IniReadSection($AppConfig, 'Compatibility Patches')
				If Not @error And $CPSection[0][0] > 0 Then
					For $i = 1 To $CPSection[0][0]
						If StringInStr($CPSection[$i][0], $ModArray[$GUIModEditorLastActive + 1]) Then IniDelete($AppConfig, 'Compatibility Patches', $CPSection[$i][0])
					Next
				EndIf
				If $ModEditorTextString <> '' Then
					For $i = 1 To $CPTextArray[0]
						$CPTextArrayPart = StringSplit($CPTextArray[$i], '=')
						If $CPTextArrayPart[0] = 2 Then IniWrite($AppConfig, 'Compatibility Patches', $CPTextArrayPart[1], $CPTextArrayPart[2])
					Next
				EndIf
				$CPSection = IniReadSection($AppConfig, 'Compatibility Patches')
				If Not @error And $CPSection[0][0] > 0 Then
					For $i = 1 To $CPSection[0][0]
						If StringInStr($CPSection[$i][0], $ModArray[$GUIModEditorLastActive + 1]) Then $CPData &= $CPSection[$i][0] & ':' & @CRLF & '> ' & StringReplace($CPSection[$i][1], '; ', @CRLF & '> ') & @CRLF & @CRLF
					Next
				EndIf
				GUICtrlSetData($ModEditorEdit[$GUIModEditorLastActive], StringReplace($CPData, @CRLF & @CRLF, '', -1))
				$ModEditorText[$GUIModEditorLastActive][0] = GUICtrlRead($ModEditorEdit[$GUIModEditorLastActive])
			Case Else
				If $ModEditorTextString <> '' Then
					IniWrite($AppConfig, $ModArray[$GUIModEditorLastActive + 1], $ModEditorText[$GUIModEditorLastActive][1], $ModEditorTextString)
				Else
					IniDelete($AppConfig, $ModArray[$GUIModEditorLastActive + 1], $ModEditorText[$GUIModEditorLastActive][1])
				EndIf
				$ModEditorText[$GUIModEditorLastActive][0] = GUICtrlRead($ModEditorEdit[$GUIModEditorLastActive])
				$ModLink[$GUIModEditorLastActive] = IniRead($AppConfig, $ModArray[$GUIModEditorLastActive + 1], 'Link', '')
				$ModAuthor[$GUIModEditorLastActive] = IniRead($AppConfig, $ModArray[$GUIModEditorLastActive + 1], 'Author', '')
				$ModLogo[$GUIModEditorLastActive] = IniRead($AppConfig, $ModArray[$GUIModEditorLastActive + 1], 'Logo', $LinkPics)
				$ModSize[$GUIModEditorLastActive] = CheckPath($GUIModEditorLastActive)
				CheckPathsSize()
		EndSwitch
		GUICtrlSetState($ModEditorButton[$GUIModEditorLastActive], $GUI_DISABLE)
		ModEditorPaint($GUIModEditorLastActive)
	EndFunc   ;==>ModEditorApply

	Func ModEditorCreate($i)
		If FileExists($AppConfig) Then
			If BitAND(WinGetState($GUIModEditor[$i]), 1) Then
				If BitAND(WinGetState($GUIModEditor[$i]), 2) Then
					WinActivate($GUIModEditor[$i])
				Else
					GUISetState(@SW_SHOW, $GUIModEditor[$i])
				EndIf
			Else
				Local $W = 860, $H = 300, $w2 = 150
				$GUIModEditor[$i] = GUICreate($_Editing & ': ' & $ModArray[$i + 1], $W, $H, $GUIMainPosition[0] + $GUIMainSize[0] / 2 - $W / 2, $GUIMainPosition[1] + $GUIMainSize[1] / 2 - $H / 2)
				GUISetFont(9, Default, Default, 'Calibri', $GUIModEditor[$i], 6)
				GUISetOnEvent($GUI_EVENT_CLOSE, 'GUIClose', $GUIModEditor[$i])
				$ModEditorTreeView[$i] = GUICtrlCreateTreeView(0, -2, $w2, $H - $yh + 1, BitOR($TVS_HASBUTTONS, $TVS_HASLINES, $TVS_LINESATROOT, $TVS_DISABLEDRAGDROP, $TVS_SHOWSELALWAYS, $TVS_TRACKSELECT), $WS_EX_CLIENTEDGE)
				$ModEditorTreeViewItem[$i][1] = GUICtrlCreateTreeViewItem(StringUpper(StringLeft($_StatusMsg, 1)) & StringTrimLeft($_StatusMsg, 1), $ModEditorTreeView[$i])
				GUICtrlSetOnEvent(-1, 'ModEditorItem')
				$ModEditorTreeViewItem[$i][2] = GUICtrlCreateTreeViewItem($_Name, $ModEditorTreeView[$i])
				GUICtrlSetOnEvent(-1, 'ModEditorItem')
				$ModEditorTreeViewItem[$i][3] = GUICtrlCreateTreeViewItem($_Author, $ModEditorTreeView[$i])
				GUICtrlSetOnEvent(-1, 'ModEditorItem')
				$ModEditorTreeViewItem[$i][4] = GUICtrlCreateTreeViewItem($_Link, $ModEditorTreeView[$i])
				GUICtrlSetOnEvent(-1, 'ModEditorItem')
				$ModEditorTreeViewItem[$i][5] = GUICtrlCreateTreeViewItem($_Logo, $ModEditorTreeView[$i])
				GUICtrlSetOnEvent(-1, 'ModEditorItem')
				$ModEditorTreeViewItem[$i][6] = GUICtrlCreateTreeViewItem($_Properties, $ModEditorTreeView[$i])
				GUICtrlSetOnEvent(-1, 'ModEditorItem')
				For $i1 = 0 To 3
					$ModEditorTreeViewItem[$i][7 + 2 * $i1] = GUICtrlCreateTreeViewItem($QualityArray[$i1] & ': ' & $_Archives, $ModEditorTreeViewItem[$i][6])
					GUICtrlSetOnEvent(-1, 'ModEditorItem')
					$ModEditorTreeViewItem[$i][8 + 2 * $i1] = GUICtrlCreateTreeViewItem($QualityArray[$i1] & ': ' & $_Size, $ModEditorTreeViewItem[$i][6])
					GUICtrlSetOnEvent(-1, 'ModEditorItem')
				Next
				$ModEditorTreeViewItem[$i][15] = GUICtrlCreateTreeViewItem($_Unsupported, $ModEditorTreeView[$i])
				GUICtrlSetOnEvent(-1, 'ModEditorItem')
				$ModEditorTreeViewItem[$i][16] = GUICtrlCreateTreeViewItem($_Directory, $ModEditorTreeView[$i])
				GUICtrlSetOnEvent(-1, 'ModEditorItem')
				$ModEditorTreeViewItem[$i][17] = GUICtrlCreateTreeViewItem($_CompatibilityPatches, $ModEditorTreeView[$i])
				GUICtrlSetOnEvent(-1, 'ModEditorItem')
				$ModEditorButton[$i] = GUICtrlCreateButton($_ApplyChanges, 0, $H - $yh, $w2 + 1, $yh, BitOR($WS_TABSTOP, $BS_CENTER, $BS_VCENTER))
				GUICtrlSetOnEvent(-1, 'ModEditorApply')
				GUICtrlSetState(-1, $GUI_DISABLE)
				$ModEditorEdit[$i] = GUICtrlCreateEdit('', $w2 + 4, 2, $W - $w2 - 10, $H - 4, BitOR($GUI_SS_DEFAULT_EDIT, $ES_NOHIDESEL, $ES_WANTRETURN), 0)
				GUICtrlSetFont(-1, 9, Default, Default, 'Consolas', 6)
				GUICtrlSetBkColor($ModEditorEdit[$i], GetSysColor(15))
				GUICtrlSetState($ModEditorTreeViewItem[$i][6], $GUI_EXPAND)
				ModEditorPaint($i)
				GUISetState(@SW_SHOW)
				_GUICtrlTreeView_SelectItem($ModEditorTreeView[$i], $ModEditorTreeViewItem[$i][0], $TVGN_CARET)
				_GUICtrlTreeView_SelectItem($ModEditorTreeView[$i], $ModEditorTreeViewItem[$i][2], $TVGN_CARET)
			EndIf
		EndIf
	EndFunc   ;==>ModEditorCreate

	Func ModEditorItem()
		For $i = 0 To $ModArray[0] - 1
			If $GUIModEditor[$i] = @GUI_WinHandle Then
				ModEditorSwitch($i, 0)
				Switch @GUI_CtrlId
					Case $ModEditorTreeViewItem[$i][1]
						GUICtrlSetData($ModEditorEdit[$i], StringReplace(IniRead($AppConfig, $ModArray[$i + 1], 'Status', ''), '; ', @CRLF))
						ModEditorSwitch($i, 1)
						$ModEditorText[$i][1] = 'Status'
					Case $ModEditorTreeViewItem[$i][2]
						GUICtrlSetData($ModEditorEdit[$i], StringReplace(IniRead($AppConfig, $ModArray[$i + 1], 'Name', ''), '; ', @CRLF))
						ModEditorSwitch($i, 1)
						$ModEditorText[$i][1] = 'Name'
					Case $ModEditorTreeViewItem[$i][3]
						GUICtrlSetData($ModEditorEdit[$i], StringReplace(IniRead($AppConfig, $ModArray[$i + 1], 'Author', ''), '; ', @CRLF))
						ModEditorSwitch($i, 1)
						$ModEditorText[$i][1] = 'Author'
					Case $ModEditorTreeViewItem[$i][4]
						GUICtrlSetData($ModEditorEdit[$i], StringReplace(IniRead($AppConfig, $ModArray[$i + 1], 'Link', ''), '; ', @CRLF))
						ModEditorSwitch($i, 1)
						$ModEditorText[$i][1] = 'Link'
					Case $ModEditorTreeViewItem[$i][5]
						GUICtrlSetData($ModEditorEdit[$i], StringReplace(IniRead($AppConfig, $ModArray[$i + 1], 'Logo', ''), '; ', @CRLF))
						ModEditorSwitch($i, 1)
						$ModEditorText[$i][1] = 'Logo'
					Case $ModEditorTreeViewItem[$i][6]
						GUICtrlSetData($ModEditorEdit[$i], $_PropertiesDesc)
						ModEditorSwitch($i, -1)
					Case $ModEditorTreeViewItem[$i][7]
						GUICtrlSetData($ModEditorEdit[$i], StringReplace(IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[0] & 'Archives', ''), '; ', @CRLF))
						ModEditorSwitch($i, 1)
						$ModEditorText[$i][1] = $QualityArray[0] & 'Archives'
					Case $ModEditorTreeViewItem[$i][8]
						$ModEditorText[$i][2] = StringReplace(IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[0] & 'Size', ''), '; ', @CRLF)
						GUICtrlSetData($ModEditorEdit[$i], $ModEditorText[$i][2])
						ModEditorSwitch($i, 1)
						$ModEditorText[$i][1] = $QualityArray[0] & 'Size'
					Case $ModEditorTreeViewItem[$i][9]
						GUICtrlSetData($ModEditorEdit[$i], StringReplace(IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[1] & 'Archives', ''), '; ', @CRLF))
						ModEditorSwitch($i, 1)
						$ModEditorText[$i][1] = $QualityArray[1] & 'Archives'
					Case $ModEditorTreeViewItem[$i][10]
						$ModEditorText[$i][2] = StringReplace(IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[1] & 'Size', ''), '; ', @CRLF)
						GUICtrlSetData($ModEditorEdit[$i], $ModEditorText[$i][2])
						ModEditorSwitch($i, 1)
						$ModEditorText[$i][1] = $QualityArray[1] & 'Size'
					Case $ModEditorTreeViewItem[$i][11]
						GUICtrlSetData($ModEditorEdit[$i], StringReplace(IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[2] & 'Archives', ''), '; ', @CRLF))
						ModEditorSwitch($i, 1)
						$ModEditorText[$i][1] = $QualityArray[2] & 'Archives'
					Case $ModEditorTreeViewItem[$i][12]
						$ModEditorText[$i][2] = StringReplace(IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[2] & 'Size', ''), '; ', @CRLF)
						GUICtrlSetData($ModEditorEdit[$i], $ModEditorText[$i][2])
						ModEditorSwitch($i, 1)
						$ModEditorText[$i][1] = $QualityArray[2] & 'Size'
					Case $ModEditorTreeViewItem[$i][13]
						GUICtrlSetData($ModEditorEdit[$i], StringReplace(IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[3] & 'Archives', ''), '; ', @CRLF))
						ModEditorSwitch($i, 1)
						$ModEditorText[$i][1] = $QualityArray[3] & 'Archives'
					Case $ModEditorTreeViewItem[$i][14]
						$ModEditorText[$i][2] = StringReplace(IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[3] & 'Size', ''), '; ', @CRLF)
						GUICtrlSetData($ModEditorEdit[$i], $ModEditorText[$i][2])
						ModEditorSwitch($i, 1)
						$ModEditorText[$i][1] = $QualityArray[3] & 'Size'
					Case $ModEditorTreeViewItem[$i][15]
						GUICtrlSetData($ModEditorEdit[$i], StringReplace(IniRead($AppConfig, $ModArray[$i + 1], 'Unsupported', ''), '; ', @CRLF))
						ModEditorSwitch($i, 1)
						$ModEditorText[$i][1] = 'Unsupported'
					Case $ModEditorTreeViewItem[$i][16]
						GUICtrlSetData($ModEditorEdit[$i], $ModArray[$i + 1])
						ModEditorSwitch($i, 1)
						$ModEditorText[$i][1] = 'Directory'
					Case $ModEditorTreeViewItem[$i][17]
						Local $CPData = ''
						Local $CPSection = IniReadSection($AppConfig, 'Compatibility Patches')
						If Not @error And $CPSection[0][0] > 0 Then
							For $i1 = 1 To $CPSection[0][0]
								If StringInStr($CPSection[$i1][0], $ModArray[$i + 1]) Then $CPData &= $CPSection[$i1][0] & ':' & @CRLF & '> ' & StringReplace($CPSection[$i1][1], '; ', @CRLF & '> ') & @CRLF & @CRLF
							Next
						EndIf
						GUICtrlSetData($ModEditorEdit[$i], StringReplace($CPData, @CRLF & @CRLF, '', -1))
						ModEditorSwitch($i, 1)
						$ModEditorText[$i][1] = 'Compatibility Patches'
				EndSwitch
				$ModEditorText[$i][0] = GUICtrlRead($ModEditorEdit[$i])
			EndIf
		Next
	EndFunc   ;==>ModEditorItem

	Func ModEditorPaint($i)
		Local $Empty = 8
		GUICtrlSetColor($ModEditorTreeViewItem[$i][1], (IniRead($AppConfig, $ModArray[$i + 1], 'Status', '') = '' ? 0xcd0101 : GetSysColor(18)))
		GUICtrlSetColor($ModEditorTreeViewItem[$i][2], (IniRead($AppConfig, $ModArray[$i + 1], 'Name', '') = '' ? 0xcd0101 : GetSysColor(18)))
		GUICtrlSetColor($ModEditorTreeViewItem[$i][3], (IniRead($AppConfig, $ModArray[$i + 1], 'Author', '') = '' ? 0xcd0101 : GetSysColor(18)))
		GUICtrlSetColor($ModEditorTreeViewItem[$i][4], (IniRead($AppConfig, $ModArray[$i + 1], 'Link', '') = '' ? 0xcd0101 : GetSysColor(18)))
		GUICtrlSetColor($ModEditorTreeViewItem[$i][5], (IniRead($AppConfig, $ModArray[$i + 1], 'Logo', '') = '' ? 0xcd0101 : GetSysColor(18)))
		If IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[0] & 'Archives', '') = '' Then
			GUICtrlSetColor($ModEditorTreeViewItem[$i][7], 0xcd0101)
			$Empty -= 1
		Else
			GUICtrlSetColor($ModEditorTreeViewItem[$i][7], GetSysColor(18))
		EndIf
		If IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[0] & 'Size', '') = '' Then
			GUICtrlSetColor($ModEditorTreeViewItem[$i][8], 0xcd0101)
			$Empty -= 1
		Else
			GUICtrlSetColor($ModEditorTreeViewItem[$i][8], GetSysColor(18))
		EndIf
		If IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[1] & 'Archives', '') = '' Then
			GUICtrlSetColor($ModEditorTreeViewItem[$i][9], 0xcd0101)
			$Empty -= 1
		Else
			GUICtrlSetColor($ModEditorTreeViewItem[$i][9], GetSysColor(18))
		EndIf
		If IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[1] & 'Size', '') = '' Then
			GUICtrlSetColor($ModEditorTreeViewItem[$i][10], 0xcd0101)
			$Empty -= 1
		Else
			GUICtrlSetColor($ModEditorTreeViewItem[$i][10], GetSysColor(18))
		EndIf
		If IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[2] & 'Archives', '') = '' Then
			GUICtrlSetColor($ModEditorTreeViewItem[$i][11], 0xcd0101)
			$Empty -= 1
		Else
			GUICtrlSetColor($ModEditorTreeViewItem[$i][11], GetSysColor(18))
		EndIf
		If IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[2] & 'Size', '') = '' Then
			GUICtrlSetColor($ModEditorTreeViewItem[$i][12], 0xcd0101)
			$Empty -= 1
		Else
			GUICtrlSetColor($ModEditorTreeViewItem[$i][12], GetSysColor(18))
		EndIf
		If IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[3] & 'Archives', '') = '' Then
			GUICtrlSetColor($ModEditorTreeViewItem[$i][13], 0xcd0101)
			$Empty -= 1
		Else
			GUICtrlSetColor($ModEditorTreeViewItem[$i][13], GetSysColor(18))
		EndIf
		If IniRead($AppConfig, $ModArray[$i + 1], $QualityArray[3] & 'Size', '') = '' Then
			GUICtrlSetColor($ModEditorTreeViewItem[$i][14], 0xcd0101)
			$Empty -= 1
		Else
			GUICtrlSetColor($ModEditorTreeViewItem[$i][14], GetSysColor(18))
		EndIf
		GUICtrlSetColor($ModEditorTreeViewItem[$i][15], (IniRead($AppConfig, $ModArray[$i + 1], 'Unsupported', '') = '' ? 0xcd0101 : GetSysColor(18)))
		GUICtrlSetColor($ModEditorTreeViewItem[$i][16], ($ModArray[$i + 1] = '' ? 0xcd0101 : GetSysColor(16)))
		Local $CPData = 0
		Local $CPSection = IniReadSection($AppConfig, 'Compatibility Patches')
		If Not @error And $CPSection[0][0] > 0 Then
			For $i1 = 1 To $CPSection[0][0]
				If StringInStr($CPSection[$i1][0], $ModArray[$i + 1]) Then
					$CPData = 1
					ExitLoop
				EndIf
			Next
		EndIf
		GUICtrlSetColor($ModEditorTreeViewItem[$i][17], ($CPData = 1 ? GetSysColor(18) : 0xcd0101))
		GUICtrlSetColor($ModEditorTreeViewItem[$i][6], ($Empty = 0 ? 0xcd0101 : GetSysColor(16)))
	EndFunc   ;==>ModEditorPaint

	Func ModEditorSwitch($i, $ModEditorSwitchState)
		Switch $ModEditorSwitchState
			Case 0
				If BitAND(GUICtrlGetState($ModEditorButton[$i]), $GUI_ENABLE) Then
					GUICtrlSetState($ModEditorTreeView[$i], $GUI_DISABLE)
					GUICtrlSetState($ModEditorButton[$i], $GUI_DISABLE)
					For $i1 = 0 To $ModArray[0] - 1
						If BitAND(WinGetState($GUIModEditor[$i1]), 2) And $GUIModEditor[$i1] <> @GUI_WinHandle Then GUISetState(@SW_DISABLE, $GUIModEditor[$i1])
					Next
					If _ExtMsgBox(48, $_Apply & '|&' & $_Skip, $_Warning, $_ApplyModChanges & '?') = 1 Then ModEditorApply()
					For $i1 = 0 To $ModArray[0] - 1
						If BitAND(WinGetState($GUIModEditor[$i1]), 2) And $GUIModEditor[$i1] <> @GUI_WinHandle Then GUISetState(@SW_ENABLE, $GUIModEditor[$i1])
					Next
					GUICtrlSetState($ModEditorTreeView[$i], $GUI_ENABLE)
				EndIf
				GUICtrlSetState($ModEditorButton[$i], $GUI_DISABLE)
				GUICtrlSetStyle($ModEditorEdit[$i], BitOR($GUI_SS_DEFAULT_EDIT, $ES_NOHIDESEL, $ES_WANTRETURN, $ES_READONLY))
			Case 1
				GUICtrlSetState($ModEditorEdit[$i], $GUI_ENABLE)
				GUICtrlSetStyle($ModEditorEdit[$i], BitOR($GUI_SS_DEFAULT_EDIT, $ES_NOHIDESEL, $ES_WANTRETURN))
			Case -1
				GUICtrlSetState($ModEditorEdit[$i], $GUI_ENABLE)
				GUICtrlSetStyle($ModEditorEdit[$i], BitOR($GUI_SS_DEFAULT_EDIT, $ES_NOHIDESEL, $ES_WANTRETURN, $ES_READONLY))
		EndSwitch
	EndFunc   ;==>ModEditorSwitch

	Func ModListInit()
		Local $ModArraySectionNames = IniReadSectionNames($AppConfig)
		_ArraySort($ModArraySectionNames, 0, 1)
		$ModArraySectionNames[0] = UBound($ModArraySectionNames) - 1
		If Not @error Then
			For $i = 1 To $ModArraySectionNames[0]
				If StringLeft(IniRead($AppConfig, $ModArraySectionNames[$i], 'Status', ''), 3) = '(-)' Then
					If IniRead($AppConfig, $ModArraySectionNames[$i], 'Status', '') Then
						$ModRemovedArray[0] += 1
						ReDim $ModRemovedArray[$ModRemovedArray[0] + 1]
						ReDim $ModRemovedStatus[$ModRemovedArray[0] + 1]
						ReDim $ModRemovedName[$ModRemovedArray[0] + 1]
						ReDim $HelpListViewItem[$ModArray[0] + $ModRemovedArray[0] + 1]
						$ModRemovedArray[$ModRemovedArray[0]] = $ModArraySectionNames[$i]
						$ModRemovedStatus[$ModRemovedArray[0] - 1] = IniRead($AppConfig, $ModRemovedArray[$ModRemovedArray[0]], 'Status', '(x)|20120720')
						If StringInStr($ModRemovedStatus[$ModRemovedArray[0] - 1], '|', 0, 4) Then $ModRemovedStatus[$ModRemovedArray[0] - 1] = StringLeft($ModRemovedStatus[$ModRemovedArray[0] - 1], StringInStr($ModRemovedStatus[$ModRemovedArray[0] - 1], '|', 0, 4) - 1)
						$ModRemovedStatus[$ModRemovedArray[0] - 1] = StringReplace($ModRemovedStatus[$ModRemovedArray[0] - 1], '(-)', $_Removed)
						$ModRemovedName[$ModRemovedArray[0] - 1] = IniRead($AppConfig, $ModRemovedArray[$ModRemovedArray[0]], 'Name', $ModRemovedArray[$ModRemovedArray[0]])
						$ModRemovedNameLongest = ($ModRemovedNameLongest > _StringSize($ModRemovedName[$ModRemovedArray[0] - 1], 9, Default, Default, 'Calibri')[2] ? $ModRemovedNameLongest : _StringSize($ModRemovedName[$ModRemovedArray[0] - 1], 9, Default, Default, 'Calibri')[2])
					EndIf
				Else
					Local $ModArraySection = IniReadSection($AppConfig, $ModArraySectionNames[$i])
					If Not @error Then
						For $i1 = 1 To $ModArraySection[0][0]
							If StringInStr('Status Name Author Link Logo ' & $QualityArray[0] & 'Archives ' & $QualityArray[1] & 'Archives ' & $QualityArray[2] & 'Archives ' & $QualityArray[3] & 'Archives ' & _
									$QualityArray[0] & 'Size ' & $QualityArray[1] & 'Size ' & $QualityArray[2] & 'Size ' & $QualityArray[3] & 'Size Unsupported', $ModArraySection[$i1][0]) Then
								For $i2 = 1 To $ModListArray[0]
									If $ModListArray[$i2] = $ModArraySectionNames[$i] Then
										$ModESPFound = 0
										$ModScriptsFound = 0
										$ModArray[0] += 1
										ReDim $ModArray[$ModArray[0] + 1]
										ReDim $ListViewItem[$ModArray[0] + 1]
										ReDim $ListViewItemContextMenu[$ModArray[0] + 1]
										ReDim $HelpListViewItem[$ModArray[0] + $ModRemovedArray[0] + 1]
										ReDim $GUIModEditor[$ModArray[0] + 1]
										ReDim $ModEditorButton[$ModArray[0] + 1]
										ReDim $ModEditorEdit[$ModArray[0] + 1]
										ReDim $ModEditorText[$ModArray[0] + 1][3]
										ReDim $ModEditorTreeView[$ModArray[0] + 1]
										ReDim $ModEditorTreeViewItem[$ModArray[0] + 1][18]
										ReDim $ModStatus[$ModArray[0] + 1]
										ReDim $ModName[$ModArray[0] + 1]
										ReDim $ModLink[$ModArray[0] + 1]
										ReDim $ModAuthor[$ModArray[0] + 1]
										ReDim $ModLogo[$ModArray[0] + 1]
										ReDim $ModSize[$ModArray[0] + 1]
										ReDim $ModESP[$ModArray[0] + 1]
										ReDim $ModScripts[$ModArray[0] + 1]
										ReDim $ModState[$ModArray[0] + 1]
										ReDim $ModChecked[$ModArray[0] + 1]
										ReDim $ModToolTip[$ModArray[0] + 1]
										ReDim $ModQuality[$ModArray[0] + 1]
										ReDim $MenuListPathAll[$ModArray[0] + 1]
										ReDim $MenuListPathAllUnspec[$ModArray[0] + 1]
										ReDim $MenuListCheckAll[$ModArray[0] + 1]
										ReDim $MenuListUncheckAll[$ModArray[0] + 1]
										ReDim $MenuListClearCurrent[$ModArray[0] + 1]
										ReDim $MenuListClearAllEx[$ModArray[0] + 1]
										ReDim $MenuListClearAll[$ModArray[0] + 1]
										ReDim $MenuListSpecifyPath[$ModArray[0] + 1]
										ReDim $MenuListEditor[$ModArray[0] + 1]
										ReDim $MenuListDownload[$ModArray[0] + 1]
										ReDim $MenuListOpen[$ModArray[0] + 1]
										$ModArray[$ModArray[0]] = $ModArraySectionNames[$i]
										$GUIModEditor[$ModArray[0] - 1] = 0
										$ModStatus[$ModArray[0] - 1] = IniRead($AppConfig, $ModArray[$ModArray[0]], 'Status', '(x)|20120720')
										If StringInStr($ModStatus[$ModArray[0] - 1], '|', 0, 4) Then $ModStatus[$ModArray[0] - 1] = StringLeft($ModStatus[$ModArray[0] - 1], StringInStr($ModStatus[$ModArray[0] - 1], '|', 0, 4) - 1)
										$ModStatus[$ModArray[0] - 1] = StringReplace(StringReplace(StringReplace($ModStatus[$ModArray[0] - 1], '(U)', $_Updated), '(+)', $_Added), '(x)', '')
										$ModName[$ModArray[0] - 1] = IniRead($AppConfig, $ModArray[$ModArray[0]], 'Name', $ModArray[$ModArray[0]])
										$ModNameLongest = ($ModNameLongest > _StringSize($ModName[$ModArray[0] - 1], 9, Default, Default, 'Calibri')[2] ? $ModNameLongest : _StringSize($ModName[$ModArray[0] - 1], 9, Default, Default, 'Calibri')[2])
										$ModAuthor[$ModArray[0] - 1] = IniRead($AppConfig, $ModArray[$ModArray[0]], 'Author', '')
										$ModLink[$ModArray[0] - 1] = IniRead($AppConfig, $ModArray[$ModArray[0]], 'Link', '')
										$ModLogo[$ModArray[0] - 1] = IniRead($AppConfig, $ModArray[$ModArray[0]], 'Logo', $LinkPics)
										If UBound($ListOverwrite) > 0 Then
											For $i3 = 0 To UBound($ListOverwrite) - 1
												If $ModListArray[$i2] = $ListOverwrite[$i3][0] Then
													If StringRight($ListOverwrite[$i3][2], 4) = '.esp' Or StringRight($ListOverwrite[$i3][2], 4) = '.esm' Then
														$ModESPFound = 1
														ExitLoop
													EndIf
												EndIf
											Next
										EndIf
										If $ModESPFound = 0 Then
											If _SQLite_Query(-1, 'SELECT [From] FROM [SMC] WHERE [Mod]="' & $ModListArray[$i2] & '"', $Query) <> $SQLITE_OK Then
												LogWrite(' [X] ' & $_SQLiteError & '=' & _SQLite_ErrCode() & ' (' & _SQLite_ErrMsg() & ')' & @CRLF)
												$OverallErrors += 1
											EndIf
											While _SQLite_FetchData($Query, $Row) = $SQLITE_OK
												If StringRight($Row[0], 4) = '.esp' Or StringRight($Row[0], 4) = '.esm' Then
													$ModESPFound = 1
													ExitLoop
												EndIf
											WEnd
											_SQLite_QueryFinalize($Query)
										EndIf
										If _SQLite_Query(-1, 'SELECT [To] FROM [SMC] WHERE [Mod]="' & $ModListArray[$i2] & '"', $Query) <> $SQLITE_OK Then
											LogWrite(' [X] ' & $_SQLiteError & '=' & _SQLite_ErrCode() & ' (' & _SQLite_ErrMsg() & ')' & @CRLF)
											$OverallErrors += 1
										EndIf
										While _SQLite_FetchData($Query, $Row) = $SQLITE_OK
											If StringLeft($Row[0], 8) = '\scripts' Then
												$ModScriptsFound = 1
												ExitLoop
											EndIf
										WEnd
										_SQLite_QueryFinalize($Query)
										$ModESP[$ModArray[0] - 1] = $ModESPFound
										$ModScripts[$ModArray[0] - 1] = $ModScriptsFound
										ExitLoop 2
									EndIf
								Next
							EndIf
						Next
					EndIf
				EndIf
			Next
			$xw = $ModNameLongest + 10 + 3 * 5.5 + 10
		Else
			_ExtMsgBox(16, '&' & $_Quit, $_Error, $_iniModsError & ' :(')
			ExitClean()
		EndIf
	EndFunc   ;==>ModListInit

	Func ModLogoCacheUpdate()
		If @Compiled And $NoAutoUpdate <> 1 Then
			GUISetCursor(15)
			GUISetState(@SW_LOCK)
			Local $ModLogoCacheUpdateFound = 0
			$UpdateCancel = 0
			GUICtrlSetData($MainButton, $_CancelButton)
			GUICtrlSetState($MainButton, $GUI_ENABLE)
			GUICtrlSetState($ListView, $GUI_DISABLE)
			GUICtrlSetState($MenuMainAppPageOpen, $GUI_DISABLE)
			GUICtrlSetState($MenuMainModPageOpen, $GUI_DISABLE)
			GUICtrlSetState($MenuMainSettings, $GUI_DISABLE)
			GUICtrlSetState($MenuMainHelp, $GUI_DISABLE)
			GUICtrlSetTip($MainButton, $_ButtonCancelTip)
			GUICtrlSetOnEvent($MainButton, 'UpdateCancel')
			GUISetState(@SW_UNLOCK)
			If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 8)
			LogWrite('[G] ' & $_ModLogoCacheUpdate & @CRLF)
			If FileExists(@ScriptDir & '\Pics\') = 0 Then DirCreate(@ScriptDir & '\Pics\')
			For $i = 0 To $ModArray[0] - 1
				If $UpdateCancel = 1 Then ExitLoop
				If FileExists(@ScriptDir & '\Pics\' & $ModArray[$i + 1] & '.jpg') = 0 Or FileGetSize(@ScriptDir & '\Pics\' & $ModArray[$i + 1] & '.jpg') <> IniRead($AppConfig, 'Checksums', 'Pics\' & $ModArray[$i + 1] & '.jpg', '') And $ModLogo[$i] Then
					$ModLogoCacheUpdateFound += 1
					Select
						Case StringLeft(StringRight($ModLogo[$i], 4), 1) = '.'
							$ModLogoCurrentLink = $ModLogo[$i]
						Case StringRight($ModLogo[$i], 1) = '/'
							$ModLogoCurrentLink = $ModLogo[$i] & $ModArray[$i + 1] & '.jpg'
						Case Else
							$ModLogoCurrentLink = $ModLogo[$i] & '/' & $ModArray[$i + 1] & '.jpg'
					EndSelect
					If $ModLogoCacheUpdateFound = 1 Then LogWrite(' [+]' & @CRLF)
					$DownloadHandle = InetGet($ModLogoCurrentLink, @ScriptDir & '\Pics\' & $ModArray[$i + 1] & '.jpg', 16)
					InetClose($DownloadHandle)
					LogWrite('  |- ' & @ScriptDir & '\Pics\' & $ModArray[$i + 1] & '.jpg' & @CRLF)
				EndIf
				MainProgressSet(Round($i * 100 / ($ModArray[0] - 1)), $_StatusMsg & ': ' & $_ModLogoCacheUpdate, 2)
			Next
			If $ModLogoCacheUpdateFound > 0 Then LogWrite(' ' & ($UpdateCancel = 1 ? '[X] ' & $_UpdateCancelled : '[-]') & @CRLF)
			GUISetState(@SW_LOCK)
			GUICtrlSetState($ListView, $GUI_ENABLE)
			GUICtrlSetState($MenuMainAppPageOpen, $GUI_ENABLE)
			GUICtrlSetState($MenuMainModPageOpen, $GUI_ENABLE)
			GUICtrlSetState($MenuMainSettings, $GUI_ENABLE)
			GUICtrlSetState($MenuMainHelp, $GUI_ENABLE)
			GUISetState(@SW_UNLOCK)
			If $TaskBarCompat = 1 Then _ITaskBar_SetProgressState($GUIMain, 2)
			GUISetCursor(2)
		EndIf
	EndFunc   ;==>ModLogoCacheUpdate

	Func ModLogoSet($i = '', $AppLogo = 0)
		If $AppLogo Or $i = '' Or Not FileExists(@ScriptDir & '\Pics\' & $ModArray[$i + 1] & '.jpg') Then
			_SetImage($MainPic, $SMCLogo)
			$ModLogoSelected = 0
		Else
			_SetImage($MainPic, @ScriptDir & '\Pics\' & $ModArray[$i + 1] & '.jpg')
			$ModLogoSelected = $i
		EndIf
	EndFunc   ;==>ModLogoSet

	Func ModPathOpen($i)
		ShellExecute(_GUICtrlListView_GetItemText($ListView, $i, 1))
	EndFunc   ;==>ModPathOpen

	Func MoveFFTIC($BSAoptUnpack, $MoveF1, $MoveF2, $MoveT, $MoveI = 0, $MoveC = 0)
		$PathFrom = $MoveF1
		If $MoveI <> 0 And IsArray($MoveI) And $MoveI[0] > 1 Then $PathFrom &= '\' & $MoveI[2] & '\'
		$PathFrom &= $MoveF2
		$PathTo = $MoveT
		$PathFrom = StringLeft($PathFrom, 2) & StringReplace(StringReplace(StringTrimLeft($PathFrom, 2), '\\', '\'), '\\', '\')
		$PathTo = StringLeft($PathTo, 2) & StringReplace(StringReplace(StringTrimLeft($PathTo, 2), '\\', '\'), '\\', '\')
		If FileExists($PathFrom) Then
			Select
				Case $BSAoptUnpack = 1 And StringRight($PathFrom, 4) = '.bsa'
					CombineBSAoptUnpack($BSAoptUnpack, $PathFrom)
					Return 1
				Case $NoParallax = 1 And (StringRight($PathFrom, 6) = '_p.dds' Or StringRight($PathFrom, 7) = '_p1.dds' Or StringRight($PathFrom, 7) = '_p2.dds')
					LogWrite(' [T] ' & $PathFrom & ' -x> ' & $PathTo & @CRLF)
					Return 0
				Case Else
					If $MoveC = 0 Then
						If FileMove($PathFrom, $PathTo, 9) Then
							LogWrite('  |- ' & $PathFrom & ' --> ' & $PathTo & @CRLF)
							Return 1
						Else
							LogWrite(' [X] ' & $PathFrom & ' -x> ' & $PathTo & @CRLF)
							$OverallErrors += 1
							Return 0
						EndIf
					Else
						If FileCopy($PathFrom, $PathTo, 9) Then
							LogWrite('  |- ' & $PathFrom & ' ==> ' & $PathTo & @CRLF)
							Return 1
						Else
							LogWrite(' [X] ' & $PathFrom & ' =x> ' & $PathTo & @CRLF)
							$OverallErrors += 1
							Return 0
						EndIf
					EndIf
			EndSelect
		Else
			Return 0
		EndIf
	EndFunc   ;==>MoveFFTIC

	Func PageOpenApp()
		If StringLeft($LinkHome, 4) = 'http' Then ShellExecute($LinkHome)
	EndFunc   ;==>PageOpenApp

	Func PageOpenMod()
		If $ModLogoSelected Then
			MainDownload($ModLogoSelected)
		Else
			If StringLeft($ListLink, 4) = 'http' Then ShellExecute($ListLink)
		EndIf
	EndFunc   ;==>PageOpenMod

	Func ProgressPaint($hWnd, $ProgressBarPercent, $ProgressBarState)
		Local $hDC, $hMemDC, $hSv, $hObj, $hBitmap, $hTheme, $tRect, $tClip, $W, $H, $Ret, $Result = 1
		$hTheme = DllCall('uxtheme.dll', 'ptr', 'OpenThemeData', 'hwnd', $hWnd, 'wstr', 'Progress')
		If @error Or Not $hTheme[0] Then Return 0
		$tRect = _WinAPI_GetClientRect($hWnd)
		$W = DllStructGetData($tRect, 3) - DllStructGetData($tRect, 1)
		$H = DllStructGetData($tRect, 4) - DllStructGetData($tRect, 2)
		$hDC = _WinAPI_GetDC($hWnd)
		$hMemDC = _WinAPI_CreateCompatibleDC($hDC)
		$hBitmap = _WinAPI_CreateSolidBitmap(0, _WinAPI_GetSysColor(15), $W, $H, 0)
		$hSv = _WinAPI_SelectObject($hMemDC, $hBitmap)
		DllStructSetData($tRect, 1, 0)
		DllStructSetData($tRect, 2, 0)
		DllStructSetData($tRect, 3, $W)
		DllStructSetData($tRect, 4, $H)
		$Ret = DllCall('uxtheme.dll', 'uint', 'DrawThemeBackground', 'ptr', $hTheme[0], 'hwnd', $hMemDC, 'int', 1, 'int', 0, 'ptr', DllStructGetPtr($tRect), 'ptr', 0)
		If @error Or $Ret[0] Then $Result = 0
		DllStructSetData($tRect, 3, $W * $ProgressBarPercent / 100)
		$tClip = DllStructCreate($tagRECT)
		DllStructSetData($tClip, 1, 1)
		DllStructSetData($tClip, 2, 1)
		DllStructSetData($tClip, 3, $W - 1)
		DllStructSetData($tClip, 4, $H - 1)
		DllCall('uxtheme.dll', 'uint', 'DrawThemeBackground', 'ptr', $hTheme[0], 'hwnd', $hMemDC, 'int', 5, 'int', 1 + $ProgressBarState, 'ptr', DllStructGetPtr($tRect), 'ptr', DllStructGetPtr($tClip))
		If @error Or $Ret[0] Then $Result = 0
		_WinAPI_ReleaseDC($hWnd, $hDC)
		_WinAPI_SelectObject($hMemDC, $hSv)
		_WinAPI_DeleteDC($hMemDC)
		DllCall('uxtheme.dll', 'uint', 'CloseThemeData', 'ptr', $hTheme[0])
		If $Result Then
			_WinAPI_DeleteObject(_SendMessage($hWnd, 0x0172, 0, $hBitmap))
			$hObj = _SendMessage($hWnd, 0x0173)
			If $hObj <> $hBitmap Then _WinAPI_DeleteObject($hBitmap)
		EndIf
	EndFunc   ;==>ProgressPaint

	Func RunProcessing($InputPath, $OutputPath, $ProgressText = '', $ProgressSegment = -1, $ProgressCeiling = -1, $Pack = 0, $Extra = '-y')
		Local $OriginalSize = StringInStr(FileGetAttrib($InputPath), 'D') ? DirGetSize($InputPath) : FileGetSize($InputPath)
		Local $ProgressSegmentFloor = $ProgressBarLastStoredPercent, $TempPercent = 0, $TempPercentNew = 0, $MultiPart = 0
		If $Pack = 1 Then
			Local $Buffer
			$Buffer = $InputPath
			$InputPath = $OutputPath
			$OutputPath = $Buffer
		EndIf
		If $Pack <> 1 And StringInStr(StringTrimLeft($InputPath, StringInStr($InputPath, '\', 0, -1)), 'part01') Then
			$MultiPart = 1
			While FileExists(StringReplace($InputPath, 'part01', 'part' & StringRight(101 + $MultiPart, 2)))
				$OriginalSize += FileGetSize(StringReplace($InputPath, 'part' & StringRight(100 + $MultiPart, 2), 'part' & StringRight(101 + $MultiPart, 2)))
				$MultiPart += 1
			WEnd
			$ProgressText = StringReplace($ProgressText, 'part01', 'part01-' & $MultiPart)
		EndIf
		If StringCompare(StringRight($OutputPath, 1), '\') <> 0 Then $OutputPath &= '\'
		Local $PID = Run('"' & $7zExe & '" ' & ($Pack ? 'a' : 'x') & ' "' & $InputPath & '" ' & ($Pack ? '' : '-o') & '"' & $OutputPath & ($Pack ? '*' : '') & '" ' & $Extra, @ScriptDir, @SW_HIDE, 6)
		While ProcessExists($PID)
			If $CombineInterruptSwitch = -1 Then ProcessClose($PID)
			Local $7zProcessStats = ProcessGetStats($PID, 1)
			If $7zProcessStats = 0 Then ExitLoop
			Local $7zProgress = 0
			If $OriginalSize > 0 Then $7zProgress = Int($7zProcessStats[3] / $OriginalSize * 100)
			If $ProgressText Then
				$TempPercentNew = ($7zProgress > 100 ? 100 : $7zProgress < 0 ? 0 : $7zProgress)
				If $TempPercent <> $TempPercentNew Then
					$TempPercent = $TempPercentNew
					If $ProgressSegment = -1 Or $ProgressCeiling = -1 Then
						MainProgressSet(-1, $ProgressText & ' (' & $TempPercent & '%)')
					Else
						Local $ProgressCurrent = $ProgressSegmentFloor + Round($ProgressSegment * $TempPercent / 100)
						MainProgressSet($ProgressCurrent > $ProgressCeiling ? $ProgressCeiling : $ProgressCurrent, $ProgressText & ' (' & $TempPercent & '%)')
					EndIf
				EndIf
			EndIf
			Sleep(100)
		WEnd
		ProcessWaitClose($PID)
		If $CombineInterruptSwitch <> -1 Then
			Local $7zProcessOutputArray = StringSplit(StderrRead($PID), @CRLF)
			If $7zProcessOutputArray[0] > 2 Then
				LogWrite(' [X] ' & $_7zError & '=' & StringLower($7zProcessOutputArray[$7zProcessOutputArray[0] - 2]) & @CRLF)
				Return 1
			EndIf
		EndIf
		Return 0
	EndFunc   ;==>RunProcessing

	Func SettingsApply()
		GUISetCursor(15, 0, $GUISettings)
		GUISetState(@SW_DISABLE, $GUISettings)
		GUISetCursor(15, 0, $GUIMain)
		GUISetState(@SW_DISABLE, $GUIMain)
		GUICtrlSetState($SettingsButton, $GUI_DISABLE)
		For $i = 0 To UBound($SettingsArray) - 2
			If $SettingsArray[$i][2] <> $SettingsArray[$i][3] Then
				IniWrite($AppConfig, 'System', $SettingsArray[$i][0], $SettingsArray[$i][3] = 1 ? 1 : '')
				$SettingsArray[$i][2] = $SettingsArray[$i][3]
				Switch $i
					Case 7
						$NoAutoUpdate = $SettingsArray[$i][2] = 1 ? 1 : ''
					Case 8
						$NoLogging = $SettingsArray[$i][2] = 1 ? 1 : ''
				EndSwitch
			EndIf
		Next
		If $SettingsArray[UBound($SettingsArray) - 1][2] <> $SettingsArray[UBound($SettingsArray) - 1][3] Then
			IniWrite($AppConfig, 'System', $SettingsArray[UBound($SettingsArray) - 1][0], $SettingsArray[UBound($SettingsArray) - 1][3] = 1 ? 1 : '')
			$SettingsArray[UBound($SettingsArray) - 1][2] = $SettingsArray[UBound($SettingsArray) - 1][3]
			$EditorMode = $SettingsArray[UBound($SettingsArray) - 1][2] = 1 ? 1 : ''
			For $i = 0 To $ModArray[0] - 1
				ListViewItemContextMenuChange($i, (_GUICtrlListView_GetItemText($ListView, $i, 1) ? 1 : 0))
			Next
		EndIf
		GUISetState(@SW_ENABLE, $GUIMain)
		GUISetCursor(2, 0, $GUIMain)
		GUISetState(@SW_ENABLE, $GUISettings)
		GUISetCursor(2, 0, $GUISettings)
	EndFunc   ;==>SettingsApply

	Func SettingsCreate()
		If FileExists($AppConfig) Then
			If BitAND(WinGetState($GUISettings), 1) Then
				If BitAND(WinGetState($GUISettings), 2) Then
					WinActivate($GUISettings)
				Else
					GUISetState(@SW_SHOW, $GUISettings)
				EndIf
			Else
				Local $W = 250, $H = $yh * 10 + 34
				$GUISettings = GUICreate($_SettingsTitle & ' - SMC', $W, $H, $GUIMainPosition[0] + ($MainPicWidth - $W) / 2, $GUIMainPosition[1] + ($GUIMainSize[1] - $H) / 2)
				GUISetFont(10, Default, Default, 'Calibri', $GUISettings, 6)
				$SettingsCleanInstall = GUICtrlCreateCheckbox('CleanInstall', 14, 4, $W - 28, $yh, $BS_RIGHTBUTTON)
				GUICtrlSetTip($SettingsCleanInstall, $_SetCleanInstall)
				$SettingsDDSoptimize = GUICtrlCreateCheckbox('DDSoptimize', 14, 4 + $yh * 1, $W - 28, $yh, $BS_RIGHTBUTTON)
				GUICtrlSetTip($SettingsDDSoptimize, $_SetDDSoptimize)
				$SettingsBSAoptUnpack = GUICtrlCreateCheckbox('BSAoptUnpack', 14, 4 + $yh * 2, $W - 28, $yh, $BS_RIGHTBUTTON)
				GUICtrlSetTip($SettingsBSAoptUnpack, $_SetBSAoptUnpack)
				$SettingsBSAoptPack = GUICtrlCreateCheckbox('BSAoptPack', 14, 4 + $yh * 3, $W - 28, $yh, $BS_RIGHTBUTTON)
				GUICtrlSetTip($SettingsBSAoptPack, $_SetBSAoptPack)
				$SettingsCreateArchive = GUICtrlCreateCheckbox('CreateArchive', 14, 4 + $yh * 4, $W - 28, $yh, $BS_RIGHTBUTTON)
				GUICtrlSetTip($SettingsCreateArchive, $_SetCreateArchive)
				$SettingsUseCompression = GUICtrlCreateCheckbox('UseCompression', 14, 4 + $yh * 5, $W - 28, $yh, $BS_RIGHTBUTTON)
				GUICtrlSetTip($SettingsUseCompression, $_SetUseCompression)
				$SettingsNoParallax = GUICtrlCreateCheckbox('NoParallax', 14, 4 + $yh * 6, $W - 28, $yh, $BS_RIGHTBUTTON)
				GUICtrlSetTip($SettingsNoParallax, $_SetNoParallax)
				$SettingsNoAutoUpdate = GUICtrlCreateCheckbox('NoAutoUpdate', 14, 4 + $yh * 7, $W - 28, $yh, $BS_RIGHTBUTTON)
				GUICtrlSetTip($SettingsNoAutoUpdate, $_SetNoAutoUpdate)
				$SettingsNoLogging = GUICtrlCreateCheckbox('NoLogging', 14, 4 + $yh * 8, $W - 28, $yh, $BS_RIGHTBUTTON)
				GUICtrlSetTip($SettingsNoLogging, $_SetNoLogging)
				$SettingsEditorMode = GUICtrlCreateCheckbox('EditorMode', 14, 4 + $yh * 9, $W - 28, $yh, $BS_RIGHTBUTTON)
				GUICtrlSetTip($SettingsEditorMode, $_SetEditorMode)
				$SettingsButton = GUICtrlCreateButton($_Apply, 4, $H - $yh - 4, $W - 8, $yh)
				GUICtrlSetTip($SettingsButton, $_ApplySettings)
				GUICtrlSetOnEvent($SettingsButton, 'SettingsApply')
				Global $SettingsArray[][] = _
						[['CleanInstall', $SettingsCleanInstall, 0, 0], _
						['DDSoptimize', $SettingsDDSoptimize, 0, 0], _
						['BSAoptUnpack', $SettingsBSAoptUnpack, 0, 0], _
						['BSAoptPack', $SettingsBSAoptPack, 0, 0], _
						['CreateArchive', $SettingsCreateArchive, 0, 0], _
						['UseCompression', $SettingsUseCompression, 0, 0], _
						['NoParallax', $SettingsNoParallax, 0, 0], _
						['NoAutoUpdate', $SettingsNoAutoUpdate, 0, 0], _
						['NoLogging', $SettingsNoLogging, 0, 0], _
						['EditorMode', $SettingsEditorMode, 0, 0]]
				For $i = 0 To UBound($SettingsArray) - 1
					If IniRead($AppConfig, 'System', $SettingsArray[$i][0], '') = '1' Then
						GUICtrlSetState($SettingsArray[$i][1], $GUI_CHECKED)
						$SettingsArray[$i][2] = 1
						$SettingsArray[$i][3] = 1
					Else
						GUICtrlSetState($SettingsArray[$i][1], $GUI_UNCHECKED)
						$SettingsArray[$i][2] = 0
						$SettingsArray[$i][3] = 0
						If $i = 4 Then GUICtrlSetState($SettingsUseCompression, $GUI_DISABLE)
					EndIf
				Next
				GUICtrlSetState($SettingsButton, $GUI_DISABLE)
				GUISetState(@SW_SHOW, $GUISettings)
				GUISetOnEvent($GUI_EVENT_CLOSE, 'GUIClose', $GUISettings)
			EndIf
		EndIf
	EndFunc   ;==>SettingsCreate

	Func UpdateCancel()
		$UpdateCancel = 1
	EndFunc   ;==>UpdateCancel
#EndRegion Functions
